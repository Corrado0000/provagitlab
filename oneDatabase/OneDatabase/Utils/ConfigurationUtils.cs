﻿using OneDatabase.Data;
using OneDatabase.Models;
using System.Linq;

namespace OneDatabase.Utils
{
    public static class ConfigurationUtils
    {

        public static string GetConfigurationValue(this oneContext context, string Category, string Name, string Version)
        {
            string res = string.Empty;
            configuration conf = context.configuration.FirstOrDefault(s => s.category == Category && s.name == Name && s.version == Version);

            if (conf != null)
            {
                res = conf.value;
            }

            return res;
        }

        public static configuration SetConfigurationValue(this oneContext context, string Category, string Name, string Version, string Value)
        {
            configuration conf = context.configuration.FirstOrDefault(s => s.category == Category && s.name == Name && s.version == Version);


            if (conf == null)
            {
                conf = new configuration();
                conf.category = Category;
                conf.name = Name;
                conf.version = Version;
            }

            conf.value = Value;

            if (conf.configurationid <= 0)
            {
                context.configuration.Add(conf);
            }
            else
            {
                context.configuration.Update(conf);
            }

            context.SaveChanges();

            return conf;

        }


        public static CompanyPropertiesEntity_1_0 CompanyProperties(this oneContext context)
        {
            return CompanyPropertiesEntity_1_0.Load(context);
        }
        public static AziendaDefaultsEntity_1_0 AziendaDefaults(this oneContext context)
        {
            return AziendaDefaultsEntity_1_0.Load(context);
        }


    }
}
