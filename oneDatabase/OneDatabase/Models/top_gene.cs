﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class top_gene
    {
        public int top_geneid { get; set; }
        public string key_anno { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? iva_perc { get; set; }
        public string iva_tras { get; set; }
        public string iva_inca { get; set; }
        public string iva_imba { get; set; }
        public string iva_acce { get; set; }
        public string iva_boll { get; set; }
        public string iva_omag { get; set; }
        public decimal? mac_gest { get; set; }
        public decimal? dib_gest { get; set; }
        public decimal? los_gest { get; set; }
        public decimal? mov_sprt { get; set; }
        public decimal? ord_sprt { get; set; }
        public decimal? ord_spre { get; set; }
        public decimal? ord_stot { get; set; }
        public decimal? pre_sprt { get; set; }
        public decimal? pre_spre { get; set; }
        public decimal? pre_stot { get; set; }
        public decimal? ddt_sprt { get; set; }
        public decimal? ddt_spre { get; set; }
        public decimal? ddt_stot { get; set; }
        public decimal? fat_sprt { get; set; }
        public string spe_port { get; set; }
        public string spe_aspe { get; set; }
        public string spe_tipo { get; set; }
        public int? top_listid { get; set; }
        public string cod_vend { get; set; }
        public string cod_buon { get; set; }
        public int? top_magaid { get; set; }
        public char? mod_valo { get; set; }
        public string cog_tras { get; set; }
        public string cog_inca { get; set; }
        public string cog_imba { get; set; }
        public string cog_acce { get; set; }
        public string cog_boll { get; set; }
        public string cog_abbu { get; set; }
        public string cog_omag { get; set; }
        public string cog_cass { get; set; }
        public string cog_bank { get; set; }
        public string val_a { get; set; }
        public string val_b { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_tiva age_tiva { get; set; }
        public virtual top_list top_list { get; set; }
        public virtual top_maga top_maga { get; set; }
    }
}
