﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_prenotazioni
    {
        public ho_prenotazioni()
        {
            ho_log = new HashSet<ho_log>();
            ho_tavoloprenotazione = new HashSet<ho_tavoloprenotazione>();
        }

        public int ho_prenotazioniid { get; set; }
        public int? ho_salaanagraficaid { get; set; }
        public string nome { get; set; }
        public int? nadulti { get; set; }
        public int? nbambini { get; set; }
        public DateTime? dataorainizio { get; set; }
        public DateTime? dataorafine { get; set; }
        public int? id_age_nomi { get; set; }
        public string note { get; set; }
        public bool? confermata { get; set; }
        public bool? noshow { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi id_age_nomiNavigation { get; set; }
        public virtual ICollection<ho_log> ho_log { get; set; }
        public virtual ICollection<ho_tavoloprenotazione> ho_tavoloprenotazione { get; set; }
    }
}
