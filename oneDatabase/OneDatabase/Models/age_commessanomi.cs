﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_commessanomi
    {
        public int age_commessanomiid { get; set; }
        public int age_commessaid { get; set; }
        public int age_nomiid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_commessa age_commessa { get; set; }
        public virtual age_nomi age_nomi { get; set; }
    }
}
