﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class top_maga_settore
    {
        public top_maga_settore()
        {
            ana_ubicazione = new HashSet<ana_ubicazione>();
            ddt_gene = new HashSet<ddt_gene>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_gene = new HashSet<ord_gene>();
            pre_gene = new HashSet<pre_gene>();
            top_maga_scaffale = new HashSet<top_maga_scaffale>();
        }

        public int top_maga_settoreid { get; set; }
        public int? top_magaid { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public string graphstream { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual top_maga top_maga { get; set; }
        public virtual ICollection<ana_ubicazione> ana_ubicazione { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
        public virtual ICollection<top_maga_scaffale> top_maga_scaffale { get; set; }
    }
}
