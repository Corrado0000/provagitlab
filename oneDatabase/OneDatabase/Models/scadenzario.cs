﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class scadenzario
    {
        public scadenzario()
        {
            scadenzario_modifica = new HashSet<scadenzario_modifica>();
            scadenzario_rata = new HashSet<scadenzario_rata>();
        }

        public int scadenzarioid { get; set; }
        public int? movimentoid { get; set; }
        public int? movimento_contropartitaid { get; set; }
        public int? age_pagaid { get; set; }
        public int? age_bankid { get; set; }
        public decimal? importo { get; set; }
        public bool? attivita { get; set; }
        public bool? passivita { get; set; }
        public bool? modificaprecedentiscadenze { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public int? age_nomiidrap_2 { get; set; }

        public virtual movimento movimento { get; set; }
        public virtual movimento_contropartita movimento_contropartita { get; set; }
        public virtual ICollection<scadenzario_modifica> scadenzario_modifica { get; set; }
        public virtual ICollection<scadenzario_rata> scadenzario_rata { get; set; }
    }
}
