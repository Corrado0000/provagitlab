﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_tavoloprenotazione
    {
        public int ho_tavoloprenotazioneid { get; set; }
        public int id_prenotazione { get; set; }
        public int id_map_sala_tavolo { get; set; }

        public virtual ho_tavoloanagrafica id_map_sala_tavoloNavigation { get; set; }
        public virtual ho_prenotazioni id_prenotazioneNavigation { get; set; }
    }
}
