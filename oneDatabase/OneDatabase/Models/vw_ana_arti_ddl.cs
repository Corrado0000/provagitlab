﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ana_arti_ddl
    {
        public int? ana_artiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string rif_arti { get; set; }
        public bool? enabled { get; set; }
        public string descrizione { get; set; }
        public decimal? giacenza { get; set; }
        public string ff { get; set; }
        public string for_arti { get; set; }
        public bool? istaglia { get; set; }
        public string key_anno { get; set; }
        public int? top_magaid { get; set; }
        public string sel_arti { get; set; }
    }
}
