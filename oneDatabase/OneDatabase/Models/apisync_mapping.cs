﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class apisync_mapping
    {
        public int apisync_mappingid { get; set; }
        public string entityname { get; set; }
        public int? entityid { get; set; }
        public long? apisync_itemid { get; set; }
        public int? config_apisyncid { get; set; }
        public string valuestring { get; set; }
    }
}
