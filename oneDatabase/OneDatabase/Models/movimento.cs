﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class movimento
    {
        public movimento()
        {
            movimento_contropartita = new HashSet<movimento_contropartita>();
            movimento_iva = new HashSet<movimento_iva>();
            scadenzario = new HashSet<scadenzario>();
        }

        public int movimentoid { get; set; }
        public string numero { get; set; }
        public string anno { get; set; }
        public DateTime? datamovimento { get; set; }
        public int? causaleid { get; set; }
        public int? age_valuid { get; set; }
        public string descrizione { get; set; }
        public string numerodocumento { get; set; }
        public DateTime? datadocumento { get; set; }
        public int? registroivaid { get; set; }
        public string protocollo { get; set; }
        public int? pianodeicontiid { get; set; }
        public int? age_nomiid { get; set; }
        public decimal? totaledocumento { get; set; }
        public decimal? imponibile { get; set; }
        public decimal? imposta { get; set; }
        public decimal? impostadetraibile { get; set; }
        public decimal? impostaindetraibile { get; set; }
        public int? rif_fat_geneid { get; set; }
        public int? rif_mov_geneid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public int? rif_fat_gene_incassoid { get; set; }
        public bool? ritenuta { get; set; }
        public int? rif_movimentoid_pagamue { get; set; }
        public DateTime? dataricezione { get; set; }
        public bool? risconto { get; set; }
        public bool? rateo { get; set; }
        public DateTime? datainiziocompetenza { get; set; }
        public DateTime? datafinecompetenza { get; set; }
        public int? rif_movimentoid_rateorisconto { get; set; }
        public decimal? aliquotaritenuta { get; set; }
        public decimal? importoritenuta { get; set; }
        public int? rif_pianodeicontiid_rateorisconto { get; set; }

        public virtual causale causale { get; set; }
        public virtual pianodeiconti pianodeiconti { get; set; }
        public virtual registroiva registroiva { get; set; }
        public virtual ICollection<movimento_contropartita> movimento_contropartita { get; set; }
        public virtual ICollection<movimento_iva> movimento_iva { get; set; }
        public virtual ICollection<scadenzario> scadenzario { get; set; }
    }
}
