﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_cont
    {
        public age_cont()
        {
            age_cdc = new HashSet<age_cdc>();
        }

        public int age_contid { get; set; }
        public int? age_nomiid { get; set; }
        public string key_agen { get; set; }
        public string cnt_nome { get; set; }
        public string cnt_cari { get; set; }
        public string tel_nume { get; set; }
        public string tel_nfax { get; set; }
        public string tel_ncel { get; set; }
        public string cnt_mail { get; set; }
        public string cnt_note { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual ICollection<age_cdc> age_cdc { get; set; }
    }
}
