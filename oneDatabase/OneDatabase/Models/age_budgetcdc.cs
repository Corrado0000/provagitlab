﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_budgetcdc
    {
        public int age_budgetcdcid { get; set; }
        public int? age_cdcid { get; set; }
        public string key_anno { get; set; }
        public decimal? acquisto { get; set; }
        public decimal? vendita { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_cdc age_cdc { get; set; }
    }
}
