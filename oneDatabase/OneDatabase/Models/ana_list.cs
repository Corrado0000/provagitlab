﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_list
    {
        public ana_list()
        {
            age_nomi = new HashSet<age_nomi>();
            ana_offe = new HashSet<ana_offe>();
            ord_gene = new HashSet<ord_gene>();
        }

        public int ana_listid { get; set; }
        public int? ana_artiid { get; set; }
        public int? top_listid { get; set; }
        public string cod_prez { get; set; }
        public decimal? ric_perc { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string sco_text { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? prv_impo { get; set; }
        public bool? pre_impo_manuale { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? prv_tota { get; set; }
        public decimal? pro_1 { get; set; }
        public decimal? pro_2 { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool? pre_impo_listino { get; set; }
        public int? ana_list_ricaricaid { get; set; }
        public decimal? ric_fissa { get; set; }
        public int? age_nomiid { get; set; }
        public decimal? sconto_da { get; set; }
        public decimal? sconto_a { get; set; }
        public DateTime? datainizio_in { get; set; }
        public DateTime? datafine_in { get; set; }
        public bool? usa_pre_acqu { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual top_list top_list { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
        public virtual ICollection<ana_offe> ana_offe { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
    }
}
