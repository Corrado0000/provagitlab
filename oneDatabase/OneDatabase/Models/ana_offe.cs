﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_offe
    {
        public int ana_offeid { get; set; }
        public string key_offe { get; set; }
        public string cod_offe { get; set; }
        public DateTime? dat_iniz { get; set; }
        public DateTime? dat_fine { get; set; }
        public int? ana_artiid { get; set; }
        public string gru_arti { get; set; }
        public string mar_arti { get; set; }
        public int? age_nomiid { get; set; }
        public int? ana_listid { get; set; }
        public string cod_prez { get; set; }
        public decimal? qta_base { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? prv_impo { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string sco_text { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? prv_tota { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual ana_arti ana_arti { get; set; }
        public virtual ana_list ana_list { get; set; }
    }
}
