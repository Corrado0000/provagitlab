﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class aggiornamento_prezzi_xmarca
    {
        public string cod_arti { get; set; }
        public decimal? pre_vend { get; set; }
        public decimal? iva_perc { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? ric_perc { get; set; }
    }
}
