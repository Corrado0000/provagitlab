﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_mapaggiunta
    {
        public int ho_mapaggiuntaid { get; set; }
        public int? ana_artiid { get; set; }
        public int? ana_cateid { get; set; }
        public int? ana_subcateid { get; set; }
        public int? pietanzaid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual ana_cate ana_cate { get; set; }
        public virtual ana_subcate ana_subcate { get; set; }
        public virtual ana_arti pietanza { get; set; }
    }
}
