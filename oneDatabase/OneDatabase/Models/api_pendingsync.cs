﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class api_pendingsync
    {
        public int api_pendingsyncid { get; set; }
        public string entityname { get; set; }
        public long? entityid { get; set; }
        public int? config_apisyncid { get; set; }
    }
}
