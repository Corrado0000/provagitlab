﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_fat_gene_dett
    {
        public int? fat_geneid { get; set; }
        public int? ana_artiid { get; set; }
        public int? doc_artiid { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? pre_tota { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public decimal? iva_perc { get; set; }
        public string iva_code { get; set; }
        public string sco_text { get; set; }
        public string num_fatt { get; set; }
        public DateTime? dat_fatt { get; set; }
        public string key_anno { get; set; }
        public string cod_matr { get; set; }
        public string cod_type { get; set; }
        public string art_misu { get; set; }
        public string num_boll { get; set; }
        public string des_plus { get; set; }
        public string art_note { get; set; }
        public decimal? subtotale { get; set; }
        public string art_item { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
        public int? indice { get; set; }
    }
}
