﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class scadenzario_modifica
    {
        public int scadenzario_modificaid { get; set; }
        public int? scadenzarioid { get; set; }
        public int? scadenzario_ratamodificataid { get; set; }
        public decimal? importo { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual scadenzario scadenzario { get; set; }
        public virtual scadenzario_rata scadenzario_ratamodificata { get; set; }
    }
}
