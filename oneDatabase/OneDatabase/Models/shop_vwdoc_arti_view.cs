﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class shop_vwdoc_arti_view
    {
        public int? doc_artiid { get; set; }
        public int? mov_geneid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string art_misu { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? pre_acqu { get; set; }
        public string sco_text { get; set; }
        public decimal? iva_perc { get; set; }
        public bool? enabled { get; set; }
        public int? ddt_geneid { get; set; }
        public int? fat_geneid { get; set; }
        public decimal? qta_logi { get; set; }
        public string rif_arti { get; set; }
        public decimal? pre_vend { get; set; }
        public string num_logi { get; set; }
    }
}
