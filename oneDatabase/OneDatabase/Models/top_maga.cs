﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class top_maga
    {
        public top_maga()
        {
            ana_losc = new HashSet<ana_losc>();
            ana_maga = new HashSet<ana_maga>();
            ana_ubicazione = new HashSet<ana_ubicazione>();
            ddt_gene = new HashSet<ddt_gene>();
            doc_arti = new HashSet<doc_arti>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_arti = new HashSet<ord_arti>();
            ord_gene = new HashSet<ord_gene>();
            pre_arti = new HashSet<pre_arti>();
            pre_gene = new HashSet<pre_gene>();
            top_caus = new HashSet<top_caus>();
            top_gene = new HashSet<top_gene>();
            top_maga_settore = new HashSet<top_maga_settore>();
        }

        public int top_magaid { get; set; }
        public string cod_maga { get; set; }
        public string des_maga { get; set; }
        public string mag_tipo { get; set; }
        public decimal? mag_fisc { get; set; }
        public string rif_indi { get; set; }
        public string rif_nciv { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public int? age_statid { get; set; }
        public string rif_quad { get; set; }
        public int? age_nomiid { get; set; }
        public char? cod_serv { get; set; }
        public string cod_tari { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? prv_impo { get; set; }
        public string ldv_into { get; set; }
        public string mapfile { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual age_stat age_stat { get; set; }
        public virtual ICollection<ana_losc> ana_losc { get; set; }
        public virtual ICollection<ana_maga> ana_maga { get; set; }
        public virtual ICollection<ana_ubicazione> ana_ubicazione { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<doc_arti> doc_arti { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_arti> ord_arti { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_arti> pre_arti { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
        public virtual ICollection<top_caus> top_caus { get; set; }
        public virtual ICollection<top_gene> top_gene { get; set; }
        public virtual ICollection<top_maga_settore> top_maga_settore { get; set; }
    }
}
