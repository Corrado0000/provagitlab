﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class top_nume
    {
        public int top_numeid { get; set; }
        public string key_anno { get; set; }
        public string cod_docu { get; set; }
        public string cod_nume { get; set; }
        public string des_nume { get; set; }
        public char? pre_nume { get; set; }
        public decimal? pro_nume { get; set; }
        public string suf_nume { get; set; }
        public DateTime? dat_nume { get; set; }
        public decimal? dat_ctrl { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
