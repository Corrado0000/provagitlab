﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwcommessa_utilizzata
    {
        public string modulo { get; set; }
        public string cod_commessa { get; set; }
        public int? age_nomiid { get; set; }
    }
}
