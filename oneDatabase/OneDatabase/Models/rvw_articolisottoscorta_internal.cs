﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_articolisottoscorta_internal
    {
        public int? ana_artiid { get; set; }
        public int? age_nomiid { get; set; }
        public string key_anno { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string gru_arti { get; set; }
        public int? ana_marcaid { get; set; }
        public string marca { get; set; }
        public decimal? qta_giac { get; set; }
        public decimal? qta_scor { get; set; }
        public decimal? qta_rior { get; set; }
        public bool? enabled { get; set; }
        public decimal? pqta_tota { get; set; }
        public decimal? oqta_tota { get; set; }
        public string magazzino { get; set; }
        public int? top_magaid { get; set; }
    }
}
