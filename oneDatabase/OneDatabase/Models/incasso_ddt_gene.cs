﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class incasso_ddt_gene
    {
        public int incasso_ddt_geneid { get; set; }
        public int? ddt_geneid { get; set; }
        public int? movimentoid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
