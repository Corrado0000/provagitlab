﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_lifo
    {
        public int ana_lifoid { get; set; }
        public string key_anno { get; set; }
        public int? ana_artiid { get; set; }
        public decimal? qta_inve { get; set; }
        public decimal? qta_entr { get; set; }
        public decimal? qta_usci { get; set; }
        public decimal? qta_giac { get; set; }
        public decimal? qta_ordi { get; set; }
        public decimal? qta_impe { get; set; }
        public decimal? qta_disp { get; set; }
        public decimal? qta_acqu { get; set; }
        public DateTime? ult_acqu { get; set; }
        public decimal? pre_inve { get; set; }
        public decimal? prv_inve { get; set; }
        public decimal? cos_medi { get; set; }
        public decimal? cov_medi { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
    }
}
