﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_onemobile_articoli
    {
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string des_plus { get; set; }
        public string rif_arti { get; set; }
        public string nome { get; set; }
        public decimal? art_peso { get; set; }
        public decimal? qta_conf { get; set; }
        public decimal? qta_coll { get; set; }
        public decimal? qta_scor { get; set; }
        public decimal? qta_maxi { get; set; }
        public decimal? qta_rior { get; set; }
        public decimal? net_acqu { get; set; }
        public decimal? iva_perc { get; set; }
        public string log_1 { get; set; }
        public bool? enabled { get; set; }
        public string key_anno { get; set; }
        public decimal? qta_inve { get; set; }
        public decimal? qta_entr { get; set; }
        public decimal? qta_usci { get; set; }
        public decimal? qta_impe { get; set; }
        public decimal? qta_giac { get; set; }
        public decimal? qta_ordi { get; set; }
        public decimal? qta_disp { get; set; }
        public decimal? ric_perc { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? pre_tota { get; set; }
        public string cod_list { get; set; }
        public string cod_maga { get; set; }
        public string for_arti { get; set; }
        public int? ana_artiid { get; set; }
        public int? top_magaid { get; set; }
    }
}
