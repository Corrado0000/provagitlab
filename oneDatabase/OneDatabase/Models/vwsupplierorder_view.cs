﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwsupplierorder_view
    {
        public int? ord_geneid { get; set; }
        public int? num_ordi_number { get; set; }
        public string num_ordi { get; set; }
        public string cod_caus { get; set; }
        public string cod_maga { get; set; }
        public string rag_soci { get; set; }
        public string cod_agen { get; set; }
        public string num_comm { get; set; }
        public DateTime? dat_comm { get; set; }
        public string cod_vett { get; set; }
        public string cod_bank { get; set; }
        public string des_bank { get; set; }
        public char? pre_nume { get; set; }
        public DateTime? dat_ordi { get; set; }
        public int? top_causid { get; set; }
        public int? top_magaid { get; set; }
        public int? age_nomiid { get; set; }
        public string doc_rife { get; set; }
        public decimal? tot_docu { get; set; }
        public decimal? tot_impo { get; set; }
        public string doc_stat { get; set; }
        public bool? enabled { get; set; }
        public int? creationuser { get; set; }
    }
}
