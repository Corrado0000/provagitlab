﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_stat
    {
        public age_stat()
        {
            age_bank = new HashSet<age_bank>();
            age_nomi = new HashSet<age_nomi>();
            age_sedi = new HashSet<age_sedi>();
            age_valu = new HashSet<age_valu>();
            ddt_gene = new HashSet<ddt_gene>();
            fat_gene = new HashSet<fat_gene>();
            ord_gene = new HashSet<ord_gene>();
            pre_gene = new HashSet<pre_gene>();
            top_maga = new HashSet<top_maga>();
        }

        public int age_statid { get; set; }
        public string codice { get; set; }
        public string cod_stat { get; set; }
        public string descrizione { get; set; }
        public string continente { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<age_bank> age_bank { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
        public virtual ICollection<age_sedi> age_sedi { get; set; }
        public virtual ICollection<age_valu> age_valu { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
        public virtual ICollection<top_maga> top_maga { get; set; }
    }
}
