﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_abbinamenti
    {
        public int ho_abbinamentiid { get; set; }
        public int? ana_artiid { get; set; }
        public int? ana_artiabbinamentoid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual ana_arti ana_artiabbinamento { get; set; }
    }
}
