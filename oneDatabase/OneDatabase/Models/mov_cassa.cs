﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class mov_cassa
    {
        public int mov_cassaid { get; set; }
        public string tipo { get; set; }
        public decimal? importo { get; set; }
        public DateTime? data { get; set; }
        public bool? isfirstofday { get; set; }
    }
}
