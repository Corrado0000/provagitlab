﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ddt_gene_stato
    {
        public int? ddt_geneid { get; set; }
        public int? fatturatototalmente { get; set; }
        public int? fatturatoparzialmente { get; set; }
        public int? nonfatturato { get; set; }
    }
}
