﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_tiva
    {
        public age_tiva()
        {
            age_nomi = new HashSet<age_nomi>();
            age_sedi = new HashSet<age_sedi>();
            ana_arti = new HashSet<ana_arti>();
            ddt_gene = new HashSet<ddt_gene>();
            doc_arti = new HashSet<doc_arti>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_arti = new HashSet<ord_arti>();
            ord_gene = new HashSet<ord_gene>();
            pre_arti = new HashSet<pre_arti>();
            pre_gene = new HashSet<pre_gene>();
            top_gene = new HashSet<top_gene>();
        }

        public int age_tivaid { get; set; }
        public string iva_code { get; set; }
        public string iva_desc { get; set; }
        public decimal? iva_perc { get; set; }
        public string iva_tipo { get; set; }
        public char? iva_ncli { get; set; }
        public char? iva_nfor { get; set; }
        public char? iva_ecli { get; set; }
        public char? iva_efor { get; set; }
        public decimal? iva_inue { get; set; }
        public decimal? iva_exue { get; set; }
        public decimal? iva_noli { get; set; }
        public decimal? iva_dedi { get; set; }
        public string old_code { get; set; }
        public decimal? indetraibilita { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string natura { get; set; }
        public string riferimento_normativo { get; set; }
        public bool? autoconsumo { get; set; }

        public virtual ICollection<age_nomi> age_nomi { get; set; }
        public virtual ICollection<age_sedi> age_sedi { get; set; }
        public virtual ICollection<ana_arti> ana_arti { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<doc_arti> doc_arti { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_arti> ord_arti { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_arti> pre_arti { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
        public virtual ICollection<top_gene> top_gene { get; set; }
    }
}
