﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_forn
    {
        public int ana_fornid { get; set; }
        public int? ana_artiid { get; set; }
        public string rif_arti { get; set; }
        public int? age_nomiid { get; set; }
        public int? age_valuid { get; set; }
        public string cod_acqu { get; set; }
        public decimal? pre_acqu { get; set; }
        public decimal? prv_acqu { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string sco_text { get; set; }
        public decimal? net_acqu { get; set; }
        public decimal? nev_acqu { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string tiporecord { get; set; }
        public decimal? pre_cons { get; set; }
        public decimal? pre_publ { get; set; }
        public int? dilaz_pag_giorni { get; set; }
        public int? giorni_abbuono_calc_add { get; set; }
        public decimal? perc_addebito_agg { get; set; }
        public decimal? sco_cassa { get; set; }
        public string allin_scad { get; set; }
        public string periodicita_fatturaz { get; set; }
        public string cond_seq { get; set; }
        public int? qta_max { get; set; }
        public int? qta_min { get; set; }
        public int? qta_ass { get; set; }
        public int? qta_omag { get; set; }
        public DateTime? data_iniz_validita { get; set; }
        public DateTime? data_fine_validita { get; set; }
        public int? qta_arti_omaggio { get; set; }
        public string desc_arti_omaggio { get; set; }
        public decimal? perc_sconto_ricarico { get; set; }
        public string segno_sconto_ricarico { get; set; }
        public string ripresa_sconti_listino { get; set; }
        public string cond_sottose { get; set; }
        public int? ana_artiomaggid { get; set; }
        public bool? fromfarmaclick { get; set; }
        public decimal? lis_base { get; set; }
        public decimal? lis_netto { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual age_valu age_valu { get; set; }
        public virtual ana_arti ana_arti { get; set; }
    }
}
