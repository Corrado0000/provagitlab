﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class registroiva
    {
        public registroiva()
        {
            causale = new HashSet<causale>();
            configurazione = new HashSet<configurazione>();
            movimento = new HashSet<movimento>();
            registroiva_tipomovimento = new HashSet<registroiva_tipomovimento>();
        }

        public int registroivaid { get; set; }
        public string anno { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public bool? ivaposticipata { get; set; }
        public int? ultimoprogressivo { get; set; }
        public DateTime? ultimadata { get; set; }
        public decimal? stampa_ultimomese { get; set; }
        public decimal? stampa_ultimapagina { get; set; }
        public bool? stampanumeridipagina { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<causale> causale { get; set; }
        public virtual ICollection<configurazione> configurazione { get; set; }
        public virtual ICollection<movimento> movimento { get; set; }
        public virtual ICollection<registroiva_tipomovimento> registroiva_tipomovimento { get; set; }
    }
}
