﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class movimento_iva
    {
        public int movimento_ivaid { get; set; }
        public int? movimentoid { get; set; }
        public int? indice { get; set; }
        public decimal? imponibile { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? imposta { get; set; }
        public decimal? impostadetraibile { get; set; }
        public decimal? impostaindetraibile { get; set; }
        public int? pianodeicontiid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual movimento movimento { get; set; }
        public virtual pianodeiconti pianodeiconti { get; set; }
    }
}
