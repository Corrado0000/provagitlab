﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_age_nomi_ddl
    {
        public int? age_nomiid { get; set; }
        public string cod_agen { get; set; }
        public string rag_soci { get; set; }
        public string par_tiva { get; set; }
        public bool? enabled { get; set; }
        public int? age_prefid { get; set; }
        public int? age_cateid { get; set; }
        public decimal? cli_pref { get; set; }
        public decimal? for_pref { get; set; }
        public decimal? vet_pref { get; set; }
        public decimal? rap_pref { get; set; }
        public string rag_tito { get; set; }
        public string descliente { get; set; }
    }
}
