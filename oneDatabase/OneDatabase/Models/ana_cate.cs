﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_cate
    {
        public ana_cate()
        {
            Inverseana_catepadre = new HashSet<ana_cate>();
            ana_cate_scontoconcordato = new HashSet<ana_cate_scontoconcordato>();
            ana_subcate = new HashSet<ana_subcate>();
            ho_configurazionestampante = new HashSet<ho_configurazionestampante>();
            ho_mapaggiunta = new HashSet<ho_mapaggiunta>();
            ho_mapmenuprod = new HashSet<ho_mapmenuprod>();
        }

        public int ana_cateid { get; set; }
        public string cod_categoria { get; set; }
        public string desc_categoria { get; set; }
        public int? indice { get; set; }
        public int? livello { get; set; }
        public int? ana_catepadreid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string label { get; set; }
        public bool? cauzione { get; set; }
        public decimal? sconto_da { get; set; }
        public decimal? sconto_a { get; set; }
        public char? blocco_omag { get; set; }
        public bool? ps_nosync { get; set; }

        public virtual ana_cate ana_catepadre { get; set; }
        public virtual ICollection<ana_cate> Inverseana_catepadre { get; set; }
        public virtual ICollection<ana_cate_scontoconcordato> ana_cate_scontoconcordato { get; set; }
        public virtual ICollection<ana_subcate> ana_subcate { get; set; }
        public virtual ICollection<ho_configurazionestampante> ho_configurazionestampante { get; set; }
        public virtual ICollection<ho_mapaggiunta> ho_mapaggiunta { get; set; }
        public virtual ICollection<ho_mapmenuprod> ho_mapmenuprod { get; set; }
    }
}
