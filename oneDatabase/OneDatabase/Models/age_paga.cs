﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_paga
    {
        public age_paga()
        {
            age_nomi = new HashSet<age_nomi>();
            ddt_gene = new HashSet<ddt_gene>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_gene = new HashSet<ord_gene>();
            pre_gene = new HashSet<pre_gene>();
        }

        public int age_pagaid { get; set; }
        public string cod_paga { get; set; }
        public string des_paga { get; set; }
        public int? tip_pagaid { get; set; }
        public decimal? pag_rate { get; set; }
        public decimal? pag_iniz { get; set; }
        public decimal? pag_perc { get; set; }
        public decimal? pag_step { get; set; }
        public decimal? pag_gfis { get; set; }
        public decimal? pag_spes { get; set; }
        public decimal? pag_scon { get; set; }
        public string pag_escl { get; set; }
        public string pag_mese { get; set; }
        public string pag_ivam { get; set; }
        public string pag_spem { get; set; }
        public decimal? pag_skip { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string cog_inc { get; set; }
        public string cog_inc_contropartita { get; set; }

        public virtual tip_paga tip_paga { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
    }
}
