﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class bilancio
    {
        public int bilancioid { get; set; }
        public string anno { get; set; }
        public bool aperto { get; set; }
        public int? movimentochiusurapatrimoniali { get; set; }
        public int? movimentochiusuraeconomici { get; set; }
        public int? movimentoaperturapatrimoniali { get; set; }
    }
}
