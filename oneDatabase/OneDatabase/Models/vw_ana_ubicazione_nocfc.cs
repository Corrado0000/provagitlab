﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ana_ubicazione_nocfc
    {
        public int? ana_artiid { get; set; }
        public int? doc_artiid { get; set; }
        public int? percontodiage_nomiid { get; set; }
        public int? pre_artiid { get; set; }
        public int? ord_artiid { get; set; }
        public int? top_magaid { get; set; }
        public int? top_maga_settoreid { get; set; }
        public int? top_maga_scaffaleid { get; set; }
        public int? top_maga_posizioneid { get; set; }
        public string ubicazione { get; set; }
        public decimal? quantita { get; set; }
    }
}
