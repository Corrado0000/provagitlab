﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwcustomerorder_totaleevaso
    {
        public int? pre_geneid { get; set; }
        public decimal? totale_evaso { get; set; }
    }
}
