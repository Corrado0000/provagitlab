﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwcausale
    {
        public int? causaleid { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public string anno { get; set; }
        public string ds_tipomovimento { get; set; }
        public int? tipomovimentoid { get; set; }
        public string ds_registroiva { get; set; }
        public int? registroivaid { get; set; }
        public bool? enabled { get; set; }
    }
}
