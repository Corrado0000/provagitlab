﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class tbl_scontrino
    {
        public int tbl_scontrinoid { get; set; }
        public string scontrino_inte { get; set; }
        public decimal? totale_scontrino { get; set; }
        public decimal? totale_contanti { get; set; }
        public int? num_righe { get; set; }
        public int? rif_movgeneid { get; set; }
        public int? rif_fatgeneid { get; set; }
        public int? tipopagamento { get; set; }
        public DateTime? creationdate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public int? lastmodifyuser { get; set; }
        public bool? isfatturato { get; set; }
        public bool? enabled { get; set; }

        public virtual tbl_scontrino tbl_scontrinoNavigation { get; set; }
        public virtual tbl_scontrino Inversetbl_scontrinoNavigation { get; set; }
    }
}
