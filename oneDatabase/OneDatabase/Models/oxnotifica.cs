﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class oxnotifica
    {
        public int oxnotificaid { get; set; }
        public string users { get; set; }
        public string users_descr { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public int? interval { get; set; }
        public string filename { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
