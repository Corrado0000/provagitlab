﻿using OneDatabase.Data;
using OneDatabase.Utils;
using System;

namespace OneDatabase.Models
{
    [Serializable]
    public class AziendaDefaultsEntity_1_0
    {
       

        private const String _category = "Sinergia.One";
        private const String _name = "AziendaDefaults";
        public const String Version = "0.1.1";

      
        public int CausaleFattura { get; set; }
        public int CausaleDDT { get; set; }
        #region Proprietà

        public int IvaStandardID { get; set; }
        public int IvaOmaggiID { get; set; }
        public int IvaTraspostoID { get; set; }
        public int IvaIncassoID { get; set; }
        public int IvaImballoID { get; set; }
        public int IvaAccessoriID { get; set; }
        public int IvaBolliID { get; set; }
        public String CodContoRicaviTrasporto { get; set; }
        public String CodContoRicaviIncasso { get; set; }
        public String CodContoRicaviImballo { get; set; }
        public String CodContoRicaviAccessori { get; set; }
        public String CodContoRicaviBolli { get; set; }
        public int DefaultStateID { get; set; }
        public int DefaultValutaID { get; set; }

        #endregion

        #region Persistenza

        public static AziendaDefaultsEntity_1_0 Load(oneContext context)
        {
            // Deserializzazione.
            //
            String xml = context.GetConfigurationValue(_category, _name, Version);

            if (xml != "")
            {
                System.IO.StringReader sr = new System.IO.StringReader(xml);

                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AziendaDefaultsEntity_1_0));
                return (AziendaDefaultsEntity_1_0)serializer.Deserialize(sr);
            }

            return new AziendaDefaultsEntity_1_0();
        }

        public void Save(oneContext context)
        {
            // Serializzazione risultati.
            //
            System.IO.StringWriter sw = new System.IO.StringWriter();

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AziendaDefaultsEntity_1_0));
            serializer.Serialize(sw, this);

            context.SetConfigurationValue(_category, _name, Version, sw.ToString());
        }

        #endregion

    }
}
