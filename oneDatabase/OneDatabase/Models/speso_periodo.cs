﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class speso_periodo
    {
        public string codice_periodo { get; set; }
        public string periodo { get; set; }
    }
}
