﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class doc_arti
    {
        public doc_arti()
        {
            Inversedoc_arti_padre = new HashSet<doc_arti>();
            ana_lotto = new HashSet<ana_lotto>();
            ana_ubicazione = new HashSet<ana_ubicazione>();
            move_gene_taglie = new HashSet<move_gene_taglie>();
        }

        public int doc_artiid { get; set; }
        public string key_anno { get; set; }
        public string key_arti { get; set; }
        public int? mov_geneid { get; set; }
        public int? ddt_geneid { get; set; }
        public int? fat_geneid { get; set; }
        public int? importedddt_geneid { get; set; }
        public string num_docu { get; set; }
        public string num_rife { get; set; }
        public string cod_rife { get; set; }
        public string num_logi { get; set; }
        public int? ana_artiid { get; set; }
        public string des_arti { get; set; }
        public string sel_arti { get; set; }
        public string art_item { get; set; }
        public string art_misu { get; set; }
        public string art_note { get; set; }
        public string art_comp { get; set; }
        public int? top_magaid { get; set; }
        public int? ana_loscid { get; set; }
        public DateTime? cod_scad { get; set; }
        public string cod_prez { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? qta_logi { get; set; }
        public string cod_matr { get; set; }
        public string cod_type { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? prv_impo { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string sco_text { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? prv_tota { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? iva_perc { get; set; }
        public decimal? pro_1 { get; set; }
        public decimal? pro_2 { get; set; }
        public int? age_cdcid { get; set; }
        public string dbf_gate { get; set; }
        public int? fidelityage_nomiid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public decimal? qta_conf { get; set; }
        public int? tbl_scontrinoid { get; set; }
        public int? doc_arti_padreid { get; set; }
        public decimal? qta_distinta { get; set; }
        public int importedpre_geneid { get; set; }
        public string tipo_doc_importazione { get; set; }
        public int importedord_geneid { get; set; }
        public int indice { get; set; }
        public string des_arti_sostitutiva { get; set; }
        public bool? cauzione { get; set; }
        public decimal? qta_coll { get; set; }

        public virtual age_cdc age_cdc { get; set; }
        public virtual age_tiva age_tiva { get; set; }
        public virtual ana_arti ana_arti { get; set; }
        public virtual ana_losc ana_losc { get; set; }
        public virtual ddt_gene ddt_gene { get; set; }
        public virtual doc_arti doc_arti_padre { get; set; }
        public virtual fat_gene fat_gene { get; set; }
        public virtual age_nomi fidelityage_nomi { get; set; }
        public virtual mov_gene mov_gene { get; set; }
        public virtual top_maga top_maga { get; set; }
        public virtual ICollection<doc_arti> Inversedoc_arti_padre { get; set; }
        public virtual ICollection<ana_lotto> ana_lotto { get; set; }
        public virtual ICollection<ana_ubicazione> ana_ubicazione { get; set; }
        public virtual ICollection<move_gene_taglie> move_gene_taglie { get; set; }
    }
}
