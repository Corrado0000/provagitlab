﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_conti
    {
        public int ho_contiid { get; set; }
        public int? id_map_sala_tavolo { get; set; }
        public decimal? totale_conto { get; set; }
        public string id_group { get; set; }
        public DateTime? dataoraapertura { get; set; }
        public DateTime? dataorachiusura { get; set; }
        public int? ncoperti { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ho_tavoloanagrafica id_map_sala_tavoloNavigation { get; set; }
    }
}
