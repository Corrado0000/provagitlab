﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_tavoloanagrafica
    {
        public ho_tavoloanagrafica()
        {
            ho_conti = new HashSet<ho_conti>();
            ho_contopiet = new HashSet<ho_contopiet>();
            ho_logid_tavoloanagraficaNavigation = new HashSet<ho_log>();
            ho_logid_tavoloanagrafica_outNavigation = new HashSet<ho_log>();
            ho_tavoloprenotazione = new HashSet<ho_tavoloprenotazione>();
        }

        public int ho_tavoloanagraficaid { get; set; }
        public int? ho_salaanagraficaid { get; set; }
        public int? ho_tipotavoloid { get; set; }
        public string nome { get; set; }
        public string tavstato { get; set; }
        public int? tavmappax { get; set; }
        public int? tavmappay { get; set; }
        public DateTime? datainiziooccupato { get; set; }
        public bool contoaperto { get; set; }
        public string idgroupcontoaperto { get; set; }
        public bool ispark { get; set; }
        public string motivo_annulla { get; set; }
        public int? id_prenotazione { get; set; }
        public int? id_tavolocollegato { get; set; }
        public int? age_nomiid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ho_salaanagrafica ho_salaanagrafica { get; set; }
        public virtual ho_tipotavolo ho_tipotavolo { get; set; }
        public virtual ICollection<ho_conti> ho_conti { get; set; }
        public virtual ICollection<ho_contopiet> ho_contopiet { get; set; }
        public virtual ICollection<ho_log> ho_logid_tavoloanagraficaNavigation { get; set; }
        public virtual ICollection<ho_log> ho_logid_tavoloanagrafica_outNavigation { get; set; }
        public virtual ICollection<ho_tavoloprenotazione> ho_tavoloprenotazione { get; set; }
    }
}
