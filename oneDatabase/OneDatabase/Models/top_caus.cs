﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class top_caus
    {
        public top_caus()
        {
            Inversetop_causid_trasddtNavigation = new HashSet<top_caus>();
            Inversetop_causid_trasfatNavigation = new HashSet<top_caus>();
            ddt_gene = new HashSet<ddt_gene>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_gene = new HashSet<ord_gene>();
            pre_gene = new HashSet<pre_gene>();
        }

        public int top_causid { get; set; }
        public string cod_caus { get; set; }
        public string des_caus { get; set; }
        public string cod_docu { get; set; }
        public int? top_magaid { get; set; }
        public string cod_tipo { get; set; }
        public char? cod_serv { get; set; }
        public string cod_tari { get; set; }
        public decimal? cau_entr { get; set; }
        public decimal? cau_usci { get; set; }
        public decimal? cau_impe { get; set; }
        public decimal? cau_ordi { get; set; }
        public decimal? cau_valo { get; set; }
        public decimal? cau_cont { get; set; }
        public decimal? cau_maga { get; set; }
        public bool? cau_nota { get; set; }
        public decimal? cli_pref { get; set; }
        public decimal? for_pref { get; set; }
        public decimal? vet_pref { get; set; }
        public decimal? ddt_from { get; set; }
        public decimal? ddt_info { get; set; }
        public decimal? ord_from { get; set; }
        public decimal? ord_info { get; set; }
        public decimal? pre_from { get; set; }
        public decimal? pre_info { get; set; }
        public decimal? cnt_from { get; set; }
        public decimal? cnt_info { get; set; }
        public decimal? int_from { get; set; }
        public decimal? int_info { get; set; }
        public decimal? rip_from { get; set; }
        public decimal? rip_info { get; set; }
        public string cog_merc { get; set; }
        public string cog_prod { get; set; }
        public string cog_serv { get; set; }
        public string cog_vari { get; set; }
        public string cog_omag { get; set; }
        public string cog_cass { get; set; }
        public string cog_nfat { get; set; }
        public string cog_ncre { get; set; }
        public string cog_grup { get; set; }
        public string cos_c { get; set; }
        public string cos_p { get; set; }
        public bool? use_pacq { get; set; }
        public string dbf_gate { get; set; }
        public bool? gestionelotti { get; set; }
        public bool? movimentazionesulottiscaduti { get; set; }
        public bool? cdc_accredito { get; set; }
        public bool? cdc_addebito { get; set; }
        public bool? magazzinocontoterzi { get; set; }
        public bool? movimentazionecontestuale { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool? cau_stasegno { get; set; }
        public string cod_nume { get; set; }
        public bool? mostrasuvenditatouch { get; set; }
        public string cog_inc { get; set; }
        public bool? cau_resocauz { get; set; }
        public string cog_cauz { get; set; }
        public bool? ddt_to { get; set; }
        public bool? fat_to { get; set; }
        public bool? genera_fat_auto { get; set; }
        public int? top_causid_trasddt { get; set; }
        public int? top_causid_trasfat { get; set; }

        public virtual top_caus top_causid_trasddtNavigation { get; set; }
        public virtual top_caus top_causid_trasfatNavigation { get; set; }
        public virtual top_maga top_maga { get; set; }
        public virtual ICollection<top_caus> Inversetop_causid_trasddtNavigation { get; set; }
        public virtual ICollection<top_caus> Inversetop_causid_trasfatNavigation { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
    }
}
