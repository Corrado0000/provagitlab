﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_pre_gene_tot
    {
        public int? pre_geneid { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? qta_tota { get; set; }
        public string sel_arti { get; set; }
        public string cod_prez { get; set; }
        public decimal? iva_perc { get; set; }
        public decimal? tot_impo { get; set; }
        public string iva_code { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public decimal? tot_scontato { get; set; }
        public string art_item { get; set; }
    }
}
