﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vword_arti_view
    {
        public int? ord_artiid { get; set; }
        public int? ord_geneid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string art_misu { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? qta_logi { get; set; }
        public decimal? pre_impo { get; set; }
        public string sco_text { get; set; }
        public decimal? iva_perc { get; set; }
        public bool? enabled { get; set; }
        public int? indice { get; set; }
    }
}
