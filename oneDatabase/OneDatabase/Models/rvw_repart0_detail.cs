﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_repart0_detail
    {
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public int? ana_artiid { get; set; }
        public string expr1 { get; set; }
        public string mar_arti { get; set; }
        public string sco_text { get; set; }
        public decimal? pre_vend { get; set; }
        public decimal? pre_acqu { get; set; }
        public decimal? qta_giac { get; set; }
        public string key_anno { get; set; }
        public int? top_listid { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? qta_entr { get; set; }
        public decimal? qta_usci { get; set; }
        public string des_list { get; set; }
        public string for_arti { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
    }
}
