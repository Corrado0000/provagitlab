﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class codice_carica
    {
        public int? codice_caricaid { get; set; }
        public string descrizione_carica { get; set; }
    }
}
