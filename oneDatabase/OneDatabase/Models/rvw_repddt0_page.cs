﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_repddt0_page
    {
        public int? ddt_geneid { get; set; }
        public string key_anno { get; set; }
        public string num_boll { get; set; }
        public DateTime? dat_boll { get; set; }
        public int? age_nomiid { get; set; }
        public string rag_soci { get; set; }
        public string cod_caus { get; set; }
        public decimal? tot_docu { get; set; }
        public bool? enabled { get; set; }
        public int? top_magaid { get; set; }
        public string cod_maga { get; set; }
        public string des_maga { get; set; }
        public int? top_causid { get; set; }
        public string des_caus { get; set; }
        public string rap_1 { get; set; }
        public string rap_2 { get; set; }
        public string controllo_documento { get; set; }
        public string num_comm { get; set; }
        public string indirizzo { get; set; }
        public string civico { get; set; }
        public string cap { get; set; }
        public string localita { get; set; }
        public string provincia { get; set; }
        public string filiale { get; set; }
        public string filiale_indirizzo { get; set; }
        public string filiale_civico { get; set; }
        public string filiale_cap { get; set; }
        public string filiale_localita { get; set; }
        public string filiale_provincia { get; set; }
        public string ora { get; set; }
        public string telefono { get; set; }
        public string filtro_4 { get; set; }
        public string filtro_5 { get; set; }
        public string descrizione { get; set; }
        public string zona { get; set; }
        public string codice_pin { get; set; }
        public string evaso { get; set; }
        public int? fatturato { get; set; }
        public int? non_fatturato { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public int? age_nomiidrap_2 { get; set; }
    }
}
