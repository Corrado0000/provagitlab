﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_giacenzemagazzino
    {
        public int? ana_artiid { get; set; }
        public int? top_magaid { get; set; }
        public string key_anno { get; set; }
        public decimal? qta_entr { get; set; }
        public decimal? qta_usci { get; set; }
        public decimal? qta_impe { get; set; }
        public decimal? qta_ordi { get; set; }
        public decimal? qta_giac { get; set; }
        public decimal? qta_disp { get; set; }
    }
}
