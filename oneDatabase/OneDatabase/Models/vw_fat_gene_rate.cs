﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_fat_gene_rate
    {
        public int? fat_gene_rateid { get; set; }
        public int? fat_geneid { get; set; }
        public int? tip_pagaid { get; set; }
        public string desc_pagam { get; set; }
        public DateTime? dat_scad { get; set; }
        public decimal? importo { get; set; }
        public decimal? importo_pagato { get; set; }
    }
}
