﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_tipotavolo
    {
        public ho_tipotavolo()
        {
            ho_tavoloanagrafica = new HashSet<ho_tavoloanagrafica>();
        }

        public int ho_tipotavoloid { get; set; }
        public string tipnometavolo { get; set; }
        public int? tipnumpostitavolo { get; set; }
        public decimal? lunghezza_metri { get; set; }
        public decimal? larghezza_metri { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<ho_tavoloanagrafica> ho_tavoloanagrafica { get; set; }
    }
}
