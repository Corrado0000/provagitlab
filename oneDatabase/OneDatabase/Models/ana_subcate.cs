﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_subcate
    {
        public ana_subcate()
        {
            ho_configurazionestampante = new HashSet<ho_configurazionestampante>();
            ho_contopiet = new HashSet<ho_contopiet>();
            ho_mapaggiunta = new HashSet<ho_mapaggiunta>();
            ho_mapmenuprod = new HashSet<ho_mapmenuprod>();
        }

        public int ana_subcateid { get; set; }
        public int ana_cateid { get; set; }
        public string cod_subcate { get; set; }
        public string desc_subcate { get; set; }
        public bool ismenu { get; set; }
        public decimal? prezzomenu { get; set; }
        public int? indice { get; set; }
        public int? age_tivaid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_cate ana_cate { get; set; }
        public virtual ICollection<ho_configurazionestampante> ho_configurazionestampante { get; set; }
        public virtual ICollection<ho_contopiet> ho_contopiet { get; set; }
        public virtual ICollection<ho_mapaggiunta> ho_mapaggiunta { get; set; }
        public virtual ICollection<ho_mapmenuprod> ho_mapmenuprod { get; set; }
    }
}
