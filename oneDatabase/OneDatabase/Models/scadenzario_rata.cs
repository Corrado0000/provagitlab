﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class scadenzario_rata
    {
        public scadenzario_rata()
        {
            scadenzario_modifica = new HashSet<scadenzario_modifica>();
        }

        public int scadenzario_rataid { get; set; }
        public int? scadenzarioid { get; set; }
        public int? indice { get; set; }
        public DateTime? scadenza { get; set; }
        public int? tip_pagaid { get; set; }
        public decimal? importo { get; set; }
        public decimal? importopagato { get; set; }
        public string numerodistinta { get; set; }
        public DateTime? datadistinta { get; set; }
        public string numerodisposizione { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool? bloccata { get; set; }

        public virtual scadenzario scadenzario { get; set; }
        public virtual ICollection<scadenzario_modifica> scadenzario_modifica { get; set; }
    }
}
