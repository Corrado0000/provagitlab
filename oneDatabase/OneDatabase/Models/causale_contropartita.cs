﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class causale_contropartita
    {
        public int causale_contropartitaid { get; set; }
        public int? causaleid { get; set; }
        public int? indice { get; set; }
        public int? pianodeicontiid { get; set; }
        public int? age_nomiid { get; set; }
        public bool? dare { get; set; }
        public bool? iva { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
