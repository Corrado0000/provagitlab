﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwcommessa_nomi_view
    {
        public int? age_commessaid { get; set; }
        public int? age_nomiid { get; set; }
        public string cod_commessa { get; set; }
        public string descr_commessa { get; set; }
    }
}
