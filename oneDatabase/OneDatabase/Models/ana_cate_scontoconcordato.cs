﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_cate_scontoconcordato
    {
        public int ana_cate_scontoconcordatoid { get; set; }
        public int? ana_cateid { get; set; }
        public int? age_nomiid { get; set; }
        public decimal? sconto_da { get; set; }
        public decimal? sconto_a { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual ana_cate ana_cate { get; set; }
    }
}
