﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class causale
    {
        public causale()
        {
            causale_iva = new HashSet<causale_iva>();
            movimento = new HashSet<movimento>();
        }

        public int causaleid { get; set; }
        public string anno { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public int? tipomovimentoid { get; set; }
        public int? registroivaid { get; set; }
        public bool? registroivamodificabile { get; set; }
        public bool? documento { get; set; }
        public bool? scadenzario { get; set; }
        public bool? autofattura { get; set; }
        public bool? clienti { get; set; }
        public bool? fornitori { get; set; }
        public bool? vettori { get; set; }
        public string descrizioneprimanota { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string tipoquadro { get; set; }
        public bool? ivanoesposta { get; set; }
        public bool? nole_leasing { get; set; }
        public string letteranol { get; set; }
        public bool? fisc_privileg { get; set; }
        public bool? splitpayment { get; set; }
        public bool? ratei_risconti { get; set; }

        public virtual registroiva registroiva { get; set; }
        public virtual tipomovimento tipomovimento { get; set; }
        public virtual ICollection<causale_iva> causale_iva { get; set; }
        public virtual ICollection<movimento> movimento { get; set; }
    }
}
