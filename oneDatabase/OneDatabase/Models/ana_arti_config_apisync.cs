﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_arti_config_apisync
    {
        public int ana_arti_config_apisyncid { get; set; }
        public int ana_artiid { get; set; }
        public int config_apisyncid { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual config_apisync config_apisync { get; set; }
    }
}
