﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class appuser_ana_cate
    {
        public int appuser_ana_cateid { get; set; }
        public int? appuserid { get; set; }
        public int? ana_cateid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
