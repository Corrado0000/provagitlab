﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fel_progr
    {
        public int fel_progrid { get; set; }
        public int? fat_geneid { get; set; }
        public string progressivo { get; set; }

        public virtual fat_gene fat_gene { get; set; }
    }
}
