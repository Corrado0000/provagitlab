﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_user
    {
        public int age_userid { get; set; }
        public int? appuserid { get; set; }
        public string cod_tipo { get; set; }
        public string cod_user { get; set; }
        public string cod_grup { get; set; }
        public string pas_word { get; set; }
        public decimal? liv_acce { get; set; }
        public string liv_init { get; set; }
        public int? age_nomiid { get; set; }
        public string cod_rapp { get; set; }
        public decimal? amm_logi { get; set; }
        public decimal? com_logi { get; set; }
        public decimal? tec_logi { get; set; }
        public decimal? rap_logi { get; set; }
        public decimal? int_logi { get; set; }
        public decimal? rip_logi { get; set; }
        public decimal? web_logi { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
    }
}
