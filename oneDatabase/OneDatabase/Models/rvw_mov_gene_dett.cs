﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_mov_gene_dett
    {
        public int? mov_geneid { get; set; }
        public int? doc_artiid { get; set; }
        public string key_anno { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string art_misu { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_impo { get; set; }
        public string sco_text { get; set; }
        public decimal? iva_perc { get; set; }
        public string iva_code { get; set; }
        public string art_item { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? subtotale { get; set; }
        public decimal? ivato { get; set; }
        public string des_plus { get; set; }
    }
}
