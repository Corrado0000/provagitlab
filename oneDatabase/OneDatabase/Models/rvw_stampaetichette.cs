﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_stampaetichette
    {
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public int? ana_artiid { get; set; }
        public string expr1 { get; set; }
        public string mar_arti { get; set; }
        public string sco_text { get; set; }
        public decimal? pre_vend { get; set; }
        public decimal? pre_acqu { get; set; }
        public string des_plus { get; set; }
        public string dbf_gate { get; set; }
        public int? top_listid { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? iva_perc { get; set; }
        public int? ana_marcaid { get; set; }
        public int? age_tivaid { get; set; }
        public string rif_arti { get; set; }
    }
}
