﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rpcauzionidarendere_pre_arti
    {
        public int? pre_geneid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_tota { get; set; }
        public string art_misu { get; set; }
    }
}
