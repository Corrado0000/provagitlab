﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_ubicazione
    {
        public int ana_ubicazioneid { get; set; }
        public int? ana_artiid { get; set; }
        public int? top_magaid { get; set; }
        public int? top_maga_settoreid { get; set; }
        public int? top_maga_scaffaleid { get; set; }
        public int? top_maga_posizioneid { get; set; }
        public bool? entrata { get; set; }
        public bool? uscita { get; set; }
        public int? doc_artiid { get; set; }
        public int? pre_artiid { get; set; }
        public int? ord_artiid { get; set; }
        public decimal? quantitamovimentoentrata { get; set; }
        public decimal? quantitamovimentouscita { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string note { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual doc_arti doc_arti { get; set; }
        public virtual ord_arti ord_arti { get; set; }
        public virtual pre_arti pre_arti { get; set; }
        public virtual top_maga top_maga { get; set; }
        public virtual top_maga_posizione top_maga_posizione { get; set; }
        public virtual top_maga_scaffale top_maga_scaffale { get; set; }
        public virtual top_maga_settore top_maga_settore { get; set; }
    }
}
