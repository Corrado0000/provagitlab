﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_diba
    {
        public int ana_dibaid { get; set; }
        public int? ana_artiid { get; set; }
        public string cod_comp { get; set; }
        public string sel_comp { get; set; }
        public string des_comp { get; set; }
        public string qta_gest { get; set; }
        public decimal? qta_tota { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
    }
}
