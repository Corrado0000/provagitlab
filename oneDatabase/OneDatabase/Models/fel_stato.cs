﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fel_stato
    {
        public fel_stato()
        {
            fel_fat_gene_stato = new HashSet<fel_fat_gene_stato>();
        }

        public int fel_statoid { get; set; }
        public string cod_stato { get; set; }
        public string des_stato { get; set; }
        public char? ciclo { get; set; }

        public virtual ICollection<fel_fat_gene_stato> fel_fat_gene_stato { get; set; }
    }
}
