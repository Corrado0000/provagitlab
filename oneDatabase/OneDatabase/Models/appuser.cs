﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class appuser
    {
        public int appuserid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int? userpin { get; set; }
        public int? contactid { get; set; }
        public bool enabled { get; set; }
        public DateTime? passwordexpiredate { get; set; }
        public DateTime? userexpiredate { get; set; }
        public bool iswindowsuser { get; set; }
        public bool? requirepckey { get; set; }
        public string pckey { get; set; }
    }
}
