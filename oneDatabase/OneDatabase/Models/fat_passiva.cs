﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fat_passiva
    {
        public fat_passiva()
        {
            fat_passiva_rate = new HashSet<fat_passiva_rate>();
        }

        public int fat_passivaid { get; set; }
        public string external_id { get; set; }
        public string external_path { get; set; }
        public string tipoprovider { get; set; }
        public string par_tiva { get; set; }
        public string cod_fisc { get; set; }
        public string denom_mitt { get; set; }
        public string tip_doc { get; set; }
        public string descrizione { get; set; }
        public string num_fatt { get; set; }
        public DateTime dat_fatt { get; set; }
        public string key_anno { get; set; }
        public decimal importo { get; set; }
        public string num_doc { get; set; }
        public string rif_amm { get; set; }
        public DateTime dat_ricez { get; set; }
        public string payloadjson { get; set; }
        public int creationuser { get; set; }
        public DateTime creationdate { get; set; }
        public int lastmodifyuser { get; set; }
        public DateTime lastmodifydate { get; set; }
        public bool enabled { get; set; }

        public virtual ICollection<fat_passiva_rate> fat_passiva_rate { get; set; }
    }
}
