﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class view_aggiornaprezzi
    {
        public decimal? pre_acqu { get; set; }
        public decimal? pre_impo { get; set; }
        public string cod_arti { get; set; }
        public decimal? net_acqu { get; set; }
    }
}
