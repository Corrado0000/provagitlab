﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class configurazione
    {
        public int configurazioneid { get; set; }
        public string anno { get; set; }
        public int? pianodeiconti_clieid { get; set; }
        public int? pianodeiconti_fornid { get; set; }
        public int? pianodeiconti_cassid { get; set; }
        public int? pianodeiconti_vendid { get; set; }
        public int? pianodeiconti_acquid { get; set; }
        public int? pianodeiconti_erarid { get; set; }
        public int? pianodeiconti_nondid { get; set; }
        public int? pianodeiconti_sospid { get; set; }
        public string tipoiva { get; set; }
        public int? pianodeiconti_patrid { get; set; }
        public int? pianodeiconti_econid { get; set; }
        public int? causale_cdareid { get; set; }
        public int? causale_cavereid { get; set; }
        public int? pianodeiconti_cutilid { get; set; }
        public int? pianodeiconti_cperdid { get; set; }
        public int? causale_adareid { get; set; }
        public int? causale_aavereid { get; set; }
        public int? pianodeiconti_autilid { get; set; }
        public int? pianodeiconti_aperdid { get; set; }
        public decimal? progressivoprimanota { get; set; }
        public decimal? giornale_mese { get; set; }
        public decimal? giornale_ultimoprogressivo { get; set; }
        public decimal? giornale_ultimapagina { get; set; }
        public bool? giornale_stampanumeropagina { get; set; }
        public decimal? giornale_dare { get; set; }
        public decimal? giornale_avere { get; set; }
        public decimal? riepilogo_mese { get; set; }
        public decimal? riepilogo_ultimapagina { get; set; }
        public bool? riepilogo_stampanumeropagina { get; set; }
        public decimal? ivaannoprecedente { get; set; }
        public decimal? ivagennaio { get; set; }
        public decimal? ivafebbraio { get; set; }
        public decimal? ivamarzo { get; set; }
        public decimal? ivaaprile { get; set; }
        public decimal? ivamaggio { get; set; }
        public decimal? ivagiugno { get; set; }
        public decimal? ivaluglio { get; set; }
        public decimal? ivaagosto { get; set; }
        public decimal? ivasettembre { get; set; }
        public decimal? ivaottobre { get; set; }
        public decimal? ivanovembre { get; set; }
        public decimal? ivadicembre { get; set; }
        public int? liquidazioneultimomese { get; set; }
        public string modalitastampaliqregistroiva { get; set; }
        public int? registroiva_liqid { get; set; }
        public int? contabilizzazionetop_causid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual registroiva registroiva_liq { get; set; }
    }
}
