﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class move_gene_taglie
    {
        public int id { get; set; }
        public int? doc_artiid { get; set; }
        public int? ana_artiid { get; set; }
        public string taglia { get; set; }
        public string quantita { get; set; }
        public DateTime? data { get; set; }

        public virtual doc_arti doc_arti { get; set; }
    }
}
