﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_cate
    {
        public age_cate()
        {
            age_nomi = new HashSet<age_nomi>();
        }

        public int age_cateid { get; set; }
        public string cod_cate { get; set; }
        public int? age_prefid { get; set; }
        public string suf_pref { get; set; }
        public string des_cate { get; set; }
        public decimal? pro_cate { get; set; }
        public int? top_listid { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_pref age_pref { get; set; }
        public virtual top_list top_list { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
    }
}
