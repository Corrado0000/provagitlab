﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fat_gene_rate_pagamenti
    {
        public int fat_gene_rate_pagamentiid { get; set; }
        public int fat_gene_rateid { get; set; }
        public DateTime dat_paga { get; set; }
        public int? tip_pagaid { get; set; }
        public decimal importo { get; set; }
        public string note { get; set; }
        public int creationuser { get; set; }
        public DateTime creationdate { get; set; }
        public int lastmodifyuser { get; set; }
        public DateTime lastmodifydate { get; set; }
        public bool enabled { get; set; }

        public virtual fat_gene_rate fat_gene_rate { get; set; }
        public virtual tip_paga tip_paga { get; set; }
    }
}
