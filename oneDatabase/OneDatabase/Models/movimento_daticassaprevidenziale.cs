﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class movimento_daticassaprevidenziale
    {
        public int movimento_daticassaprevidenzialeid { get; set; }
        public int movimentoid { get; set; }
        public decimal? alcassa { get; set; }
        public decimal? aliquotaiva { get; set; }
        public decimal? imponibilecassa { get; set; }
        public decimal? importocontributocassa { get; set; }
        public string natura { get; set; }
        public string riferimentoamministrazione { get; set; }
        public string ritenuta { get; set; }
        public string tipocassa { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
