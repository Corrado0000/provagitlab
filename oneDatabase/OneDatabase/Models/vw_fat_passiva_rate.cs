﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_fat_passiva_rate
    {
        public int? fat_passiva_rateid { get; set; }
        public int? fat_passivaid { get; set; }
        public string mod_paga { get; set; }
        public DateTime? dat_scad { get; set; }
        public decimal? importo { get; set; }
        public decimal? importo_pagato { get; set; }
    }
}
