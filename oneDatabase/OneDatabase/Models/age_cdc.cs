﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_cdc
    {
        public age_cdc()
        {
            Inverseparentage_cdc = new HashSet<age_cdc>();
            age_budgetcdc = new HashSet<age_budgetcdc>();
            ddt_gene = new HashSet<ddt_gene>();
            doc_arti = new HashSet<doc_arti>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_arti = new HashSet<ord_arti>();
            ord_gene = new HashSet<ord_gene>();
            pre_arti = new HashSet<pre_arti>();
            pre_gene = new HashSet<pre_gene>();
        }

        public int age_cdcid { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public int? age_nomiid { get; set; }
        public int? parentage_cdcid { get; set; }
        public int? responsabileage_contid { get; set; }
        public bool? sedeprimaria { get; set; }
        public int? sedeage_sediid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual age_cdc parentage_cdc { get; set; }
        public virtual age_cont responsabileage_cont { get; set; }
        public virtual age_sedi sedeage_sedi { get; set; }
        public virtual ICollection<age_cdc> Inverseparentage_cdc { get; set; }
        public virtual ICollection<age_budgetcdc> age_budgetcdc { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<doc_arti> doc_arti { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_arti> ord_arti { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_arti> pre_arti { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
    }
}
