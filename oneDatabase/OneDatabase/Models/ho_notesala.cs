﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_notesala
    {
        public int ho_notesalaid { get; set; }
        public int? ho_salaanagraficaid { get; set; }
        public string note { get; set; }
        public DateTime? data { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ho_salaanagrafica ho_salaanagrafica { get; set; }
    }
}
