﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_arti_ana_cate
    {
        public int ana_arti_ana_cateid { get; set; }
        public int? ana_artiid { get; set; }
        public int? ana_cateid { get; set; }
        public bool principale { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
