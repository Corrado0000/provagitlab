﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rpanacauz_darenderetotale
    {
        public string key_anno { get; set; }
        public int? age_nomiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public decimal? qta_da_rendere { get; set; }
        public string art_misu { get; set; }
    }
}
