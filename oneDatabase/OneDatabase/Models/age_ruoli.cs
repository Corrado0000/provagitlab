﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_ruoli
    {
        public int age_ruoliid { get; set; }
        public string des_ruolo { get; set; }
        public int? importanza { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string cod_pref { get; set; }
    }
}
