﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_mapmenuprod
    {
        public ho_mapmenuprod()
        {
            ho_configurazionestampante = new HashSet<ho_configurazionestampante>();
            ho_contopiet = new HashSet<ho_contopiet>();
        }

        public int ho_mapmenuprodid { get; set; }
        public int? id_anaarti { get; set; }
        public int? id_anacate { get; set; }
        public int? id_anasubcate { get; set; }
        public decimal? prezzoaggxmenu { get; set; }
        public string cod_group { get; set; }
        public int? venditecount { get; set; }
        public int? indice { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti id_anaartiNavigation { get; set; }
        public virtual ana_cate id_anacateNavigation { get; set; }
        public virtual ana_subcate id_anasubcateNavigation { get; set; }
        public virtual ICollection<ho_configurazionestampante> ho_configurazionestampante { get; set; }
        public virtual ICollection<ho_contopiet> ho_contopiet { get; set; }
    }
}
