﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class appuserpassword
    {
        public int appuserpasswordid { get; set; }
        public int? appuserid { get; set; }
        public string password { get; set; }
        public DateTime? TIMESTAMP { get; set; }
        public bool? enabled { get; set; }
    }
}
