﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_varianteconto
    {
        public int ho_variantecontoid { get; set; }
        public int? ho_contopietid { get; set; }
        public int? ana_artiid { get; set; }
        public int? distinta_baseid { get; set; }
        public int? ho_mapaggiuntaid { get; set; }
        public decimal? qta { get; set; }
        public bool? isrimozione { get; set; }
        public bool? isvariante { get; set; }
        public decimal? prezzo { get; set; }
        public decimal? subtotale { get; set; }
        public int? top_listid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual distinta_base distinta_base { get; set; }
        public virtual ho_contopiet ho_contopiet { get; set; }
    }
}
