﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_docorf0_page
    {
        public int? ord_geneid { get; set; }
        public string key_anno { get; set; }
        public string num_ordi { get; set; }
        public DateTime? dat_ordi { get; set; }
        public string num_comm { get; set; }
        public DateTime? commessa { get; set; }
        public int? top_causid { get; set; }
        public string des_valu { get; set; }
        public string cod_caus { get; set; }
        public string des_caus { get; set; }
        public string rag_soci { get; set; }
        public string rif_indi { get; set; }
        public string rif_nciv { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public string cod_paga { get; set; }
        public string des_paga { get; set; }
        public string des_bank { get; set; }
        public string abi_code { get; set; }
        public string cab_code { get; set; }
        public string con_bank { get; set; }
        public string con_iban { get; set; }
        public string doc_head { get; set; }
        public string doc_note { get; set; }
        public string doc_stat { get; set; }
        public decimal? tot_impo { get; set; }
        public decimal? tot_osta { get; set; }
        public decimal? tot_esen { get; set; }
        public decimal? tot_docu { get; set; }
        public string doc_tipo { get; set; }
        public string cod_vett { get; set; }
        public string vettore { get; set; }
        public string spe_tipo { get; set; }
        public string doc_rife { get; set; }
        public string log_10 { get; set; }
        public string log_9 { get; set; }
        public string log_8 { get; set; }
        public string log_7 { get; set; }
        public string log_6 { get; set; }
        public string log_5 { get; set; }
        public string log_4 { get; set; }
        public string log_3 { get; set; }
        public string log_2 { get; set; }
        public string log_1 { get; set; }
        public string des_prov { get; set; }
        public string des_citt { get; set; }
        public string des_avvi { get; set; }
        public string des_nciv { get; set; }
        public string des_indi { get; set; }
        public string des_nome { get; set; }
        public string doc_cont { get; set; }
        public decimal? sco_perc { get; set; }
    }
}
