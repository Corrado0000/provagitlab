﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class articoloclass
    {
        public int articoloclassid { get; set; }
        public string codarti { get; set; }
        public int? qta { get; set; }
        public int? taglia { get; set; }
        public string codicesito { get; set; }
        public string idgroup { get; set; }
    }
}
