﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_pre_arti_dett
    {
        public int? pre_geneid { get; set; }
        public int? pre_artiid { get; set; }
        public string key_anno { get; set; }
        public decimal? qta_totabo { get; set; }
        public decimal? pre_impo { get; set; }
        public string sco_text { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public int? ana_artiid { get; set; }
        public string des_plus { get; set; }
        public decimal? iva_perc { get; set; }
        public string iva_code { get; set; }
        public string art_item { get; set; }
        public decimal? subtotale { get; set; }
        public decimal? subtotalebo { get; set; }
        public string dbf_gate { get; set; }
        public int? indice { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public decimal? qta_coll { get; set; }
        public decimal? qta_tota { get; set; }
        public string art_misu { get; set; }
        public decimal? qta_tota_vendita { get; set; }
        public string art_misu_vendita { get; set; }
    }
}
