﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class oxproposte_incasso_doc
    {
        public int oxproposte_incasso_docid { get; set; }
        public int? oxproposte_incassoid { get; set; }
        public string num_fatt { get; set; }
        public int? num_rata { get; set; }
        public decimal? importo_coperto { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
