﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_gruppoordinazione
    {
        public ho_gruppoordinazione()
        {
            ana_arti = new HashSet<ana_arti>();
            ho_contopiet = new HashSet<ho_contopiet>();
        }

        public int ho_gruppoordinazioneid { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public int? indice { get; set; }
        public string colore { get; set; }
        public int? timer { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<ana_arti> ana_arti { get; set; }
        public virtual ICollection<ho_contopiet> ho_contopiet { get; set; }
    }
}
