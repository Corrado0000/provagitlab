﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_movimento_contropartita_segno
    {
        public int? movimento_contropartitaid { get; set; }
        public int? movimentoid { get; set; }
        public int? indice { get; set; }
        public int? pianodeicontiid { get; set; }
        public int? age_nomiid { get; set; }
        public char? segno { get; set; }
        public decimal? importo { get; set; }
        public bool? iva { get; set; }
        public string note { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool? escludi_ratei_risconti { get; set; }
        public bool? acquisto { get; set; }
        public int? causaleid { get; set; }
        public DateTime? datadocumento { get; set; }
        public DateTime? dataricezione { get; set; }
    }
}
