﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rpcauzionidarendere_doc_arti
    {
        public int? fat_geneid { get; set; }
        public int? ddt_geneid { get; set; }
        public int? mov_geneid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_tota { get; set; }
        public string art_misu { get; set; }
    }
}
