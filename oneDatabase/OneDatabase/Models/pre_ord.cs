﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class pre_ord
    {
        public int pre_ordid { get; set; }
        public int? ana_artiid { get; set; }
        public int? age_nomiid { get; set; }
        public int? qta_ord { get; set; }
        public DateTime? creationdate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public int? lastmodifyuser { get; set; }
        public bool? enabled { get; set; }
    }
}
