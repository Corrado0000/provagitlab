﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class park_sco
    {
        public int park_scoid { get; set; }
        public string nome { get; set; }
        public string snapshot { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
