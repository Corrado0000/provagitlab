﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class oxproposte_incasso
    {
        public int oxproposte_incassoid { get; set; }
        public int? oxid { get; set; }
        public string cod_agen { get; set; }
        public string tip_pagacodice { get; set; }
        public decimal? importo { get; set; }
        public DateTime? data_incasso { get; set; }
        public string note { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
