﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_fat_gene
    {
        public int? fat_geneid { get; set; }
        public int? num_fatt_number { get; set; }
        public string num_fatt { get; set; }
        public DateTime? dat_fatt { get; set; }
        public string cod_caus { get; set; }
        public string cod_maga { get; set; }
        public string rag_soci { get; set; }
        public string cod_agen { get; set; }
        public string num_comm { get; set; }
        public DateTime? dat_comm { get; set; }
        public string num_docu { get; set; }
        public DateTime? dat_docu { get; set; }
        public string cod_bank { get; set; }
        public string des_bank { get; set; }
        public char? pre_nume { get; set; }
        public int? top_causid { get; set; }
        public int? top_magaid { get; set; }
        public int? age_nomiid { get; set; }
        public string doc_rife { get; set; }
        public bool? enabled { get; set; }
        public decimal? totale_imponibile { get; set; }
        public decimal? totale_imposta { get; set; }
        public decimal? totale_documento { get; set; }
        public int? creationuser { get; set; }
        public string des_caus { get; set; }
        public string fel_cod_stato { get; set; }
        public string fel_des_stato { get; set; }
        public int? stato_pag { get; set; }
        public decimal? importo_pag { get; set; }
        public decimal? importo_dapag { get; set; }
    }
}
