﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class config_apisync
    {
        public config_apisync()
        {
            ana_arti_config_apisync = new HashSet<ana_arti_config_apisync>();
            apisync_user = new HashSet<apisync_user>();
        }

        public int config_apisyncid { get; set; }
        public string tipoapi { get; set; }
        public string url { get; set; }
        public string usr { get; set; }
        public string pwd { get; set; }
        public int causaleordineid { get; set; }
        public int top_listid { get; set; }
        public long? idultimoordineimportato { get; set; }
        public int? idshop { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<ana_arti_config_apisync> ana_arti_config_apisync { get; set; }
        public virtual ICollection<apisync_user> apisync_user { get; set; }
    }
}
