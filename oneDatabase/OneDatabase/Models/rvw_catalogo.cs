﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_catalogo
    {
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public int? ana_artiid { get; set; }
        public string gru_arti { get; set; }
        public string mar_arti { get; set; }
        public string sco_text { get; set; }
        public decimal? pre_vend { get; set; }
        public decimal? pre_acqu { get; set; }
        public string des_plus { get; set; }
        public string dbf_gate { get; set; }
        public int? top_listid { get; set; }
        public string cod_list { get; set; }
        public decimal? pre_impo { get; set; }
        public bool? enabled { get; set; }
        public decimal? pre_tota { get; set; }
    }
}
