﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class delete_vmage_cont_view
    {
        public int? age_contid { get; set; }
        public int? age_nomiid { get; set; }
        public string cnt_nome { get; set; }
        public string tel_nume { get; set; }
        public string tel_nfax { get; set; }
        public string tel_ncel { get; set; }
        public string cnt_mail { get; set; }
        public bool? enabled { get; set; }
    }
}
