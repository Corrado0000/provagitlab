﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class tipomovimento
    {
        public tipomovimento()
        {
            causale = new HashSet<causale>();
            registroiva_tipomovimento = new HashSet<registroiva_tipomovimento>();
        }

        public int tipomovimentoid { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public bool? gestioneiva { get; set; }
        public bool? acquisto { get; set; }
        public bool? movimentogenerico { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<causale> causale { get; set; }
        public virtual ICollection<registroiva_tipomovimento> registroiva_tipomovimento { get; set; }
    }
}
