﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class registroiva_tipomovimento
    {
        public int registroiva_tipomovimentoid { get; set; }
        public int? tipomovimentoid { get; set; }
        public int? registroivaid { get; set; }

        public virtual registroiva registroiva { get; set; }
        public virtual tipomovimento tipomovimento { get; set; }
    }
}
