﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_repnom0_detail
    {
        public int? age_nomiid { get; set; }
        public string rag_soci { get; set; }
        public string rap_1 { get; set; }
        public string rap_2 { get; set; }
        public string rif_indi { get; set; }
        public string rif_nciv { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public string tel_nume { get; set; }
        public string tel_nfax { get; set; }
        public string rif_mail { get; set; }
        public string rif_iweb { get; set; }
        public decimal? cli_pref { get; set; }
        public decimal? for_pref { get; set; }
        public decimal? vet_pref { get; set; }
        public int? age_prefid { get; set; }
        public int? age_cateid { get; set; }
        public string des_cate { get; set; }
        public string des_pref { get; set; }
        public string cod_agen { get; set; }
    }
}
