﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class livello
    {
        public int livelloid { get; set; }
        public string anno { get; set; }
        public int? indice { get; set; }
        public string espressione { get; set; }
        public string descrizione { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
