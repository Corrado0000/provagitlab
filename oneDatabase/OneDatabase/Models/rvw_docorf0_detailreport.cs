﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_docorf0_detailreport
    {
        public int? ord_artiid { get; set; }
        public string key_anno { get; set; }
        public int? ord_geneid { get; set; }
        public int? ana_artiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string des_plus { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? qta_totabo { get; set; }
        public decimal? pre_impo { get; set; }
        public string sco_text { get; set; }
        public string iva_perc { get; set; }
        public string art_item { get; set; }
        public decimal? subtotale { get; set; }
        public decimal? subtotalebo { get; set; }
        public string for_arti { get; set; }
        public string art_misu { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string des_arti_eng { get; set; }
    }
}
