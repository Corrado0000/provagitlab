﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class tip_paga
    {
        public tip_paga()
        {
            age_paga = new HashSet<age_paga>();
            fat_gene_rate = new HashSet<fat_gene_rate>();
            fat_gene_rate_pagamenti = new HashSet<fat_gene_rate_pagamenti>();
            fat_passiva_rate_pagamenti = new HashSet<fat_passiva_rate_pagamenti>();
        }

        public int tip_pagaid { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public string categoria { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool? ishoreca { get; set; }
        public string cog_inc_contropartita { get; set; }

        public virtual ICollection<age_paga> age_paga { get; set; }
        public virtual ICollection<fat_gene_rate> fat_gene_rate { get; set; }
        public virtual ICollection<fat_gene_rate_pagamenti> fat_gene_rate_pagamenti { get; set; }
        public virtual ICollection<fat_passiva_rate_pagamenti> fat_passiva_rate_pagamenti { get; set; }
    }
}
