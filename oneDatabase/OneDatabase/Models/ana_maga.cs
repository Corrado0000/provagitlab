﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_maga
    {
        public int ana_magaid { get; set; }
        public string key_anno { get; set; }
        public int? ana_artiid { get; set; }
        public int? top_magaid { get; set; }
        public decimal? qta_inve { get; set; }
        public decimal? qta_entr { get; set; }
        public decimal? qta_usci { get; set; }
        public decimal? qta_giac { get; set; }
        public decimal? qta_impe { get; set; }
        public decimal? qta_ordi { get; set; }
        public decimal? qta_disp { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual top_maga top_maga { get; set; }
    }
}
