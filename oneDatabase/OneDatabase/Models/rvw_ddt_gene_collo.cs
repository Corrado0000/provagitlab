﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_ddt_gene_collo
    {
        public string rag_soci { get; set; }
        public string num_boll { get; set; }
        public DateTime? dat_boll { get; set; }
        public int? ddt_geneid { get; set; }
        public decimal? spe_peso { get; set; }
        public string doc_rife { get; set; }
        public decimal? spe_coll { get; set; }
        public string doc_note { get; set; }
        public string des_nome { get; set; }
        public string des_indi { get; set; }
        public string des_citt { get; set; }
        public string des_prov { get; set; }
        public string des_caus { get; set; }
    }
}
