﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fel_fat_gene_stato
    {
        public int fel_fat_gene_statoid { get; set; }
        public int? fat_geneid { get; set; }
        public int? fel_statoid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual fat_gene fat_gene { get; set; }
        public virtual fel_stato fel_stato { get; set; }
    }
}
