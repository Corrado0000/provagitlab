﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class configuration
    {
        public int configurationid { get; set; }
        public string category { get; set; }
        public string name { get; set; }
        public string version { get; set; }
        public string value { get; set; }
    }
}
