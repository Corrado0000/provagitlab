﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_puntivendita_movimenti
    {
        public int? mov_geneid { get; set; }
        public string key_anno { get; set; }
        public string num_movi { get; set; }
        public DateTime? dat_movi { get; set; }
        public string num_logi { get; set; }
        public int? top_causid { get; set; }
        public string cod_caus { get; set; }
        public int? top_magaid { get; set; }
        public string cod_maga { get; set; }
        public int? top_listid { get; set; }
        public string cod_list { get; set; }
        public bool? enabled { get; set; }
    }
}
