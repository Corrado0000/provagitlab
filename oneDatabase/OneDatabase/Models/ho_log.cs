﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_log
    {
        public int ho_logid { get; set; }
        public string azione { get; set; }
        public string testolog { get; set; }
        public string scontrino { get; set; }
        public string stampante { get; set; }
        public int? id_tavoloanagrafica { get; set; }
        public int? id_tavoloanagrafica_out { get; set; }
        public int? id_contopiet { get; set; }
        public int? indicechiusura { get; set; }
        public int? id_prenotazione { get; set; }
        public int? id_salaoperazione { get; set; }
        public string id_groupconto { get; set; }
        public string nomepostazione { get; set; }
        public string extensiondata { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public bool? enabled { get; set; }

        public virtual ho_contopiet id_contopietNavigation { get; set; }
        public virtual ho_prenotazioni id_prenotazioneNavigation { get; set; }
        public virtual ho_tavoloanagrafica id_tavoloanagraficaNavigation { get; set; }
        public virtual ho_tavoloanagrafica id_tavoloanagrafica_outNavigation { get; set; }
    }
}
