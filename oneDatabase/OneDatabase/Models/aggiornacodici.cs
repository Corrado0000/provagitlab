﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class aggiornacodici
    {
        public string rif_arti { get; set; }
        public string attuale { get; set; }
        public string nuovo { get; set; }
        public string cod_arti { get; set; }
    }
}
