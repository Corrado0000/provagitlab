﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rpfat_view_totali
    {
        public int? fat_geneid { get; set; }
        public string num_fatt { get; set; }
        public DateTime? dat_fatt { get; set; }
        public string des_caus { get; set; }
        public string des_paga { get; set; }
        public string abi_code { get; set; }
        public string cab_code { get; set; }
        public string des_bank { get; set; }
        public string spo_bank { get; set; }
        public string con_bank { get; set; }
        public string con_iban { get; set; }
        public string par_tiva { get; set; }
        public string cod_fisc { get; set; }
        public string rag_soci { get; set; }
        public string rif_indi { get; set; }
        public string rif_nciv { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public string spe_port { get; set; }
        public string spe_aspe { get; set; }
        public string rap_1 { get; set; }
        public string rap_2 { get; set; }
        public string key_anno { get; set; }
        public decimal? tot_arti { get; set; }
        public decimal? tot_scon { get; set; }
        public decimal? tot_merc { get; set; }
        public decimal? tot_prod { get; set; }
        public decimal? tot_serv { get; set; }
        public decimal? tot_vari { get; set; }
        public decimal? tot_omag { get; set; }
        public decimal? tot_spes { get; set; }
        public decimal? tot_impo { get; set; }
        public decimal? tot_osta { get; set; }
        public decimal? tot_esen { get; set; }
        public decimal? tot_docu { get; set; }
        public decimal? tot_movi { get; set; }
        public decimal? tot_abbu { get; set; }
        public decimal? tot_acco { get; set; }
        public decimal? tot_paga { get; set; }
        public string des_valu { get; set; }
        public decimal? cos_tras { get; set; }
        public decimal? cos_inca { get; set; }
        public decimal? cos_imba { get; set; }
        public decimal? cos_acce { get; set; }
        public decimal? cos_boll { get; set; }
        public string doc_note { get; set; }
        public string des_nome { get; set; }
        public string des_indi { get; set; }
        public string des_nciv { get; set; }
        public string des_avvi { get; set; }
        public string des_citt { get; set; }
        public string des_prov { get; set; }
        public decimal? spe_coll { get; set; }
        public decimal? spe_peso { get; set; }
        public decimal? spe_cuba { get; set; }
        public DateTime? spe_data { get; set; }
        public string spe_time { get; set; }
        public string doc_rife { get; set; }
        public string vettore { get; set; }
        public string spe_tipo { get; set; }
        public string cod_exte { get; set; }
        public decimal? sco_perc { get; set; }
        public string num_comm { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
        public decimal? pag_rate { get; set; }
        public decimal? pag_iniz { get; set; }
        public decimal? pag_perc { get; set; }
        public decimal? pag_step { get; set; }
        public decimal? pag_gfis { get; set; }
        public decimal? pag_spes { get; set; }
        public decimal? pag_scon { get; set; }
        public string pag_escl { get; set; }
        public string pag_mese { get; set; }
        public string pag_ivam { get; set; }
        public string pag_spem { get; set; }
        public decimal? pag_skip { get; set; }
        public int? top_causid { get; set; }
        public int? iva_trasid { get; set; }
        public string iva_code { get; set; }
        public decimal? iva_perc { get; set; }
        public string cod_agen { get; set; }
        public decimal? tot_cauz { get; set; }
        public string rap_1_tel_nume { get; set; }
    }
}
