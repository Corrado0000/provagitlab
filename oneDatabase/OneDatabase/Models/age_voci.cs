﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_voci
    {
        public int age_vociid { get; set; }
        public char? cod_voce { get; set; }
        public string des_voce { get; set; }
        public string log_voce { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
