﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_cronologiadocumenti
    {
        public string docid { get; set; }
        public string tipo { get; set; }
        public int? geneid { get; set; }
        public string num_docu { get; set; }
        public DateTime? dat_docu { get; set; }
        public string key_anno { get; set; }
        public decimal? tot_docu { get; set; }
        public int? ana_artiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_tota { get; set; }
        public int? fidelityage_nomiid { get; set; }
        public int? age_nomiid { get; set; }
        public int? top_causid { get; set; }
        public string des_caus { get; set; }
        public string cod_caus { get; set; }
        public decimal? cau_usci { get; set; }
    }
}
