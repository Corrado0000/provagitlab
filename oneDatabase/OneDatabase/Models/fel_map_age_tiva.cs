﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fel_map_age_tiva
    {
        public int fel_map_age_tivaid { get; set; }
        public int age_tivaid { get; set; }
        public string natura { get; set; }
        public string riferimento_normativo { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
