﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_configurazionestampante
    {
        public int ho_configurazionestampanteid { get; set; }
        public int? id_sala { get; set; }
        public int? id_ana_cate { get; set; }
        public int? id_ana_subcate { get; set; }
        public int? id_mapmenuprod { get; set; }
        public string id_stampante { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_cate id_ana_cateNavigation { get; set; }
        public virtual ana_subcate id_ana_subcateNavigation { get; set; }
        public virtual ho_mapmenuprod id_mapmenuprodNavigation { get; set; }
        public virtual ho_salaanagrafica id_salaNavigation { get; set; }
    }
}
