﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_fat_gene_rate_pagamenti
    {
        public int? fat_gene_rate_pagamentiid { get; set; }
        public int? fat_gene_rateid { get; set; }
        public DateTime? dat_paga { get; set; }
        public int? tip_pagaid { get; set; }
        public string desc_pagam { get; set; }
        public decimal? importo { get; set; }
        public string note { get; set; }
        public DateTime? data_reg { get; set; }
    }
}
