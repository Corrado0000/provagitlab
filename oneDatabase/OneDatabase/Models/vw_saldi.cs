﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_saldi
    {
        public int? movimentoid { get; set; }
        public decimal? saldi { get; set; }
    }
}
