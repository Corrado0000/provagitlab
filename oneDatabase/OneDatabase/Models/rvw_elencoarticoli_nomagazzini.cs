﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_elencoarticoli_nomagazzini
    {
        public int? ana_artiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public int? top_magaid { get; set; }
        public string key_anno { get; set; }
        public string gru_arti { get; set; }
        public string marca { get; set; }
        public string art_misu { get; set; }
        public decimal? net_acqu { get; set; }
        public decimal? pre_vend { get; set; }
        public bool? enabled { get; set; }
        public decimal? inventario { get; set; }
        public decimal? caricati { get; set; }
        public decimal? scaricati { get; set; }
        public decimal? giacenza { get; set; }
        public decimal? val_magazzino { get; set; }
        public decimal? val_pre_vendita { get; set; }
    }
}
