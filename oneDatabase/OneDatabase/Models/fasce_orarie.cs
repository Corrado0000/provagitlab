﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fasce_orarie
    {
        public int fasce_orarieid { get; set; }
        public string daora { get; set; }
        public string aora { get; set; }
        public bool? enabled { get; set; }
        public int? lastmodifyuser { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public DateTime? lastmodifydate { get; set; }
    }
}
