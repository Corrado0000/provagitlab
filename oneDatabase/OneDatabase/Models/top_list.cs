﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class top_list
    {
        public top_list()
        {
            age_cate = new HashSet<age_cate>();
            age_nomi = new HashSet<age_nomi>();
            ana_list = new HashSet<ana_list>();
            ana_marca_top_list = new HashSet<ana_marca_top_list>();
            ddt_gene = new HashSet<ddt_gene>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            pre_gene = new HashSet<pre_gene>();
            top_gene = new HashSet<top_gene>();
        }

        public int top_listid { get; set; }
        public string cod_list { get; set; }
        public string des_list { get; set; }
        public string cod_prez { get; set; }
        public string cod_arro { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool listinodefault { get; set; }

        public virtual ICollection<age_cate> age_cate { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
        public virtual ICollection<ana_list> ana_list { get; set; }
        public virtual ICollection<ana_marca_top_list> ana_marca_top_list { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
        public virtual ICollection<top_gene> top_gene { get; set; }
    }
}
