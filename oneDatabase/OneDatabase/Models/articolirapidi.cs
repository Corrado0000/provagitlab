﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class articolirapidi
    {
        public int ana_artiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string gru_arti { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
