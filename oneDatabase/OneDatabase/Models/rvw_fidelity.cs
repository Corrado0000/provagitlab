﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_fidelity
    {
        public decimal? ivato { get; set; }
        public int? fidelity1 { get; set; }
        public int? fidelity2 { get; set; }
        public string codice { get; set; }
        public string cliente { get; set; }
        public string anno { get; set; }
        public DateTime? data_ora { get; set; }
        public string note_articolo { get; set; }
        public int? movimento { get; set; }
        public string articolo { get; set; }
        public bool? carta_assegnata { get; set; }
        public string codice_carta { get; set; }
    }
}
