﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwdoc_arti_marca_modello
    {
        public int? doc_artiid { get; set; }
        public string marca { get; set; }
        public string modello { get; set; }
    }
}
