﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_repo
    {
        public int age_repoid { get; set; }
        public string stp_docu { get; set; }
        public string stp_tipo { get; set; }
        public string stp_file { get; set; }
        public string stp_modo { get; set; }
        public string stp_comm { get; set; }
        public decimal? stp_enab { get; set; }
        public string stp_path { get; set; }
        public decimal? stp_copy { get; set; }
        public decimal? stp_view { get; set; }
        public decimal? stp_fine { get; set; }
        public decimal? stp_ncol { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
