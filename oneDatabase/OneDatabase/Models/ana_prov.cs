﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_prov
    {
        public int ana_provid { get; set; }
        public string ana_provnome { get; set; }
        public string ana_provsigla { get; set; }
    }
}
