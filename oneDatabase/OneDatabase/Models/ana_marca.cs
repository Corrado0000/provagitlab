﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_marca
    {
        public ana_marca()
        {
            ana_arti = new HashSet<ana_arti>();
            ana_marca_top_list = new HashSet<ana_marca_top_list>();
        }

        public int ana_marcaid { get; set; }
        public string nome { get; set; }
        public string descrizione { get; set; }
        public bool? acq_applicanuovi { get; set; }
        public decimal? acq_sconto1 { get; set; }
        public decimal? acq_sconto2 { get; set; }
        public decimal? acq_sconto3 { get; set; }
        public decimal? acq_sconto4 { get; set; }
        public bool? ven_applicanuovi { get; set; }
        public decimal? ven_ricarica { get; set; }
        public decimal? ven_prezzobase { get; set; }
        public decimal? ven_ricaricalistino { get; set; }
        public decimal? ven_sconto1 { get; set; }
        public decimal? ven_sconto2 { get; set; }
        public decimal? ven_sconto3 { get; set; }
        public decimal? ven_sconto4 { get; set; }
        public bool? listini_applicanuovi { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<ana_arti> ana_arti { get; set; }
        public virtual ICollection<ana_marca_top_list> ana_marca_top_list { get; set; }
    }
}
