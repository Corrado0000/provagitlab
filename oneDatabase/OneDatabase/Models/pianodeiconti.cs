﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class pianodeiconti
    {
        public pianodeiconti()
        {
            causale_iva = new HashSet<causale_iva>();
            movimento = new HashSet<movimento>();
            movimento_contropartita = new HashSet<movimento_contropartita>();
            movimento_iva = new HashSet<movimento_iva>();
        }

        public int pianodeicontiid { get; set; }
        public string codice { get; set; }
        public string descrizione { get; set; }
        public int? parentpianodeicontiid { get; set; }
        public int? livelloid { get; set; }
        public string tipologia { get; set; }
        public string sezione { get; set; }
        public bool? dare { get; set; }
        public bool? avere { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool? cont_pag { get; set; }

        public virtual ICollection<causale_iva> causale_iva { get; set; }
        public virtual ICollection<movimento> movimento { get; set; }
        public virtual ICollection<movimento_contropartita> movimento_contropartita { get; set; }
        public virtual ICollection<movimento_iva> movimento_iva { get; set; }
    }
}
