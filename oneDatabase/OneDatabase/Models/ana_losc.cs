﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_losc
    {
        public ana_losc()
        {
            doc_arti = new HashSet<doc_arti>();
            ord_arti = new HashSet<ord_arti>();
            pre_arti = new HashSet<pre_arti>();
        }

        public int ana_loscid { get; set; }
        public string key_anno { get; set; }
        public int? ana_artiid { get; set; }
        public int? top_magaid { get; set; }
        public string cod_losc { get; set; }
        public DateTime? cod_scad { get; set; }
        public decimal? qta_inve { get; set; }
        public decimal? qta_entr { get; set; }
        public decimal? qta_usci { get; set; }
        public decimal? qta_giac { get; set; }
        public decimal? qta_impe { get; set; }
        public decimal? qta_ordi { get; set; }
        public decimal? qta_disp { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual top_maga top_maga { get; set; }
        public virtual ICollection<doc_arti> doc_arti { get; set; }
        public virtual ICollection<ord_arti> ord_arti { get; set; }
        public virtual ICollection<pre_arti> pre_arti { get; set; }
    }
}
