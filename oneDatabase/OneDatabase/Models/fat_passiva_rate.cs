﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fat_passiva_rate
    {
        public fat_passiva_rate()
        {
            fat_passiva_rate_pagamenti = new HashSet<fat_passiva_rate_pagamenti>();
        }

        public int fat_passiva_rateid { get; set; }
        public int fat_passivaid { get; set; }
        public string mod_paga { get; set; }
        public DateTime dat_scad { get; set; }
        public decimal importo { get; set; }
        public int creationuser { get; set; }
        public DateTime creationdate { get; set; }
        public int lastmodifyuser { get; set; }
        public DateTime lastmodifydate { get; set; }
        public bool enabled { get; set; }

        public virtual fat_passiva fat_passiva { get; set; }
        public virtual ICollection<fat_passiva_rate_pagamenti> fat_passiva_rate_pagamenti { get; set; }
    }
}
