﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_commessa
    {
        public age_commessa()
        {
            age_commessanomi = new HashSet<age_commessanomi>();
        }

        public int age_commessaid { get; set; }
        public string cod_commessa { get; set; }
        public string descr_commessa { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<age_commessanomi> age_commessanomi { get; set; }
    }
}
