﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class prov_list_map
    {
        public int prov_list_mapid { get; set; }
        public int? ana_artiid { get; set; }
        public int? prov_listid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
