﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_salaanagrafica
    {
        public ho_salaanagrafica()
        {
            ho_configurazionestampante = new HashSet<ho_configurazionestampante>();
            ho_notesala = new HashSet<ho_notesala>();
            ho_tavoloanagrafica = new HashSet<ho_tavoloanagrafica>();
        }

        public int ho_salaanagraficaid { get; set; }
        public string codsala { get; set; }
        public int? top_listid { get; set; }
        public string salnomesala { get; set; }
        public decimal? lunghezza_metri { get; set; }
        public decimal? larghezza_metri { get; set; }
        public int? id_prodottocoperto { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ICollection<ho_configurazionestampante> ho_configurazionestampante { get; set; }
        public virtual ICollection<ho_notesala> ho_notesala { get; set; }
        public virtual ICollection<ho_tavoloanagrafica> ho_tavoloanagrafica { get; set; }
    }
}
