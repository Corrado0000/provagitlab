﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_sedi
    {
        public age_sedi()
        {
            age_cdc = new HashSet<age_cdc>();
        }

        public int age_sediid { get; set; }
        public int? age_nomiid { get; set; }
        public string key_agen { get; set; }
        public string rif_nome { get; set; }
        public string rif_indi { get; set; }
        public string rif_nciv { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public int? age_statid { get; set; }
        public string rif_quad { get; set; }
        public decimal? rif_prin { get; set; }
        public string tel_nume { get; set; }
        public string tel_nfax { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? iva_perc { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual age_stat age_stat { get; set; }
        public virtual age_tiva age_tiva { get; set; }
        public virtual ICollection<age_cdc> age_cdc { get; set; }
    }
}
