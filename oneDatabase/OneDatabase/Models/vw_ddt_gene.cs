﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ddt_gene
    {
        public int? ddt_geneid { get; set; }
        public int? num_boll_number { get; set; }
        public string num_boll { get; set; }
        public DateTime? dat_boll { get; set; }
        public string cod_caus { get; set; }
        public string cod_maga { get; set; }
        public string rag_soci { get; set; }
        public string cod_agen { get; set; }
        public string num_comm { get; set; }
        public DateTime? dat_comm { get; set; }
        public string num_docu { get; set; }
        public DateTime? dat_docu { get; set; }
        public string cod_bank { get; set; }
        public string des_bank { get; set; }
        public char? pre_nume { get; set; }
        public int? top_causid { get; set; }
        public int? top_magaid { get; set; }
        public int? age_nomiid { get; set; }
        public string doc_rife { get; set; }
        public bool? enabled { get; set; }
        public decimal? tot_impo { get; set; }
        public decimal? tot_docu { get; set; }
        public string rap_1 { get; set; }
        public string rap_2 { get; set; }
        public string controllo_documento { get; set; }
        public string des_caus { get; set; }
        public string cod_tipo { get; set; }
        public string targa_oggetto { get; set; }
        public string chilometri_oggetto { get; set; }
        public string note { get; set; }
        public string ora { get; set; }
        public string codice_pin { get; set; }
        public string stato { get; set; }
        public int? creationuser { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public int? age_nomiidrap_2 { get; set; }
        public string des_paga { get; set; }
    }
}
