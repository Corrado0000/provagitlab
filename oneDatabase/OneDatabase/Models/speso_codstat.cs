﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class speso_codstat
    {
        public float? id_stato { get; set; }
        public string nome_stato { get; set; }
    }
}
