﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_fatt_attive_scadenze
    {
        public int? rif_fat_geneid { get; set; }
        public string cod_caus { get; set; }
        public string des_caus { get; set; }
        public string num_fatt { get; set; }
        public DateTime? dat_fatt { get; set; }
        public decimal? importo { get; set; }
        public decimal? importopagato { get; set; }
        public decimal? dapagare { get; set; }
        public int? indice { get; set; }
        public DateTime? scadenza { get; set; }
    }
}
