﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class appuser_tocken
    {
        public int appuser_tockenid { get; set; }
        public int? appuserid { get; set; }
        public int? tockenid { get; set; }
    }
}
