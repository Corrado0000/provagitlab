﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ho_contopiet
    {
        public ho_contopiet()
        {
            Inverseid_padreNavigation = new HashSet<ho_contopiet>();
            ho_log = new HashSet<ho_log>();
            ho_varianteconto = new HashSet<ho_varianteconto>();
        }

        public int ho_contopietid { get; set; }
        public int? id_map_sala_tavolo { get; set; }
        public int? ana_artiid { get; set; }
        public int? id_map_menu_prod { get; set; }
        public int? ho_tavoloanagraficaorigine { get; set; }
        public string commento { get; set; }
        public decimal? quantita { get; set; }
        public decimal? prezzo { get; set; }
        public string tipo { get; set; }
        public int? idpasto { get; set; }
        public int? idstato { get; set; }
        public int? indicechiusura { get; set; }
        public decimal? subtotale { get; set; }
        public string descrizione { get; set; }
        public int? toplistid { get; set; }
        public string id_group { get; set; }
        public int? id_padre { get; set; }
        public decimal? peso { get; set; }
        public bool ispeso { get; set; }
        public decimal? sconto { get; set; }
        public bool? inviatocucina { get; set; }
        public DateTime? dataorainviocucina { get; set; }
        public string motivo_annulla { get; set; }
        public int? id_anasubcate { get; set; }
        public string art_misu2 { get; set; }
        public int? age_nomiid { get; set; }
        public int? ho_gruppoordinazioneid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual ana_arti ana_arti { get; set; }
        public virtual ho_gruppoordinazione ho_gruppoordinazione { get; set; }
        public virtual ho_tavoloanagrafica ho_tavoloanagraficaorigineNavigation { get; set; }
        public virtual ana_subcate id_anasubcateNavigation { get; set; }
        public virtual ho_mapmenuprod id_map_menu_prodNavigation { get; set; }
        public virtual ho_contopiet id_padreNavigation { get; set; }
        public virtual ICollection<ho_contopiet> Inverseid_padreNavigation { get; set; }
        public virtual ICollection<ho_log> ho_log { get; set; }
        public virtual ICollection<ho_varianteconto> ho_varianteconto { get; set; }
    }
}
