﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ana_arti_ana_misu
    {
        public int? ana_arti_ana_misuid { get; set; }
        public int? ana_artiid { get; set; }
        public int? ana_misuid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string misu_code { get; set; }
        public string misu_desc { get; set; }
        public decimal? qta_misu_in { get; set; }
        public decimal? qta_misu_out { get; set; }
        public int? ana_misuid_base { get; set; }
        public string misu_code_base { get; set; }
        public string misu_desc_base { get; set; }
    }
}
