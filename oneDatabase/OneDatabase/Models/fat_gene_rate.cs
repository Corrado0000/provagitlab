﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class fat_gene_rate
    {
        public fat_gene_rate()
        {
            fat_gene_rate_pagamenti = new HashSet<fat_gene_rate_pagamenti>();
        }

        public int fat_gene_rateid { get; set; }
        public int fat_geneid { get; set; }
        public int tip_pagaid { get; set; }
        public DateTime dat_scad { get; set; }
        public decimal importo { get; set; }
        public int creationuser { get; set; }
        public DateTime creationdate { get; set; }
        public int lastmodifyuser { get; set; }
        public DateTime lastmodifydate { get; set; }
        public bool enabled { get; set; }

        public virtual fat_gene fat_gene { get; set; }
        public virtual tip_paga tip_paga { get; set; }
        public virtual ICollection<fat_gene_rate_pagamenti> fat_gene_rate_pagamenti { get; set; }
    }
}
