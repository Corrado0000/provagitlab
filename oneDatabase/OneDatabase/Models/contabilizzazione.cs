﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class contabilizzazione
    {
        public int contabilizzazioneid { get; set; }
        public int? rifescluso_fat_geneid { get; set; }
        public int? rifescluso_mov_geneid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
