﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class pre_arti
    {
        public pre_arti()
        {
            Inversepre_arti_padre = new HashSet<pre_arti>();
            ana_lotto = new HashSet<ana_lotto>();
            ana_ubicazione = new HashSet<ana_ubicazione>();
        }

        public int pre_artiid { get; set; }
        public string key_anno { get; set; }
        public string key_arti { get; set; }
        public int? pre_geneid { get; set; }
        public string num_rife { get; set; }
        public string cod_rife { get; set; }
        public string num_logi { get; set; }
        public int? ana_artiid { get; set; }
        public string des_arti { get; set; }
        public string sel_arti { get; set; }
        public string art_item { get; set; }
        public string art_misu { get; set; }
        public string art_note { get; set; }
        public string art_comp { get; set; }
        public decimal? art_cons { get; set; }
        public int? top_magaid { get; set; }
        public int? ana_loscid { get; set; }
        public DateTime? cod_scad { get; set; }
        public string cod_prez { get; set; }
        public decimal? cod_subt { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? qta_logi { get; set; }
        public string cod_matr { get; set; }
        public string cod_type { get; set; }
        public decimal? pre_cost { get; set; }
        public decimal? prv_cost { get; set; }
        public decimal? pre_impo { get; set; }
        public decimal? prv_impo { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string sco_text { get; set; }
        public decimal? pre_tota { get; set; }
        public decimal? prv_tota { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? iva_perc { get; set; }
        public decimal? pro_1 { get; set; }
        public decimal? pro_2 { get; set; }
        public int? age_cdcid { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public int? indice { get; set; }
        public bool? enabled { get; set; }
        public int? pre_arti_padreid { get; set; }
        public decimal? qta_distinta { get; set; }
        public bool? cauzione { get; set; }
        public int? ana_misuidvendita { get; set; }
        public decimal? qta_misu_vendita { get; set; }
        public decimal? qta_conf { get; set; }
        public decimal? qta_coll { get; set; }

        public virtual age_cdc age_cdc { get; set; }
        public virtual age_tiva age_tiva { get; set; }
        public virtual ana_arti ana_arti { get; set; }
        public virtual ana_losc ana_losc { get; set; }
        public virtual ana_misu ana_misuidvenditaNavigation { get; set; }
        public virtual pre_arti pre_arti_padre { get; set; }
        public virtual pre_gene pre_gene { get; set; }
        public virtual top_maga top_maga { get; set; }
        public virtual ICollection<pre_arti> Inversepre_arti_padre { get; set; }
        public virtual ICollection<ana_lotto> ana_lotto { get; set; }
        public virtual ICollection<ana_ubicazione> ana_ubicazione { get; set; }
    }
}
