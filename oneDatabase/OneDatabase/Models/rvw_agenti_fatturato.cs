﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_agenti_fatturato
    {
        public string key_anno { get; set; }
        public string num_fatt { get; set; }
        public DateTime? dat_fatt { get; set; }
        public string cod_agen { get; set; }
        public string rag_soci { get; set; }
        public decimal? tot_impo { get; set; }
        public string rap_1 { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public decimal? pro_1 { get; set; }
        public decimal? pro_impo { get; set; }
    }
}
