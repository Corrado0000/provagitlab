﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_misu
    {
        public ana_misu()
        {
            ana_arti = new HashSet<ana_arti>();
            pre_arti = new HashSet<pre_arti>();
        }

        public int ana_misuid { get; set; }
        public string misu_code { get; set; }
        public string misu_desc { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_misu ana_misuNavigation { get; set; }
        public virtual ana_misu Inverseana_misuNavigation { get; set; }
        public virtual ICollection<ana_arti> ana_arti { get; set; }
        public virtual ICollection<pre_arti> pre_arti { get; set; }
    }
}
