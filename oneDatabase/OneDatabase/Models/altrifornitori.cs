﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class altrifornitori
    {
        public int id { get; set; }
        public string codicefornitore { get; set; }
        public string codicearticolo { get; set; }
        public string fornitore { get; set; }
        public decimal? costoacquistobase { get; set; }
        public decimal? sc1 { get; set; }
        public decimal? sc2 { get; set; }
        public decimal? sc3 { get; set; }
        public decimal? sc4 { get; set; }
        public decimal? sc5 { get; set; }
        public decimal? costoacquistonetto { get; set; }
        public string altrocodicefornitore { get; set; }
    }
}
