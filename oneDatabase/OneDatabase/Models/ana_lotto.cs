﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_lotto
    {
        public int ana_lottoid { get; set; }
        public int? ana_artiid { get; set; }
        public string codice { get; set; }
        public DateTime? datascadenza { get; set; }
        public bool? entrata { get; set; }
        public int? doc_artiid { get; set; }
        public int? pre_artiid { get; set; }
        public int? ord_artiid { get; set; }
        public decimal? qta_tota { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual doc_arti doc_arti { get; set; }
        public virtual ord_arti ord_arti { get; set; }
        public virtual pre_arti pre_arti { get; set; }
    }
}
