﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_bank
    {
        public age_bank()
        {
            age_nomi = new HashSet<age_nomi>();
            ddt_gene = new HashSet<ddt_gene>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_gene = new HashSet<ord_gene>();
            pre_gene = new HashSet<pre_gene>();
        }

        public int age_bankid { get; set; }
        public string cod_bank { get; set; }
        public string abi_code { get; set; }
        public char? abi_cinc { get; set; }
        public string cab_code { get; set; }
        public char? cab_cinc { get; set; }
        public string des_bank { get; set; }
        public string spo_bank { get; set; }
        public string rif_indi { get; set; }
        public string rif_nciv { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public int? age_statid { get; set; }
        public string rif_quad { get; set; }
        public string cod_tipo { get; set; }
        public string cod_oper { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_stat age_stat { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
    }
}
