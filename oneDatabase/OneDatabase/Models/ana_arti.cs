﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_arti
    {
        public ana_arti()
        {
            Inverseana_artiidcauzioneNavigation = new HashSet<ana_arti>();
            ana_arti_ana_misu = new HashSet<ana_arti_ana_misu>();
            ana_arti_config_apisync = new HashSet<ana_arti_config_apisync>();
            ana_cauz = new HashSet<ana_cauz>();
            ana_diba = new HashSet<ana_diba>();
            ana_forn = new HashSet<ana_forn>();
            ana_lifo = new HashSet<ana_lifo>();
            ana_list = new HashSet<ana_list>();
            ana_losc = new HashSet<ana_losc>();
            ana_lotto = new HashSet<ana_lotto>();
            ana_maga = new HashSet<ana_maga>();
            ana_offe = new HashSet<ana_offe>();
            ana_pari = new HashSet<ana_pari>();
            ana_ubicazione = new HashSet<ana_ubicazione>();
            distinta_base = new HashSet<distinta_base>();
            doc_arti = new HashSet<doc_arti>();
            ho_abbinamentiana_arti = new HashSet<ho_abbinamenti>();
            ho_abbinamentiana_artiabbinamento = new HashSet<ho_abbinamenti>();
            ho_contopiet = new HashSet<ho_contopiet>();
            ho_mapaggiuntaana_arti = new HashSet<ho_mapaggiunta>();
            ho_mapaggiuntapietanza = new HashSet<ho_mapaggiunta>();
            ho_mapmenuprod = new HashSet<ho_mapmenuprod>();
            ho_varianteconto = new HashSet<ho_varianteconto>();
            ord_arti = new HashSet<ord_arti>();
            pre_arti = new HashSet<pre_arti>();
        }

        public int ana_artiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string des_plus { get; set; }
        public string rif_arti { get; set; }
        public int? ana_marcaid { get; set; }
        public string sel_arti { get; set; }
        public string art_misu { get; set; }
        public decimal? art_peso { get; set; }
        public decimal? art_cuba { get; set; }
        public string art_ubic { get; set; }
        public string art_iweb { get; set; }
        public decimal? mac_gest { get; set; }
        public decimal? dib_gest { get; set; }
        public decimal? los_gest { get; set; }
        public decimal? qta_scor { get; set; }
        public decimal? qta_maxi { get; set; }
        public decimal? qta_rior { get; set; }
        public decimal? qta_conf { get; set; }
        public decimal? qta_coll { get; set; }
        public int? age_nomiid { get; set; }
        public int? age_valuid { get; set; }
        public string cod_acqu { get; set; }
        public decimal? pre_acqu { get; set; }
        public decimal? prv_acqu { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string sco_text { get; set; }
        public decimal? net_acqu { get; set; }
        public decimal? nev_acqu { get; set; }
        public decimal? ric_perc { get; set; }
        public decimal? ric_cmed { get; set; }
        public decimal? pre_vend { get; set; }
        public decimal? prv_vend { get; set; }
        public decimal? pre_arro { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? iva_perc { get; set; }
        public string old_arti { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
        public DateTime? ult_list { get; set; }
        public DateTime? ult_acqu { get; set; }
        public string dbf_gate { get; set; }
        public bool? gestionelotti { get; set; }
        public bool? lotto_obbligatorio { get; set; }
        public bool? lotto_dataobbligatoria { get; set; }
        public string lotto_modalitaprelievo { get; set; }
        public string lotto_modalitaselezione { get; set; }
        public string contabilizzazione_conto { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public bool? web { get; set; }
        public bool? home { get; set; }
        public decimal? lis_base { get; set; }
        public decimal? lis_netto { get; set; }
        public string for_arti { get; set; }
        public string art_anno { get; set; }
        public string art_line { get; set; }
        public string art_regi { get; set; }
        public string art_nazi { get; set; }
        public decimal? pre_web { get; set; }
        public string tag_web { get; set; }
        public string log_5 { get; set; }
        public bool? istaglia { get; set; }
        public string descrprodotto { get; set; }
        public bool? farma_flgpht { get; set; }
        public decimal? farma_przrimborsoregionale { get; set; }
        public decimal? farma_przconcordatoregionale { get; set; }
        public string farma_ssn { get; set; }
        public bool? farma_nonaggiornare { get; set; }
        public bool? flg_farma { get; set; }
        public string farma_principioattivo { get; set; }
        public string farma_flgcommercio { get; set; }
        public bool? ishoreca { get; set; }
        public bool? ispeso { get; set; }
        public bool? iscoperto { get; set; }
        public int? venditecount { get; set; }
        public string art_misu2 { get; set; }
        public int? ho_gruppoordinazioneid { get; set; }
        public bool? noninviocucina { get; set; }
        public bool? isvariante { get; set; }
        public string des_arti_eng { get; set; }
        public bool? isricetta { get; set; }
        public bool? indicaquantita { get; set; }
        public bool? ignorasustatistiche { get; set; }
        public decimal? pesodefault { get; set; }
        public decimal? art_larghezza { get; set; }
        public decimal? art_altezza { get; set; }
        public decimal? art_profondita { get; set; }
        public decimal marg_perc { get; set; }
        public bool premedpond { get; set; }
        public string filtro_pers { get; set; }
        public char? controllo_giacenza { get; set; }
        public int? top_magaid_riferimento { get; set; }
        public int? ana_artiidcauzione { get; set; }
        public decimal? qta_cauzione { get; set; }
        public decimal? pre_trasporto { get; set; }
        public char? blocco_omag { get; set; }
        public bool? ps_nosyncdesc { get; set; }
        public string ps_desc { get; set; }
        public int? ana_misuid_unita { get; set; }
        public decimal? pre_impo_unita { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual age_tiva age_tiva { get; set; }
        public virtual age_valu age_valu { get; set; }
        public virtual ana_arti ana_artiidcauzioneNavigation { get; set; }
        public virtual ana_marca ana_marca { get; set; }
        public virtual ana_misu ana_misuid_unitaNavigation { get; set; }
        public virtual ho_gruppoordinazione ho_gruppoordinazione { get; set; }
        public virtual ICollection<ana_arti> Inverseana_artiidcauzioneNavigation { get; set; }
        public virtual ICollection<ana_arti_ana_misu> ana_arti_ana_misu { get; set; }
        public virtual ICollection<ana_arti_config_apisync> ana_arti_config_apisync { get; set; }
        public virtual ICollection<ana_cauz> ana_cauz { get; set; }
        public virtual ICollection<ana_diba> ana_diba { get; set; }
        public virtual ICollection<ana_forn> ana_forn { get; set; }
        public virtual ICollection<ana_lifo> ana_lifo { get; set; }
        public virtual ICollection<ana_list> ana_list { get; set; }
        public virtual ICollection<ana_losc> ana_losc { get; set; }
        public virtual ICollection<ana_lotto> ana_lotto { get; set; }
        public virtual ICollection<ana_maga> ana_maga { get; set; }
        public virtual ICollection<ana_offe> ana_offe { get; set; }
        public virtual ICollection<ana_pari> ana_pari { get; set; }
        public virtual ICollection<ana_ubicazione> ana_ubicazione { get; set; }
        public virtual ICollection<distinta_base> distinta_base { get; set; }
        public virtual ICollection<doc_arti> doc_arti { get; set; }
        public virtual ICollection<ho_abbinamenti> ho_abbinamentiana_arti { get; set; }
        public virtual ICollection<ho_abbinamenti> ho_abbinamentiana_artiabbinamento { get; set; }
        public virtual ICollection<ho_contopiet> ho_contopiet { get; set; }
        public virtual ICollection<ho_mapaggiunta> ho_mapaggiuntaana_arti { get; set; }
        public virtual ICollection<ho_mapaggiunta> ho_mapaggiuntapietanza { get; set; }
        public virtual ICollection<ho_mapmenuprod> ho_mapmenuprod { get; set; }
        public virtual ICollection<ho_varianteconto> ho_varianteconto { get; set; }
        public virtual ICollection<ord_arti> ord_arti { get; set; }
        public virtual ICollection<pre_arti> pre_arti { get; set; }
    }
}
