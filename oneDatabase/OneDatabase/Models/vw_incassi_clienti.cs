﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_incassi_clienti
    {
        public int? age_nomiid { get; set; }
        public decimal? importopagato { get; set; }
        public decimal? importogaranzia { get; set; }
    }
}
