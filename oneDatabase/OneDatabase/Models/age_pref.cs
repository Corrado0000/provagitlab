﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_pref
    {
        public age_pref()
        {
            age_cate = new HashSet<age_cate>();
            age_nomi = new HashSet<age_nomi>();
        }

        public int age_prefid { get; set; }
        public char? cod_pref { get; set; }
        public string des_pref { get; set; }
        public decimal? pro_pref { get; set; }
        public string suf_pref { get; set; }
        public decimal? cli_pref { get; set; }
        public decimal? for_pref { get; set; }
        public decimal? vet_pref { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public decimal rap_pref { get; set; }

        public virtual ICollection<age_cate> age_cate { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
    }
}
