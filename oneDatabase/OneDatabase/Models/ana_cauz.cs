﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_cauz
    {
        public int ana_cauzid { get; set; }
        public string key_anno { get; set; }
        public int? age_nomiid { get; set; }
        public int? ana_artiid { get; set; }
        public decimal? qta_da_rendere_ap { get; set; }
        public decimal? qta_usci { get; set; }
        public decimal? qta_resa { get; set; }
        public decimal? qta_da_rendere { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual ana_arti ana_arti { get; set; }
    }
}
