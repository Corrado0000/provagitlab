﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class shop_vwmov_gene_view
    {
        public int? mov_geneid { get; set; }
        public string num_movi { get; set; }
        public DateTime? dat_movi { get; set; }
        public string cod_maga { get; set; }
        public string num_logi { get; set; }
        public decimal? pre_tota { get; set; }
    }
}
