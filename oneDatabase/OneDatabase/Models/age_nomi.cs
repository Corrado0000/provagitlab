﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_nomi
    {
        public age_nomi()
        {
            Inverseage_nomiidrap_1Navigation = new HashSet<age_nomi>();
            Inverseage_nomiidrap_2Navigation = new HashSet<age_nomi>();
            age_cdc = new HashSet<age_cdc>();
            age_commessanomi = new HashSet<age_commessanomi>();
            age_cont = new HashSet<age_cont>();
            age_sedi = new HashSet<age_sedi>();
            age_user = new HashSet<age_user>();
            ana_arti = new HashSet<ana_arti>();
            ana_cate_scontoconcordato = new HashSet<ana_cate_scontoconcordato>();
            ana_cauz = new HashSet<ana_cauz>();
            ana_forn = new HashSet<ana_forn>();
            ana_offe = new HashSet<ana_offe>();
            apisync_user = new HashSet<apisync_user>();
            ddt_geneage_nomi = new HashSet<ddt_gene>();
            ddt_geneage_nomiidrap_1Navigation = new HashSet<ddt_gene>();
            ddt_geneage_nomiidrap_2Navigation = new HashSet<ddt_gene>();
            doc_arti = new HashSet<doc_arti>();
            fat_geneage_nomi = new HashSet<fat_gene>();
            fat_geneage_nomiidrap_1Navigation = new HashSet<fat_gene>();
            fat_geneage_nomiidrap_2Navigation = new HashSet<fat_gene>();
            ho_contopiet = new HashSet<ho_contopiet>();
            ho_prenotazioni = new HashSet<ho_prenotazioni>();
            mov_geneage_nomi = new HashSet<mov_gene>();
            mov_geneage_nomiidrap_1Navigation = new HashSet<mov_gene>();
            mov_geneage_nomiidrap_2Navigation = new HashSet<mov_gene>();
            ord_gene = new HashSet<ord_gene>();
            pre_geneage_nomi = new HashSet<pre_gene>();
            pre_geneage_nomiidrap_1Navigation = new HashSet<pre_gene>();
            pre_geneage_nomiidrap_2Navigation = new HashSet<pre_gene>();
            top_maga = new HashSet<top_maga>();
        }

        public int age_nomiid { get; set; }
        public int? appuserid { get; set; }
        public string cod_agen { get; set; }
        public string rag_soci { get; set; }
        public int? age_prefid { get; set; }
        public string suf_pref { get; set; }
        public int? age_cateid { get; set; }
        public decimal? sel_priv { get; set; }
        public decimal? sel_pfis { get; set; }
        public string rag_prin { get; set; }
        public string rag_tito { get; set; }
        public string sta_next { get; set; }
        public DateTime? dat_next { get; set; }
        public string cod_exte { get; set; }
        public string par_tiva { get; set; }
        public string iso_code { get; set; }
        public string cod_fisc { get; set; }
        public string rif_indi { get; set; }
        public string rif_nciv { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public int? age_statid { get; set; }
        public string rif_quad { get; set; }
        public string tel_nume { get; set; }
        public string tel_nfax { get; set; }
        public string rif_mail { get; set; }
        public string rif_iweb { get; set; }
        public int? age_pagaid { get; set; }
        public int? age_bankid { get; set; }
        public string con_bank { get; set; }
        public string con_iban { get; set; }
        public int? age_valuid { get; set; }
        public int? ana_listid { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? iva_perc { get; set; }
        public string iva_nume { get; set; }
        public DateTime? iva_data { get; set; }
        public decimal? max_fido { get; set; }
        public decimal? ord_spre { get; set; }
        public decimal? ord_stot { get; set; }
        public decimal? pre_spre { get; set; }
        public decimal? pre_stot { get; set; }
        public decimal? ddt_spre { get; set; }
        public decimal? ddt_stot { get; set; }
        public decimal? fat_uddt { get; set; }
        public decimal? fat_isos { get; set; }
        public decimal? add_boll { get; set; }
        public decimal? add_inca { get; set; }
        public decimal? sco_perc { get; set; }
        public decimal? pro_1 { get; set; }
        public decimal? pro_2 { get; set; }
        public string spe_port { get; set; }
        public string spe_aspe { get; set; }
        public string spe_tipo { get; set; }
        public string cod_vett { get; set; }
        public string rif_note { get; set; }
        public decimal? cli_pref { get; set; }
        public decimal? for_pref { get; set; }
        public decimal? vet_pref { get; set; }
        public bool? ritenuta_acconto { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
        public int? defaultage_cdcid { get; set; }
        public string dbf_gate { get; set; }
        public int? top_listid { get; set; }
        public bool? fidelitycard { get; set; }
        public decimal? fidelitysconto { get; set; }
        public decimal? fidelityricarico { get; set; }
        public string fidelitycodicecarta { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string filtro_3 { get; set; }
        public string last_status { get; set; }
        public string last_status_code { get; set; }
        public string nom_nome { get; set; }
        public string nom_cogn { get; set; }
        public DateTime? nom_nasc { get; set; }
        public string nom_nasc_comstatest { get; set; }
        public string nom_provnasc { get; set; }
        public int? ana_lineeid { get; set; }
        public bool? ismsd { get; set; }
        public string coord_lat { get; set; }
        public string coord_long { get; set; }
        public string farmaclickcode { get; set; }
        public int? nop_id { get; set; }
        public int? giorni_consegna { get; set; }
        public bool? fidobloccato { get; set; }
        public decimal rap_pref { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public int? age_nomiidrap_2 { get; set; }
        public string fel_codice_dest { get; set; }
        public string rif_pec { get; set; }
        public string ritenuta_acconto_cau_paga { get; set; }
        public int? fel_formato_trasm { get; set; }
        public bool? esen_cauz { get; set; }
        public int? pianodeicontiid_articoli { get; set; }
        public int? pianodeicontiid_spese { get; set; }
        public decimal? marg_perc_vat { get; set; }
        public int? causale_contabileid_td01 { get; set; }
        public decimal? max_fido_assegni { get; set; }
        public string note_fido_sblocco { get; set; }
        public bool? auto_blocco_fido { get; set; }
        public DateTime? data_sblocco_fido_manuale { get; set; }
        public int? pianodeicontiid_attivo { get; set; }

        public virtual age_bank age_bank { get; set; }
        public virtual age_cate age_cate { get; set; }
        public virtual age_nomi age_nomiidrap_1Navigation { get; set; }
        public virtual age_nomi age_nomiidrap_2Navigation { get; set; }
        public virtual age_paga age_paga { get; set; }
        public virtual age_pref age_pref { get; set; }
        public virtual age_stat age_stat { get; set; }
        public virtual age_tiva age_tiva { get; set; }
        public virtual age_valu age_valu { get; set; }
        public virtual ana_list ana_list { get; set; }
        public virtual top_list top_list { get; set; }
        public virtual ICollection<age_nomi> Inverseage_nomiidrap_1Navigation { get; set; }
        public virtual ICollection<age_nomi> Inverseage_nomiidrap_2Navigation { get; set; }
        public virtual ICollection<age_cdc> age_cdc { get; set; }
        public virtual ICollection<age_commessanomi> age_commessanomi { get; set; }
        public virtual ICollection<age_cont> age_cont { get; set; }
        public virtual ICollection<age_sedi> age_sedi { get; set; }
        public virtual ICollection<age_user> age_user { get; set; }
        public virtual ICollection<ana_arti> ana_arti { get; set; }
        public virtual ICollection<ana_cate_scontoconcordato> ana_cate_scontoconcordato { get; set; }
        public virtual ICollection<ana_cauz> ana_cauz { get; set; }
        public virtual ICollection<ana_forn> ana_forn { get; set; }
        public virtual ICollection<ana_offe> ana_offe { get; set; }
        public virtual ICollection<apisync_user> apisync_user { get; set; }
        public virtual ICollection<ddt_gene> ddt_geneage_nomi { get; set; }
        public virtual ICollection<ddt_gene> ddt_geneage_nomiidrap_1Navigation { get; set; }
        public virtual ICollection<ddt_gene> ddt_geneage_nomiidrap_2Navigation { get; set; }
        public virtual ICollection<doc_arti> doc_arti { get; set; }
        public virtual ICollection<fat_gene> fat_geneage_nomi { get; set; }
        public virtual ICollection<fat_gene> fat_geneage_nomiidrap_1Navigation { get; set; }
        public virtual ICollection<fat_gene> fat_geneage_nomiidrap_2Navigation { get; set; }
        public virtual ICollection<ho_contopiet> ho_contopiet { get; set; }
        public virtual ICollection<ho_prenotazioni> ho_prenotazioni { get; set; }
        public virtual ICollection<mov_gene> mov_geneage_nomi { get; set; }
        public virtual ICollection<mov_gene> mov_geneage_nomiidrap_1Navigation { get; set; }
        public virtual ICollection<mov_gene> mov_geneage_nomiidrap_2Navigation { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_gene> pre_geneage_nomi { get; set; }
        public virtual ICollection<pre_gene> pre_geneage_nomiidrap_1Navigation { get; set; }
        public virtual ICollection<pre_gene> pre_geneage_nomiidrap_2Navigation { get; set; }
        public virtual ICollection<top_maga> top_maga { get; set; }
    }
}
