﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_mov_gene
    {
        public int? mov_geneid { get; set; }
        public int? num_movi_number { get; set; }
        public string num_movi { get; set; }
        public DateTime? dat_movi { get; set; }
        public string cod_caus { get; set; }
        public string cod_maga { get; set; }
        public string rag_soci { get; set; }
        public string cod_agen { get; set; }
        public string num_comm { get; set; }
        public DateTime? dat_comm { get; set; }
        public string num_docu { get; set; }
        public DateTime? dat_docu { get; set; }
        public string cod_bank { get; set; }
        public string des_bank { get; set; }
        public int? top_causid { get; set; }
        public int? top_magaid { get; set; }
        public int? age_nomiid { get; set; }
        public string doc_rife { get; set; }
        public char? pre_nume { get; set; }
        public bool? enabled { get; set; }
        public decimal? tot_docu { get; set; }
        public decimal? tot_acco { get; set; }
        public string key_anno { get; set; }
        public int? creationuser { get; set; }
        public decimal? imponibile { get; set; }
        public int? percontodiage_nomiid { get; set; }
    }
}
