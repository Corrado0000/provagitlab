﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_age_nomi
    {
        public int? age_nomiid { get; set; }
        public string cod_agen { get; set; }
        public string rag_soci { get; set; }
        public string dscate { get; set; }
        public string dspref { get; set; }
        public string par_tiva { get; set; }
        public string cod_exte { get; set; }
        public string cod_fisc { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public string rap_1 { get; set; }
        public string sta_next { get; set; }
        public DateTime? dat_next { get; set; }
        public int? age_nomiidrap_2 { get; set; }
        public string rap_2 { get; set; }
        public string tel_nume { get; set; }
        public string rif_note { get; set; }
        public bool? enabled { get; set; }
        public string last_status_code { get; set; }
        public string filtro_3 { get; set; }
        public string tel_nfax { get; set; }
        public string rif_mail { get; set; }
        public string rif_iweb { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
        public string rif_indi { get; set; }
        public string rif_avvi { get; set; }
        public string rif_citt { get; set; }
        public string rif_prov { get; set; }
        public int? creationuser { get; set; }
        public string fel_codice_dest { get; set; }
    }
}
