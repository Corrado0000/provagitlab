﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class appgroup
    {
        public int appgroupid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool? enabled { get; set; }
        public int? supergroupid { get; set; }
        public string diritti { get; set; }
    }
}
