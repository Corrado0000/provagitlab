﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class sys_tablestatus
    {
        public string tablename { get; set; }
        public DateTime lastinsert { get; set; }
        public DateTime lastupdate { get; set; }
        public DateTime lastdelete { get; set; }
        public byte[] lastchange { get; set; }
    }
}
