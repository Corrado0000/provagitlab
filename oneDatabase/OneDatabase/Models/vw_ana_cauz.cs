﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ana_cauz
    {
        public int? ana_artiid { get; set; }
        public int? age_nomiid { get; set; }
        public string key_anno { get; set; }
        public decimal? qta_usci { get; set; }
        public decimal? qta_resa { get; set; }
        public decimal? qta_da_rendere { get; set; }
        public decimal? qta_da_rendere_ap { get; set; }
    }
}
