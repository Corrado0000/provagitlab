﻿using OneDatabase.Data;
using OneDatabase.Utils;
using System;

namespace OneDatabase.Models
{
    [Serializable]
    public class CompanyPropertiesEntity_1_0
    {
      
        private const String _category = "Sinergia.One";
        private const String _name = "CompanyProperties";
        public const String Version = "0.1.1";

     

        #region Proprietà

        public String Azienda { get; set; }

        public String Filiale { get; set; }

        public String Indirizzo { get; set; }

        public String Civico { get; set; }

        public String Cap { get; set; }

        public String Citta { get; set; }

        public String Provincia { get; set; }

        public int IdStato { get; set; }

        public String TelefonoVoce { get; set; }

        public String TelefonoFax { get; set; }

        public String CodiceFiscale { get; set; }

        public String PartitaIVA { get; set; }

        public String CodiceISO { get; set; }

        public int IdValuta { get; set; }

        public String Email { get; set; }

        public String WebPage { get; set; }

        public String PicVertical1 { get; set; }

        public String PicVertical2 { get; set; }

        public String PicHorizontal1 { get; set; }

        public String PicHorizontal2 { get; set; }

        #endregion

        #region Persistenza

        private static CompanyPropertiesEntity_1_0 _value;

        public static CompanyPropertiesEntity_1_0 Load(oneContext context)
        {
            if (_value != null)
                return _value;

            // Deserializzazione.
            //
            String xml = context.GetConfigurationValue(_category, _name, Version);

            if (xml != "")
            {
                System.IO.StringReader sr = new System.IO.StringReader(xml);

                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(CompanyPropertiesEntity_1_0));
                _value = (CompanyPropertiesEntity_1_0)serializer.Deserialize(sr);
                return _value;
            }

            return new CompanyPropertiesEntity_1_0();
        }

        public void Save(oneContext context)
        {
            // Serializzazione risultati.
            //
            System.IO.StringWriter sw = new System.IO.StringWriter();

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(CompanyPropertiesEntity_1_0));
            serializer.Serialize(sw, this);

            context.SetConfigurationValue(_category, _name, Version, sw.ToString());
        }

        #endregion
    }
}
