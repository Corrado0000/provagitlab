﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ddt_gene
    {
        public ddt_gene()
        {
            doc_arti = new HashSet<doc_arti>();
        }

        public int ddt_geneid { get; set; }
        public string key_anno { get; set; }
        public string num_boll { get; set; }
        public DateTime? dat_boll { get; set; }
        public string num_comm { get; set; }
        public DateTime? dat_comm { get; set; }
        public string num_docu { get; set; }
        public DateTime? dat_docu { get; set; }
        public string num_logi { get; set; }
        public int? top_causid { get; set; }
        public int? top_magaid { get; set; }
        public int? top_maga_settoreid { get; set; }
        public int? top_maga_scaffaleid { get; set; }
        public int? top_maga_posizioneid { get; set; }
        public int? age_nomiid { get; set; }
        public int? age_cdcid { get; set; }
        public int? percontodiage_nomiid { get; set; }
        public char? cod_serv { get; set; }
        public string cod_tari { get; set; }
        public int? age_nomivettid { get; set; }
        public int? age_pagaid { get; set; }
        public int? age_bankid { get; set; }
        public string con_bank { get; set; }
        public string con_iban { get; set; }
        public int? top_listid { get; set; }
        public string cod_prez { get; set; }
        public int? age_valuid { get; set; }
        public decimal? chg_valu { get; set; }
        public DateTime? chg_data { get; set; }
        public int? age_tivaid { get; set; }
        public decimal? iva_perc { get; set; }
        public decimal? sco_perc { get; set; }
        public decimal? ric_perc { get; set; }
        public decimal? cos_tras { get; set; }
        public decimal? cos_inca { get; set; }
        public decimal? cos_imba { get; set; }
        public decimal? cos_acce { get; set; }
        public decimal? cos_boll { get; set; }
        public int? iva_trasid { get; set; }
        public int? iva_incaid { get; set; }
        public int? iva_imbaid { get; set; }
        public int? iva_acceid { get; set; }
        public decimal? cov_tras { get; set; }
        public decimal? cov_inca { get; set; }
        public decimal? cov_imba { get; set; }
        public decimal? cov_acce { get; set; }
        public decimal? cov_boll { get; set; }
        public string des_nome { get; set; }
        public string des_indi { get; set; }
        public string des_nciv { get; set; }
        public string des_avvi { get; set; }
        public string des_citt { get; set; }
        public string des_prov { get; set; }
        public int? age_statid { get; set; }
        public string des_quad { get; set; }
        public string key_sede { get; set; }
        public string spe_tipo { get; set; }
        public DateTime? spe_data { get; set; }
        public string spe_time { get; set; }
        public decimal? spe_coll { get; set; }
        public decimal? spe_peso { get; set; }
        public decimal? spe_cuba { get; set; }
        public decimal? spe_epal { get; set; }
        public string spe_aspe { get; set; }
        public string spe_port { get; set; }
        public string doc_rife { get; set; }
        public string doc_resa { get; set; }
        public string doc_note { get; set; }
        public string doc_stat { get; set; }
        public string doc_tipo { get; set; }
        public decimal? doc_sprt { get; set; }
        public decimal? doc_spre { get; set; }
        public decimal? doc_stot { get; set; }
        public decimal? tot_arti { get; set; }
        public decimal? tot_scon { get; set; }
        public decimal? tot_merc { get; set; }
        public decimal? tot_prod { get; set; }
        public decimal? tot_serv { get; set; }
        public decimal? tot_vari { get; set; }
        public decimal? tot_omag { get; set; }
        public decimal? tot_spes { get; set; }
        public decimal? tot_impo { get; set; }
        public decimal? tot_osta { get; set; }
        public decimal? tot_esen { get; set; }
        public decimal? tot_docu { get; set; }
        public decimal? tot_abbu { get; set; }
        public decimal? tot_acco { get; set; }
        public decimal? tot_paga { get; set; }
        public decimal? tov_arti { get; set; }
        public decimal? tov_scon { get; set; }
        public decimal? tov_merc { get; set; }
        public decimal? tov_prod { get; set; }
        public decimal? tov_serv { get; set; }
        public decimal? tov_vari { get; set; }
        public decimal? tov_omag { get; set; }
        public decimal? tov_spes { get; set; }
        public decimal? tov_impo { get; set; }
        public decimal? tov_osta { get; set; }
        public decimal? tov_esen { get; set; }
        public decimal? tov_docu { get; set; }
        public decimal? tov_abbu { get; set; }
        public decimal? tov_acco { get; set; }
        public decimal? tov_paga { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string controllo_documento { get; set; }
        public string modello_oggetto { get; set; }
        public string anno_oggetto { get; set; }
        public string chilometri_oggetto { get; set; }
        public string targa_oggetto { get; set; }
        public string telaio_oggetto { get; set; }
        public string telefonocliente { get; set; }
        public string doc_note2 { get; set; }
        public string ora { get; set; }
        public string codice_pin { get; set; }
        public int? business_unit { get; set; }
        public int? ana_lineeid { get; set; }
        public bool? vis_prez_agenda { get; set; }
        public DateTime? dat_fine { get; set; }
        public byte[] doc_sign { get; set; }
        public bool prezzi_ivati { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public int? age_nomiidrap_2 { get; set; }
        public decimal? tot_cauz { get; set; }
        public bool? pagato { get; set; }
        public decimal? importo_pagato { get; set; }
        public DateTime? data_consegna { get; set; }

        public virtual age_bank age_bank { get; set; }
        public virtual age_cdc age_cdc { get; set; }
        public virtual age_nomi age_nomi { get; set; }
        public virtual age_nomi age_nomiidrap_1Navigation { get; set; }
        public virtual age_nomi age_nomiidrap_2Navigation { get; set; }
        public virtual age_paga age_paga { get; set; }
        public virtual age_stat age_stat { get; set; }
        public virtual age_tiva age_tiva { get; set; }
        public virtual age_valu age_valu { get; set; }
        public virtual top_caus top_caus { get; set; }
        public virtual top_list top_list { get; set; }
        public virtual top_maga top_maga { get; set; }
        public virtual top_maga_posizione top_maga_posizione { get; set; }
        public virtual top_maga_scaffale top_maga_scaffale { get; set; }
        public virtual top_maga_settore top_maga_settore { get; set; }
        public virtual ICollection<doc_arti> doc_arti { get; set; }
    }
}
