﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_marca_top_list
    {
        public int ana_marca_top_listid { get; set; }
        public int? ana_marcaid { get; set; }
        public int? top_listid { get; set; }
        public decimal? ric_perc { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_marca ana_marca { get; set; }
        public virtual top_list top_list { get; set; }
    }
}
