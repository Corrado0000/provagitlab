﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class prov_list
    {
        public int prov_listid { get; set; }
        public string nome { get; set; }
        public DateTime? datainizio { get; set; }
        public DateTime? datafine { get; set; }
        public decimal? scontoperc { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
    }
}
