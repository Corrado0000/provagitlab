﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwcustomerorder_view
    {
        public int? pre_geneid { get; set; }
        public int? num_prev_number { get; set; }
        public string num_prev { get; set; }
        public DateTime? dat_prev { get; set; }
        public string cod_caus { get; set; }
        public string cod_maga { get; set; }
        public string rag_soci { get; set; }
        public string cod_agen { get; set; }
        public string num_comm { get; set; }
        public DateTime? dat_comm { get; set; }
        public string cod_vett { get; set; }
        public string cod_bank { get; set; }
        public string des_bank { get; set; }
        public char? pre_nume { get; set; }
        public int? top_causid { get; set; }
        public int? top_magaid { get; set; }
        public int? age_nomiid { get; set; }
        public string doc_stat { get; set; }
        public bool? enabled { get; set; }
        public decimal? tot_docu { get; set; }
        public string rap_1 { get; set; }
        public string rap_2 { get; set; }
        public string log_1 { get; set; }
        public string log_2 { get; set; }
        public string log_3 { get; set; }
        public string log_4 { get; set; }
        public string cod_tipo { get; set; }
        public string des_caus { get; set; }
        public string doc_rife { get; set; }
        public decimal? tot_impo { get; set; }
        public int? creationuser { get; set; }
        public int? age_nomiidrap_1 { get; set; }
        public int? age_nomiidrap_2 { get; set; }
        public string destinazione { get; set; }
        public DateTime? data_consegna { get; set; }
        public string ora_consegna { get; set; }
    }
}
