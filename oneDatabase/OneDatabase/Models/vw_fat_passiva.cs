﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_fat_passiva
    {
        public int? fat_passivaid { get; set; }
        public int? age_nomiid { get; set; }
        public string external_id { get; set; }
        public string mittente { get; set; }
        public string par_tiva { get; set; }
        public string cod_fisc { get; set; }
        public string tip_doc { get; set; }
        public string descrizione { get; set; }
        public string num_fatt { get; set; }
        public string key_anno { get; set; }
        public decimal? importo { get; set; }
        public string num_doc { get; set; }
        public DateTime? dat_fatt { get; set; }
        public DateTime? dat_ricez { get; set; }
        public int? stato_pag { get; set; }
        public decimal? importo_pag { get; set; }
        public decimal? importo_dapag { get; set; }
    }
}
