﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ana_arti
    {
        public int? ana_artiid { get; set; }
        public string des_plus { get; set; }
        public string key_anno { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string gru_arti { get; set; }
        public string gru_arti1 { get; set; }
        public string mar_arti { get; set; }
        public string rif_arti { get; set; }
        public string sel_arti { get; set; }
        public string art_misu { get; set; }
        public decimal? art_peso { get; set; }
        public decimal? art_cuba { get; set; }
        public string art_ubic { get; set; }
        public string art_iweb { get; set; }
        public decimal? qta_scor { get; set; }
        public decimal? qta_maxi { get; set; }
        public decimal? qta_rior { get; set; }
        public decimal? qta_conf { get; set; }
        public decimal? qta_coll { get; set; }
        public int? age_nomiid { get; set; }
        public string cod_agen { get; set; }
        public string rag_soci { get; set; }
        public string cod_valu { get; set; }
        public decimal? pre_acqu { get; set; }
        public decimal? prv_acqu { get; set; }
        public decimal? sco_1 { get; set; }
        public decimal? sco_2 { get; set; }
        public decimal? sco_3 { get; set; }
        public decimal? sco_4 { get; set; }
        public string sco_text { get; set; }
        public decimal? net_acqu { get; set; }
        public decimal? nev_acqu { get; set; }
        public decimal? ric_perc { get; set; }
        public decimal? pre_vend { get; set; }
        public decimal? prv_vend { get; set; }
        public decimal? pre_arro { get; set; }
        public string iva_code { get; set; }
        public string iva_desc { get; set; }
        public decimal? giacenza { get; set; }
        public bool? enabled { get; set; }
        public decimal? entrate { get; set; }
        public decimal? uscite { get; set; }
        public decimal? imponibile { get; set; }
        public decimal? ivato { get; set; }
        public string anno { get; set; }
        public string um { get; set; }
        public decimal? qta { get; set; }
        public string colore { get; set; }
        public string log_1 { get; set; }
        public string filtro_pers { get; set; }
        public string for_arti { get; set; }
        public string maga_riferimento { get; set; }
        public int? creationuser { get; set; }
    }
}
