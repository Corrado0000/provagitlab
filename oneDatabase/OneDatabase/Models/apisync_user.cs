﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class apisync_user
    {
        public int apisync_userid { get; set; }
        public int? config_apisyncid { get; set; }
        public int? age_nomiid { get; set; }
        public string usr { get; set; }
        public string pwd { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }
        public string tipo { get; set; }

        public virtual age_nomi age_nomi { get; set; }
        public virtual config_apisync config_apisync { get; set; }
    }
}
