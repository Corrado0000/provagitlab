﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class appuser_group
    {
        public int appuser_groupid { get; set; }
        public int appuserid { get; set; }
        public int groupid { get; set; }
    }
}
