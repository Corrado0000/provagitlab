﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class causale_iva
    {
        public int causale_ivaid { get; set; }
        public int? causaleid { get; set; }
        public int? indice { get; set; }
        public int? age_tivaid { get; set; }
        public int? pianodeicontiid { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual causale causale { get; set; }
        public virtual pianodeiconti pianodeiconti { get; set; }
    }
}
