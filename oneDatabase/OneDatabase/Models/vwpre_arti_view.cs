﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vwpre_arti_view
    {
        public int? pre_artiid { get; set; }
        public int? pre_geneid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string art_misu { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? qta_tota_base { get; set; }
        public string art_misu_base { get; set; }
        public decimal? qta_logi { get; set; }
        public decimal? pre_impo { get; set; }
        public string sco_text { get; set; }
        public decimal? iva_perc { get; set; }
        public bool? enabled { get; set; }
        public int? indice { get; set; }
        public string art_note { get; set; }
        public decimal? qta_coll { get; set; }
    }
}
