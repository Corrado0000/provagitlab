﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_ddt_gene_dett
    {
        public int? ddt_geneid { get; set; }
        public int? doc_artiid { get; set; }
        public decimal? qta_tota { get; set; }
        public decimal? pre_impo { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public decimal? iva_perc { get; set; }
        public string sco_text { get; set; }
        public string key_anno { get; set; }
        public string cod_matr { get; set; }
        public string cod_type { get; set; }
        public int? ana_artiid { get; set; }
        public string art_misu { get; set; }
        public string art_item { get; set; }
        public string expr1 { get; set; }
    }
}
