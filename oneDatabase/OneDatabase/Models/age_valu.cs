﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class age_valu
    {
        public age_valu()
        {
            age_nomi = new HashSet<age_nomi>();
            ana_arti = new HashSet<ana_arti>();
            ana_forn = new HashSet<ana_forn>();
            ddt_gene = new HashSet<ddt_gene>();
            fat_gene = new HashSet<fat_gene>();
            mov_gene = new HashSet<mov_gene>();
            ord_gene = new HashSet<ord_gene>();
            pre_gene = new HashSet<pre_gene>();
        }

        public int age_valuid { get; set; }
        public string cod_valu { get; set; }
        public string des_valu { get; set; }
        public int? age_statid { get; set; }
        public decimal? ult_camb { get; set; }
        public DateTime? ult_data { get; set; }
        public decimal? dec_camb { get; set; }
        public decimal? dec_unit { get; set; }
        public decimal? dec_tota { get; set; }
        public string bmp_valu { get; set; }
        public decimal? ult_camb_a { get; set; }
        public decimal? ult_camb_b { get; set; }
        public DateTime? ult_data_a { get; set; }
        public DateTime? ult_data_b { get; set; }
        public decimal? dec_unit_a { get; set; }
        public decimal? dec_unit_b { get; set; }
        public decimal? dec_tota_a { get; set; }
        public decimal? dec_tota_b { get; set; }
        public string bmp_valu_a { get; set; }
        public string bmp_valu_b { get; set; }
        public string dbf_gate { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual age_stat age_stat { get; set; }
        public virtual ICollection<age_nomi> age_nomi { get; set; }
        public virtual ICollection<ana_arti> ana_arti { get; set; }
        public virtual ICollection<ana_forn> ana_forn { get; set; }
        public virtual ICollection<ddt_gene> ddt_gene { get; set; }
        public virtual ICollection<fat_gene> fat_gene { get; set; }
        public virtual ICollection<mov_gene> mov_gene { get; set; }
        public virtual ICollection<ord_gene> ord_gene { get; set; }
        public virtual ICollection<pre_gene> pre_gene { get; set; }
    }
}
