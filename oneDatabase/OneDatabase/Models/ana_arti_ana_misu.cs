﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class ana_arti_ana_misu
    {
        public int ana_arti_ana_misuid { get; set; }
        public int? ana_artiid { get; set; }
        public int ana_misuid { get; set; }
        public decimal? qta_misu_in { get; set; }
        public decimal? qta_misu_out { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
    }
}
