﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class codici_ateco
    {
        public string codice { get; set; }
        public string descrizione { get; set; }
    }
}
