﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class vw_ana_lotto
    {
        public int? ana_lottoid { get; set; }
        public int? ana_artiid { get; set; }
        public string cod_arti { get; set; }
        public string des_arti { get; set; }
        public string codice { get; set; }
        public DateTime? datascadenza { get; set; }
        public bool? entrata { get; set; }
        public decimal? qta_tota { get; set; }
        public string num_docu { get; set; }
        public string rag_soci { get; set; }
        public string cod_caus { get; set; }
        public string des_caus { get; set; }
    }
}
