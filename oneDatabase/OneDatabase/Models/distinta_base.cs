﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class distinta_base
    {
        public distinta_base()
        {
            ho_varianteconto = new HashSet<ho_varianteconto>();
        }

        public int distinta_baseid { get; set; }
        public int? ana_artiid { get; set; }
        public int? ana_artiid_child { get; set; }
        public decimal? qta { get; set; }
        public int? creationuser { get; set; }
        public DateTime? creationdate { get; set; }
        public int? lastmodifyuser { get; set; }
        public DateTime? lastmodifydate { get; set; }
        public bool? enabled { get; set; }

        public virtual ana_arti ana_arti { get; set; }
        public virtual ICollection<ho_varianteconto> ho_varianteconto { get; set; }
    }
}
