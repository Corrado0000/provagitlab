﻿using System;
using System.Collections.Generic;

namespace OneDatabase.Models
{
    public partial class rvw_maga_mov
    {
        public int? mov_geneid { get; set; }
        public string key_anno { get; set; }
        public string num_movi { get; set; }
        public DateTime? dat_movi { get; set; }
        public int? top_causid { get; set; }
        public string cod_caus { get; set; }
        public string cod_tipo { get; set; }
        public int? top_magaid { get; set; }
        public string cod_maga { get; set; }
        public string num_docu { get; set; }
        public DateTime? dat_docu { get; set; }
        public int? age_nomiid { get; set; }
        public int? percontodiage_nomiid { get; set; }
        public string rag_soci { get; set; }
        public decimal? imponibile { get; set; }
        public decimal? totale { get; set; }
        public decimal? acconto { get; set; }
        public decimal? differenza { get; set; }
    }
}
