﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Npgsql;
using OneDatabase.Models;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace OneDatabase.Data
{
    public partial class oneContext : DbContext
    {

        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        //
        public oneContext()
        {

        }
        public oneContext(DbContextOptions<oneContext> options, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
           : base(options)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;


        }

        public string Schema { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_httpContextAccessor != null
               && _httpContextAccessor.HttpContext != null
                 && _httpContextAccessor.HttpContext.User != null
                 && _httpContextAccessor.HttpContext.User.Identity != null)
            {
                ClaimsIdentity identity = _httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity;

                if (identity != null && identity.HasClaim(s => s.Type == "schema"))
                {
                    Schema = identity.FindFirst("schema").Value;
                }
            }

            int sslMode = 0;

            int.TryParse(_configuration["Connection:SslMode"], out sslMode);

            NpgsqlConnectionStringBuilder csb = new NpgsqlConnectionStringBuilder()
            {
                Host = _configuration["Connection:Server"],
                Port = int.Parse(_configuration["Connection:Port"]),
                Database = _configuration["Connection:Database"],
                Username = _configuration["Connection:User"],
                Password = _configuration["Connection:Password"],
                SslMode = (SslMode)sslMode,
                ApplicationName = Schema

            };


            optionsBuilder.UseNpgsql(csb.ToString())
               .ReplaceService<IModelCacheKeyFactory, SchemaTenantCacheKeyFactory>();

            base.OnConfiguring(optionsBuilder);

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("dblink");

            modelBuilder.Entity<age_bank>(entity =>
            {
                entity.ToTable("age_bank", Schema);

                entity.HasIndex(e => e.age_statid)
                    .HasName("idx_age_bank_01");

                entity.Property(e => e.age_bankid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_bank'::regclass)");

                entity.Property(e => e.abi_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cab_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cod_bank)
                    .HasMaxLength(11)
                    .IsFixedLength();

                entity.Property(e => e.cod_oper)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_tipo)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.des_bank).HasMaxLength(60);

                entity.Property(e => e.rif_avvi)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(50);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.rif_quad).HasMaxLength(20);

                entity.Property(e => e.spo_bank).HasMaxLength(60);

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.age_bank)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_age_bank_01");
            });

            modelBuilder.Entity<age_budgetcdc>(entity =>
            {
                entity.ToTable("age_budgetcdc", Schema);

                entity.HasIndex(e => e.age_cdcid)
                    .HasName("idx_age_budgetcdc_01");

                entity.Property(e => e.age_budgetcdcid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_budgetcdc'::regclass)");

                entity.Property(e => e.acquisto).HasColumnType("numeric(18,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.vendita).HasColumnType("numeric(18,2)");

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.age_budgetcdc)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_age_budgetcdc_01");
            });

            modelBuilder.Entity<age_cate>(entity =>
            {
                entity.ToTable("age_cate", Schema);

                entity.HasIndex(e => e.age_prefid)
                    .HasName("idx_age_cate_01");

                entity.HasIndex(e => e.top_listid)
                    .HasName("idx_age_cate_02");

                entity.Property(e => e.age_cateid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_cate'::regclass)");

                entity.Property(e => e.cod_cate)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.des_cate).HasMaxLength(50);

                entity.Property(e => e.pro_cate).HasColumnType("numeric(10,0)");

                entity.Property(e => e.suf_pref)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.HasOne(d => d.age_pref)
                    .WithMany(p => p.age_cate)
                    .HasForeignKey(d => d.age_prefid)
                    .HasConstraintName("fk_age_cate_01");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.age_cate)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_age_cate_02");
            });

            modelBuilder.Entity<age_cdc>(entity =>
            {
                entity.ToTable("age_cdc", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_age_cdc_01");

                entity.HasIndex(e => e.parentage_cdcid)
                    .HasName("idx_age_cdc_02");

                entity.HasIndex(e => e.responsabileage_contid)
                    .HasName("idx_age_cdc_03");

                entity.HasIndex(e => e.sedeage_sediid)
                    .HasName("idx_age_cdc_04");

                entity.Property(e => e.age_cdcid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_cdc'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(32);

                entity.Property(e => e.descrizione).HasMaxLength(250);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.age_cdc)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_age_cdc_03");

                entity.HasOne(d => d.parentage_cdc)
                    .WithMany(p => p.Inverseparentage_cdc)
                    .HasForeignKey(d => d.parentage_cdcid)
                    .HasConstraintName("fk_age_cdc_01");

                entity.HasOne(d => d.responsabileage_cont)
                    .WithMany(p => p.age_cdc)
                    .HasForeignKey(d => d.responsabileage_contid)
                    .HasConstraintName("fk_age_cdc_02");

                entity.HasOne(d => d.sedeage_sedi)
                    .WithMany(p => p.age_cdc)
                    .HasForeignKey(d => d.sedeage_sediid)
                    .HasConstraintName("fk_age_cdc_04");
            });

            modelBuilder.Entity<age_commessa>(entity =>
            {
                entity.ToTable("age_commessa", Schema);

                entity.Property(e => e.age_commessaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_commessa'::regclass)");

                entity.Property(e => e.cod_commessa)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.descr_commessa).HasMaxLength(50);
            });

            modelBuilder.Entity<age_commessanomi>(entity =>
            {
                entity.ToTable("age_commessanomi", Schema);

                entity.HasIndex(e => e.age_commessaid)
                    .HasName("idx_age_commessanomi_01");

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_age_commessanomi_02");

                entity.Property(e => e.age_commessanomiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_commessanomi'::regclass)");

                entity.HasOne(d => d.age_commessa)
                    .WithMany(p => p.age_commessanomi)
                    .HasForeignKey(d => d.age_commessaid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_age_commessanomi_01");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.age_commessanomi)
                    .HasForeignKey(d => d.age_nomiid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_age_commessanomi_02");
            });

            modelBuilder.Entity<age_cont>(entity =>
            {
                entity.ToTable("age_cont", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_age_cont_01");

                entity.Property(e => e.age_contid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_cont'::regclass)");

                entity.Property(e => e.cnt_cari).HasMaxLength(50);

                entity.Property(e => e.cnt_mail).HasMaxLength(250);

                entity.Property(e => e.cnt_nome).HasMaxLength(50);

                entity.Property(e => e.cnt_note).HasMaxLength(2000);

                entity.Property(e => e.key_agen).HasMaxLength(10);

                entity.Property(e => e.tel_ncel).HasMaxLength(250);

                entity.Property(e => e.tel_nfax).HasMaxLength(250);

                entity.Property(e => e.tel_nume).HasMaxLength(250);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.age_cont)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_age_cont_01");
            });

            modelBuilder.Entity<age_nomi>(entity =>
            {
                entity.ToTable("age_nomi", Schema);

                entity.HasIndex(e => e.age_bankid)
                    .HasName("idx_age_nomi_01");

                entity.HasIndex(e => e.age_cateid)
                    .HasName("idx_age_nomi_02");

                entity.HasIndex(e => e.age_nomiidrap_1)
                    .HasName("idx_age_nomi_03");

                entity.HasIndex(e => e.age_nomiidrap_2)
                    .HasName("idx_age_nomi_04");

                entity.HasIndex(e => e.age_pagaid)
                    .HasName("idx_age_nomi_05");

                entity.HasIndex(e => e.age_prefid)
                    .HasName("idx_age_nomi_06");

                entity.HasIndex(e => e.age_statid)
                    .HasName("idx_age_nomi_07");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_age_nomi_08");

                entity.HasIndex(e => e.age_valuid)
                    .HasName("idx_age_nomi_09");

                entity.HasIndex(e => e.ana_listid)
                    .HasName("idx_age_nomi_10");

                entity.HasIndex(e => e.top_listid)
                    .HasName("idx_age_nomi_11");

                entity.Property(e => e.age_nomiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_nomi'::regclass)");

                entity.Property(e => e.add_boll).HasColumnType("numeric(1,0)");

                entity.Property(e => e.add_inca).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cli_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_exte).HasMaxLength(250);

                entity.Property(e => e.cod_fisc).HasMaxLength(250);

                entity.Property(e => e.cod_vett).HasMaxLength(10);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.coord_lat).HasMaxLength(50);

                entity.Property(e => e.coord_long).HasMaxLength(50);

                entity.Property(e => e.data_sblocco_fido_manuale).HasColumnType("date");

                entity.Property(e => e.ddt_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ddt_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.farmaclickcode).HasMaxLength(50);

                entity.Property(e => e.fat_isos).HasColumnType("numeric(1,0)");

                entity.Property(e => e.fat_uddt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.fel_codice_dest).HasMaxLength(7);

                entity.Property(e => e.fidelitycodicecarta).HasMaxLength(50);

                entity.Property(e => e.fidelityricarico).HasColumnType("numeric(20,5)");

                entity.Property(e => e.fidelitysconto).HasColumnType("numeric(20,5)");

                entity.Property(e => e.filtro_3).HasMaxLength(250);

                entity.Property(e => e.for_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.iso_code).HasMaxLength(2);

                entity.Property(e => e.iva_nume).HasMaxLength(20);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.last_status).HasMaxLength(250);

                entity.Property(e => e.last_status_code).HasMaxLength(250);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.marg_perc_vat).HasColumnType("numeric(6,2)");

                entity.Property(e => e.max_fido).HasColumnType("numeric(20,5)");

                entity.Property(e => e.max_fido_assegni).HasColumnType("numeric(20,5)");

                entity.Property(e => e.nom_cogn).HasMaxLength(250);

                entity.Property(e => e.nom_nasc_comstatest).HasMaxLength(50);

                entity.Property(e => e.nom_nome).HasMaxLength(250);

                entity.Property(e => e.nom_provnasc).HasMaxLength(3);

                entity.Property(e => e.note_fido_sblocco).HasColumnType("character varying");

                entity.Property(e => e.ord_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ord_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.par_tiva).HasMaxLength(250);

                entity.Property(e => e.pre_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pre_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pro_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pro_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.rag_prin).HasMaxLength(50);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rag_tito).HasMaxLength(20);

                entity.Property(e => e.rap_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(500);

                entity.Property(e => e.rif_iweb).HasMaxLength(50);

                entity.Property(e => e.rif_mail).HasMaxLength(250);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_pec).HasMaxLength(250);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.rif_quad).HasMaxLength(20);

                entity.Property(e => e.ritenuta_acconto_cau_paga).HasMaxLength(3);

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sel_pfis).HasColumnType("numeric(1,0)");

                entity.Property(e => e.sel_priv).HasColumnType("numeric(1,0)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.sta_next).HasMaxLength(250);

                entity.Property(e => e.suf_pref)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.tel_nfax).HasMaxLength(250);

                entity.Property(e => e.tel_nume).HasMaxLength(250);

                entity.Property(e => e.vet_pref).HasColumnType("numeric(1,0)");

                entity.HasOne(d => d.age_bank)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.age_bankid)
                    .HasConstraintName("fk_age_nomi_02");

                entity.HasOne(d => d.age_cate)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.age_cateid)
                    .HasConstraintName("fk_age_nomi_03");

                entity.HasOne(d => d.age_nomiidrap_1Navigation)
                    .WithMany(p => p.Inverseage_nomiidrap_1Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_1)
                    .HasConstraintName("fk_age_nomi_04");

                entity.HasOne(d => d.age_nomiidrap_2Navigation)
                    .WithMany(p => p.Inverseage_nomiidrap_2Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_2)
                    .HasConstraintName("fk_age_nomi_05");

                entity.HasOne(d => d.age_paga)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.age_pagaid)
                    .HasConstraintName("fk_age_nomi_06");

                entity.HasOne(d => d.age_pref)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.age_prefid)
                    .HasConstraintName("fk_age_nomi_07");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_age_nomi_08");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_age_nomi_09");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_age_nomi_10");

                entity.HasOne(d => d.ana_list)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.ana_listid)
                    .HasConstraintName("fk_age_nomi_11");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.age_nomi)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_age_nomi_12");
            });

            modelBuilder.Entity<age_paga>(entity =>
            {
                entity.ToTable("age_paga", Schema);

                entity.HasIndex(e => e.tip_pagaid)
                    .HasName("idx_age_paga_01");

                entity.Property(e => e.age_pagaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_paga'::regclass)");

                entity.Property(e => e.cod_paga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cog_inc).HasMaxLength(3);

                entity.Property(e => e.cog_inc_contropartita).HasMaxLength(50);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.pag_escl).HasMaxLength(20);

                entity.Property(e => e.pag_gfis).HasColumnType("numeric(3,0)");

                entity.Property(e => e.pag_iniz).HasColumnType("numeric(3,0)");

                entity.Property(e => e.pag_ivam).HasMaxLength(20);

                entity.Property(e => e.pag_mese).HasMaxLength(20);

                entity.Property(e => e.pag_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pag_rate).HasColumnType("numeric(3,0)");

                entity.Property(e => e.pag_scon).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pag_skip).HasColumnType("numeric(2,0)");

                entity.Property(e => e.pag_spem).HasMaxLength(20);

                entity.Property(e => e.pag_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pag_step).HasColumnType("numeric(3,0)");

                entity.HasOne(d => d.tip_paga)
                    .WithMany(p => p.age_paga)
                    .HasForeignKey(d => d.tip_pagaid)
                    .HasConstraintName("fk_age_paga_01");
            });

            modelBuilder.Entity<age_pref>(entity =>
            {
                entity.ToTable("age_pref", Schema);

                entity.Property(e => e.age_prefid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_pref'::regclass)");

                entity.Property(e => e.cli_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.des_pref).HasMaxLength(50);

                entity.Property(e => e.for_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pro_pref).HasColumnType("numeric(10,0)");

                entity.Property(e => e.rap_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.suf_pref)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.vet_pref).HasColumnType("numeric(1,0)");
            });

            modelBuilder.Entity<age_repo>(entity =>
            {
                entity.ToTable("age_repo", Schema);

                entity.Property(e => e.age_repoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_repo'::regclass)");

                entity.Property(e => e.stp_comm)
                    .HasMaxLength(50)
                    .IsFixedLength();

                entity.Property(e => e.stp_copy).HasColumnType("numeric(3,0)");

                entity.Property(e => e.stp_docu)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.stp_enab).HasColumnType("numeric(1,0)");

                entity.Property(e => e.stp_file)
                    .HasMaxLength(100)
                    .IsFixedLength();

                entity.Property(e => e.stp_fine).HasColumnType("numeric(1,0)");

                entity.Property(e => e.stp_modo)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.stp_ncol).HasColumnType("numeric(1,0)");

                entity.Property(e => e.stp_path)
                    .HasMaxLength(100)
                    .IsFixedLength();

                entity.Property(e => e.stp_tipo)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.stp_view).HasColumnType("numeric(1,0)");
            });

            modelBuilder.Entity<age_ruoli>(entity =>
            {
                entity.ToTable("age_ruoli", Schema);

                entity.Property(e => e.age_ruoliid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_ruoli'::regclass)");

                entity.Property(e => e.cod_pref).HasMaxLength(1);

                entity.Property(e => e.des_ruolo).HasMaxLength(50);
            });

            modelBuilder.Entity<age_sedi>(entity =>
            {
                entity.ToTable("age_sedi", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_age_sedi_01");

                entity.HasIndex(e => e.age_statid)
                    .HasName("idx_age_sedi_02");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_age_sedi_03");

                entity.Property(e => e.age_sediid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_sedi'::regclass)");

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_agen).HasMaxLength(10);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(50);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_nome).HasMaxLength(50);

                entity.Property(e => e.rif_prin).HasColumnType("numeric(1,0)");

                entity.Property(e => e.rif_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.rif_quad).HasMaxLength(20);

                entity.Property(e => e.tel_nfax).HasMaxLength(250);

                entity.Property(e => e.tel_nume).HasMaxLength(250);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.age_sedi)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_age_sedi_01");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.age_sedi)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_age_sedi_02");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.age_sedi)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_age_sedi_03");
            });

            modelBuilder.Entity<age_stat>(entity =>
            {
                entity.ToTable("age_stat", Schema);

                entity.Property(e => e.age_statid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_stat'::regclass)");

                entity.Property(e => e.cod_stat).HasMaxLength(3);

                entity.Property(e => e.codice).HasMaxLength(3);

                entity.Property(e => e.continente).HasMaxLength(30);

                entity.Property(e => e.descrizione)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.enabled).HasDefaultValueSql("true");
            });

            modelBuilder.Entity<age_tiva>(entity =>
            {
                entity.ToTable("age_tiva", Schema);

                entity.Property(e => e.age_tivaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_tiva'::regclass)");

                entity.Property(e => e.indetraibilita).HasColumnType("numeric(20,5)");

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_dedi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.iva_desc).HasMaxLength(50);

                entity.Property(e => e.iva_exue).HasColumnType("numeric(1,0)");

                entity.Property(e => e.iva_inue).HasColumnType("numeric(1,0)");

                entity.Property(e => e.iva_noli).HasColumnType("numeric(1,0)");

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.iva_tipo).HasMaxLength(50);

                entity.Property(e => e.natura).HasMaxLength(2);

                entity.Property(e => e.old_code).HasMaxLength(6);

                entity.Property(e => e.riferimento_normativo).HasMaxLength(100);
            });

            modelBuilder.Entity<age_user>(entity =>
            {
                entity.ToTable("age_user", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_age_user_01");

                entity.Property(e => e.age_userid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_user'::regclass)");

                entity.Property(e => e.amm_logi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cod_grup).HasMaxLength(20);

                entity.Property(e => e.cod_rapp).HasMaxLength(10);

                entity.Property(e => e.cod_tipo).HasMaxLength(10);

                entity.Property(e => e.cod_user).HasMaxLength(100);

                entity.Property(e => e.com_logi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.int_logi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.liv_acce).HasColumnType("numeric(2,0)");

                entity.Property(e => e.pas_word).HasMaxLength(20);

                entity.Property(e => e.rap_logi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.rip_logi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.tec_logi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.web_logi).HasColumnType("numeric(1,0)");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.age_user)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_age_user_01");
            });

            modelBuilder.Entity<age_valu>(entity =>
            {
                entity.ToTable("age_valu", Schema);

                entity.HasIndex(e => e.age_statid)
                    .HasName("idx_age_valu_01");

                entity.Property(e => e.age_valuid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_valu'::regclass)");

                entity.Property(e => e.bmp_valu).HasMaxLength(20);

                entity.Property(e => e.bmp_valu_a).HasMaxLength(20);

                entity.Property(e => e.bmp_valu_b).HasMaxLength(20);

                entity.Property(e => e.cod_valu)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.dec_camb).HasColumnType("numeric(2,0)");

                entity.Property(e => e.dec_tota).HasColumnType("numeric(1,0)");

                entity.Property(e => e.dec_tota_a).HasColumnType("numeric(1,0)");

                entity.Property(e => e.dec_tota_b).HasColumnType("numeric(1,0)");

                entity.Property(e => e.dec_unit).HasColumnType("numeric(1,0)");

                entity.Property(e => e.dec_unit_a).HasColumnType("numeric(1,0)");

                entity.Property(e => e.dec_unit_b).HasColumnType("numeric(1,0)");

                entity.Property(e => e.des_valu).HasMaxLength(50);

                entity.Property(e => e.ult_camb).HasColumnType("numeric(20,10)");

                entity.Property(e => e.ult_camb_a).HasColumnType("numeric(20,10)");

                entity.Property(e => e.ult_camb_b).HasColumnType("numeric(20,10)");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.age_valu)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_age_valu_01");
            });

            modelBuilder.Entity<age_voci>(entity =>
            {
                entity.ToTable("age_voci", Schema);

                entity.Property(e => e.age_vociid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_age_voci'::regclass)");

                entity.Property(e => e.des_voce).HasMaxLength(50);

                entity.Property(e => e.log_voce).HasMaxLength(50);
            });

            modelBuilder.Entity<aggiornacodici>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggiornacodici", Schema);

                entity.Property(e => e.attuale).HasMaxLength(255);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.nuovo).HasMaxLength(255);

                entity.Property(e => e.rif_arti).HasMaxLength(50);
            });

            modelBuilder.Entity<aggiornamento_prezzi_xmarca>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("aggiornamento_prezzi_xmarca", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");
            });

            modelBuilder.Entity<altrifornitori>(entity =>
            {
                entity.ToTable("altrifornitori", Schema);

                entity.Property(e => e.id).HasDefaultValueSql($"nextval('{Schema}.seq_pk_altrifornitori'::regclass)");

                entity.Property(e => e.altrocodicefornitore).HasMaxLength(50);

                entity.Property(e => e.codicearticolo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.codicefornitore)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.costoacquistobase).HasColumnType("numeric(18,2)");

                entity.Property(e => e.costoacquistonetto).HasColumnType("numeric(18,2)");

                entity.Property(e => e.fornitore).HasMaxLength(50);

                entity.Property(e => e.sc1).HasColumnType("numeric(18,2)");

                entity.Property(e => e.sc2).HasColumnType("numeric(18,2)");

                entity.Property(e => e.sc3).HasColumnType("numeric(18,2)");

                entity.Property(e => e.sc4).HasColumnType("numeric(18,2)");

                entity.Property(e => e.sc5).HasColumnType("numeric(18,2)");
            });

            modelBuilder.Entity<ana_arti>(entity =>
            {
                entity.ToTable("ana_arti", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ana_arti_01");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_ana_arti_02");

                entity.HasIndex(e => e.age_valuid)
                    .HasName("idx_ana_arti_03");

                entity.HasIndex(e => e.ana_artiidcauzione)
                    .HasName("idx_ana_arti_04");

                entity.HasIndex(e => e.ana_marcaid)
                    .HasName("idx_ana_arti_05");

                entity.HasIndex(e => e.ana_misuid_unita)
                    .HasName("idx_ana_arti_06");

                entity.HasIndex(e => e.ho_gruppoordinazioneid)
                    .HasName("idx_ana_arti_07");

                entity.Property(e => e.ana_artiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_arti'::regclass)");

                entity.Property(e => e.art_altezza).HasColumnType("numeric(18,5)");

                entity.Property(e => e.art_anno).HasMaxLength(50);

                entity.Property(e => e.art_cuba).HasColumnType("numeric(18,5)");

                entity.Property(e => e.art_iweb).HasMaxLength(100);

                entity.Property(e => e.art_larghezza).HasColumnType("numeric(18,5)");

                entity.Property(e => e.art_line).HasMaxLength(50);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.art_misu2).HasMaxLength(5);

                entity.Property(e => e.art_nazi).HasMaxLength(50);

                entity.Property(e => e.art_peso).HasColumnType("numeric(18,5)");

                entity.Property(e => e.art_profondita).HasColumnType("numeric(18,5)");

                entity.Property(e => e.art_regi).HasMaxLength(50);

                entity.Property(e => e.art_ubic).HasMaxLength(100);

                entity.Property(e => e.cod_acqu).HasMaxLength(20);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.contabilizzazione_conto).HasMaxLength(20);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.des_arti_eng).HasMaxLength(100);

                entity.Property(e => e.descrprodotto).HasMaxLength(50);

                entity.Property(e => e.dib_gest).HasColumnType("numeric(1,0)");

                entity.Property(e => e.farma_flgcommercio).HasMaxLength(1);

                entity.Property(e => e.farma_principioattivo).HasMaxLength(250);

                entity.Property(e => e.farma_przconcordatoregionale).HasColumnType("numeric(20,5)");

                entity.Property(e => e.farma_przrimborsoregionale).HasColumnType("numeric(20,5)");

                entity.Property(e => e.farma_ssn).HasMaxLength(50);

                entity.Property(e => e.filtro_pers).HasMaxLength(100);

                entity.Property(e => e.for_arti).HasMaxLength(50);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.lis_base).HasColumnType("numeric(20,5)");

                entity.Property(e => e.lis_netto).HasColumnType("numeric(20,5)");

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.log_5).HasMaxLength(50);

                entity.Property(e => e.los_gest).HasColumnType("numeric(1,0)");

                entity.Property(e => e.lotto_modalitaprelievo).HasMaxLength(20);

                entity.Property(e => e.lotto_modalitaselezione).HasMaxLength(20);

                entity.Property(e => e.mac_gest).HasColumnType("numeric(1,0)");

                entity.Property(e => e.marg_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.nev_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.old_arti).HasMaxLength(13);

                entity.Property(e => e.pesodefault).HasColumnType("numeric(9,5)");

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_arro).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo_unita).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_trasporto).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_web).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ps_desc).HasColumnType("character varying");

                entity.Property(e => e.qta_cauzione).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_coll).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_conf).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_maxi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_rior).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_scor).HasColumnType("numeric(14,3)");

                entity.Property(e => e.ric_cmed).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.rif_arti).HasMaxLength(50);

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.tag_web).HasMaxLength(250);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ana_arti)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ana_arti_01");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.ana_arti)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_ana_arti_02");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.ana_arti)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_ana_arti_03");

                entity.HasOne(d => d.ana_artiidcauzioneNavigation)
                    .WithMany(p => p.Inverseana_artiidcauzioneNavigation)
                    .HasForeignKey(d => d.ana_artiidcauzione)
                    .HasConstraintName("fk_ana_arti_04");

                entity.HasOne(d => d.ana_marca)
                    .WithMany(p => p.ana_arti)
                    .HasForeignKey(d => d.ana_marcaid)
                    .HasConstraintName("fk_ana_arti_05");

                entity.HasOne(d => d.ana_misuid_unitaNavigation)
                    .WithMany(p => p.ana_arti)
                    .HasForeignKey(d => d.ana_misuid_unita)
                    .HasConstraintName("fk_ana_arti_06");

                entity.HasOne(d => d.ho_gruppoordinazione)
                    .WithMany(p => p.ana_arti)
                    .HasForeignKey(d => d.ho_gruppoordinazioneid)
                    .HasConstraintName("fk_ana_arti_07");
            });

            modelBuilder.Entity<ana_arti_ana_cate>(entity =>
            {
                entity.ToTable("ana_arti_ana_cate", Schema);

                entity.Property(e => e.ana_arti_ana_cateid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_arti_ana_cate'::regclass)");
            });

            modelBuilder.Entity<ana_arti_ana_misu>(entity =>
            {
                entity.ToTable("ana_arti_ana_misu", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_arti_ana_misu_01");

                entity.Property(e => e.ana_arti_ana_misuid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_arti_ana_misu'::regclass)");

                entity.Property(e => e.qta_misu_in).HasColumnType("numeric(18,5)");

                entity.Property(e => e.qta_misu_out).HasColumnType("numeric(18,5)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_arti_ana_misu)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_arti_ana_misu_01");
            });

            modelBuilder.Entity<ana_arti_config_apisync>(entity =>
            {
                entity.ToTable("ana_arti_config_apisync", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_arti_config_apisync_01");

                entity.HasIndex(e => e.config_apisyncid)
                    .HasName("idx_ana_arti_config_apisync_02");

                entity.Property(e => e.ana_arti_config_apisyncid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_arti_config_apisync'::regclass)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_arti_config_apisync)
                    .HasForeignKey(d => d.ana_artiid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ana_arti_config_apisync_01");

                entity.HasOne(d => d.config_apisync)
                    .WithMany(p => p.ana_arti_config_apisync)
                    .HasForeignKey(d => d.config_apisyncid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ana_arti_config_apisync_02");
            });

            modelBuilder.Entity<ana_cate>(entity =>
            {
                entity.ToTable("ana_cate", Schema);

                entity.HasIndex(e => e.ana_catepadreid)
                    .HasName("idx_ana_cate_01");

                entity.Property(e => e.ana_cateid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_cate'::regclass)");

                entity.Property(e => e.cod_categoria).HasMaxLength(10);

                entity.Property(e => e.desc_categoria).HasMaxLength(250);

                entity.Property(e => e.label).HasMaxLength(3);

                entity.Property(e => e.sconto_a).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sconto_da).HasColumnType("numeric(6,2)");

                entity.HasOne(d => d.ana_catepadre)
                    .WithMany(p => p.Inverseana_catepadre)
                    .HasForeignKey(d => d.ana_catepadreid)
                    .HasConstraintName("fk_ana_cate_01");
            });

            modelBuilder.Entity<ana_cate_scontoconcordato>(entity =>
            {
                entity.ToTable("ana_cate_scontoconcordato", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ana_cate_scontoconcordato_01");

                entity.HasIndex(e => e.ana_cateid)
                    .HasName("idx_ana_cate_scontoconcordato_02");

                entity.Property(e => e.ana_cate_scontoconcordatoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_cate_scontoconcordato'::regclass)");

                entity.Property(e => e.sconto_a).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sconto_da).HasColumnType("numeric(6,2)");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ana_cate_scontoconcordato)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ana_cate_scontoconcordato_01");

                entity.HasOne(d => d.ana_cate)
                    .WithMany(p => p.ana_cate_scontoconcordato)
                    .HasForeignKey(d => d.ana_cateid)
                    .HasConstraintName("fk_ana_cate_scontoconcordato_02");
            });

            modelBuilder.Entity<ana_cauz>(entity =>
            {
                entity.ToTable("ana_cauz", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ana_cauz_01");

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_cauz_02");

                entity.Property(e => e.ana_cauzid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_cauz'::regclass)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_da_rendere).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_da_rendere_ap).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_resa).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_usci).HasColumnType("numeric(14,3)");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ana_cauz)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ana_cauz_01");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_cauz)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_cauz_02");
            });

            modelBuilder.Entity<ana_diba>(entity =>
            {
                entity.ToTable("ana_diba", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_diba_01");

                entity.Property(e => e.ana_dibaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_diba'::regclass)");

                entity.Property(e => e.cod_comp).HasMaxLength(13);

                entity.Property(e => e.des_comp).HasMaxLength(70);

                entity.Property(e => e.qta_gest).HasMaxLength(20);

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sel_comp).HasMaxLength(20);

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_diba)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_diba_01");
            });

            modelBuilder.Entity<ana_forn>(entity =>
            {
                entity.ToTable("ana_forn", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ana_forn_01");

                entity.HasIndex(e => e.age_valuid)
                    .HasName("idx_ana_forn_02");

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_forn_03");

                entity.Property(e => e.ana_fornid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_forn'::regclass)");

                entity.Property(e => e.allin_scad).HasMaxLength(100);

                entity.Property(e => e.cod_acqu).HasMaxLength(20);

                entity.Property(e => e.cond_seq).HasMaxLength(1);

                entity.Property(e => e.cond_sottose).HasMaxLength(2);

                entity.Property(e => e.desc_arti_omaggio).HasMaxLength(100);

                entity.Property(e => e.lis_base).HasColumnType("numeric(20,5)");

                entity.Property(e => e.lis_netto).HasColumnType("numeric(20,5)");

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.nev_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.perc_addebito_agg).HasColumnType("numeric(18,5)");

                entity.Property(e => e.perc_sconto_ricarico).HasColumnType("numeric(18,2)");

                entity.Property(e => e.periodicita_fatturaz).HasMaxLength(100);

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_cons).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_publ).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.rif_arti).HasMaxLength(14);

                entity.Property(e => e.ripresa_sconti_listino).HasMaxLength(1);

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_cassa).HasColumnType("numeric(18,5)");

                entity.Property(e => e.sco_text).HasMaxLength(20);

                entity.Property(e => e.segno_sconto_ricarico).HasMaxLength(1);

                entity.Property(e => e.tiporecord).HasMaxLength(1);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ana_forn)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ana_forn_01");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.ana_forn)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_ana_forn_02");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_forn)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_forn_03");
            });

            modelBuilder.Entity<ana_lifo>(entity =>
            {
                entity.ToTable("ana_lifo", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_lifo_01");

                entity.Property(e => e.ana_lifoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_lifo'::regclass)");

                entity.Property(e => e.cos_medi).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_medi).HasColumnType("numeric(20,5)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.pre_inve).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_inve).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_acqu).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_disp).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_entr).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_giac).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_impe).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_inve).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_ordi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_usci).HasColumnType("numeric(14,3)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_lifo)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_lifo_01");
            });

            modelBuilder.Entity<ana_list>(entity =>
            {
                entity.ToTable("ana_list", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_list_01");

                entity.HasIndex(e => e.top_listid)
                    .HasName("idx_ana_list_02");

                entity.Property(e => e.ana_listid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_list'::regclass)");

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.datafine_in).HasColumnType("date");

                entity.Property(e => e.datainizio_in).HasColumnType("date");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pro_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pro_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.prv_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ric_fissa).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.sconto_a).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sconto_da).HasColumnType("numeric(6,2)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_list)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_list_01");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.ana_list)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_ana_list_02");
            });

            modelBuilder.Entity<ana_losc>(entity =>
            {
                entity.ToTable("ana_losc", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_losc_01");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_ana_losc_02");

                entity.Property(e => e.ana_loscid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_losc'::regclass)");

                entity.Property(e => e.cod_losc).HasMaxLength(20);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_disp).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_entr).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_giac).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_impe).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_inve).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_ordi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_usci).HasColumnType("numeric(14,3)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_losc)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_losc_01");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.ana_losc)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_ana_losc_02");
            });

            modelBuilder.Entity<ana_lotto>(entity =>
            {
                entity.ToTable("ana_lotto", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_lotto_01");

                entity.HasIndex(e => e.doc_artiid)
                    .HasName("idx_ana_lotto_02");

                entity.HasIndex(e => e.ord_artiid)
                    .HasName("idx_ana_lotto_03");

                entity.HasIndex(e => e.pre_artiid)
                    .HasName("idx_ana_lotto_04");

                entity.Property(e => e.ana_lottoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_lotto'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(30);

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_lotto)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_lotto_01");

                entity.HasOne(d => d.doc_arti)
                    .WithMany(p => p.ana_lotto)
                    .HasForeignKey(d => d.doc_artiid)
                    .HasConstraintName("fk_ana_lotto_02");

                entity.HasOne(d => d.ord_arti)
                    .WithMany(p => p.ana_lotto)
                    .HasForeignKey(d => d.ord_artiid)
                    .HasConstraintName("fk_ana_lotto_03");

                entity.HasOne(d => d.pre_arti)
                    .WithMany(p => p.ana_lotto)
                    .HasForeignKey(d => d.pre_artiid)
                    .HasConstraintName("fk_ana_lotto_04");
            });

            modelBuilder.Entity<ana_maga>(entity =>
            {
                entity.ToTable("ana_maga", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_maga_01");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_ana_maga_02");

                entity.Property(e => e.ana_magaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_maga'::regclass)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_disp).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_entr).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_giac).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_impe).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_inve).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_ordi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_usci).HasColumnType("numeric(14,3)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_maga)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_maga_01");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.ana_maga)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_ana_maga_02");
            });

            modelBuilder.Entity<ana_marca>(entity =>
            {
                entity.ToTable("ana_marca", Schema);

                entity.HasIndex(e => e.nome)
                    .HasName("idx_ana_marca_01");

                entity.Property(e => e.ana_marcaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_marca'::regclass)");

                entity.Property(e => e.acq_sconto1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.acq_sconto2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.acq_sconto3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.acq_sconto4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.descrizione).HasMaxLength(150);

                entity.Property(e => e.nome).HasMaxLength(50);

                entity.Property(e => e.ven_prezzobase).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ven_ricarica).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ven_ricaricalistino).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ven_sconto1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ven_sconto2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ven_sconto3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ven_sconto4).HasColumnType("numeric(6,2)");
            });

            modelBuilder.Entity<ana_marca_top_list>(entity =>
            {
                entity.ToTable("ana_marca_top_list", Schema);

                entity.HasIndex(e => e.ana_marcaid)
                    .HasName("idx_ana_marca_top_list_01");

                entity.HasIndex(e => e.top_listid)
                    .HasName("idx_ana_marca_top_list_02");

                entity.Property(e => e.ana_marca_top_listid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_marca_top_list'::regclass)");

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.HasOne(d => d.ana_marca)
                    .WithMany(p => p.ana_marca_top_list)
                    .HasForeignKey(d => d.ana_marcaid)
                    .HasConstraintName("fk_ana_marca_top_list_01");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.ana_marca_top_list)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_ana_marca_top_list_02");
            });

            modelBuilder.Entity<ana_misu>(entity =>
            {
                entity.ToTable("ana_misu", Schema);

                entity.HasIndex(e => e.ana_misuid)
                    .HasName("idx_ana_misu_01");

                entity.Property(e => e.ana_misuid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_misu'::regclass)");

                entity.Property(e => e.misu_code)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.misu_desc).HasMaxLength(100);

                entity.HasOne(d => d.ana_misuNavigation)
                    .WithOne(p => p.Inverseana_misuNavigation)
                    .HasForeignKey<ana_misu>(d => d.ana_misuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ana_misu_01");
            });

            modelBuilder.Entity<ana_offe>(entity =>
            {
                entity.ToTable("ana_offe", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ana_offe_01");

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_offe_02");

                entity.HasIndex(e => e.ana_listid)
                    .HasName("idx_ana_offe_03");

                entity.Property(e => e.ana_offeid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_offe'::regclass)");

                entity.Property(e => e.cod_offe).HasMaxLength(20);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.gru_arti).HasMaxLength(20);

                entity.Property(e => e.key_offe).HasMaxLength(10);

                entity.Property(e => e.mar_arti).HasMaxLength(20);

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_base).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(20);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ana_offe)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ana_offe_01");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_offe)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_offe_02");

                entity.HasOne(d => d.ana_list)
                    .WithMany(p => p.ana_offe)
                    .HasForeignKey(d => d.ana_listid)
                    .HasConstraintName("fk_ana_offe_03");
            });

            modelBuilder.Entity<ana_pari>(entity =>
            {
                entity.ToTable("ana_pari", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_pari_01");

                entity.Property(e => e.ana_pariid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_pari'::regclass)");

                entity.Property(e => e.cod_pari).HasMaxLength(13);

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_pari)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_pari_01");
            });

            modelBuilder.Entity<ana_prov>(entity =>
            {
                entity.ToTable("ana_prov", Schema);

                entity.Property(e => e.ana_provid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_prov'::regclass)");

                entity.Property(e => e.ana_provnome).HasMaxLength(255);

                entity.Property(e => e.ana_provsigla).HasMaxLength(255);
            });

            modelBuilder.Entity<ana_subcate>(entity =>
            {
                entity.ToTable("ana_subcate", Schema);

                entity.HasIndex(e => e.ana_cateid)
                    .HasName("idx_ana_subcate_01");

                entity.Property(e => e.ana_subcateid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_subcate'::regclass)");

                entity.Property(e => e.cod_subcate).HasMaxLength(100);

                entity.Property(e => e.desc_subcate).HasMaxLength(250);

                entity.Property(e => e.prezzomenu).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.ana_cate)
                    .WithMany(p => p.ana_subcate)
                    .HasForeignKey(d => d.ana_cateid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ana_subcate_01");
            });

            modelBuilder.Entity<ana_ubicazione>(entity =>
            {
                entity.ToTable("ana_ubicazione", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ana_ubicazione_01");

                entity.HasIndex(e => e.doc_artiid)
                    .HasName("idx_ana_ubicazione_02");

                entity.HasIndex(e => e.ord_artiid)
                    .HasName("idx_ana_ubicazione_03");

                entity.HasIndex(e => e.pre_artiid)
                    .HasName("idx_ana_ubicazione_04");

                entity.HasIndex(e => e.top_maga_posizioneid)
                    .HasName("idx_ana_ubicazione_05");

                entity.HasIndex(e => e.top_maga_scaffaleid)
                    .HasName("idx_ana_ubicazione_06");

                entity.HasIndex(e => e.top_maga_settoreid)
                    .HasName("idx_ana_ubicazione_07");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_ana_ubicazione_08");

                entity.Property(e => e.ana_ubicazioneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ana_ubicazione'::regclass)");

                entity.Property(e => e.quantitamovimentoentrata).HasColumnType("numeric(14,3)");

                entity.Property(e => e.quantitamovimentouscita).HasColumnType("numeric(14,3)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ana_ubicazioneid_01");

                entity.HasOne(d => d.doc_arti)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.doc_artiid)
                    .HasConstraintName("fk_ana_ubicazioneid_02");

                entity.HasOne(d => d.ord_arti)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.ord_artiid)
                    .HasConstraintName("fk_ana_ubicazioneid_03");

                entity.HasOne(d => d.pre_arti)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.pre_artiid)
                    .HasConstraintName("fk_ana_ubicazioneid_04");

                entity.HasOne(d => d.top_maga_posizione)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.top_maga_posizioneid)
                    .HasConstraintName("fk_ana_ubicazioneid_06");

                entity.HasOne(d => d.top_maga_scaffale)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.top_maga_scaffaleid)
                    .HasConstraintName("fk_ana_ubicazioneid_07");

                entity.HasOne(d => d.top_maga_settore)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.top_maga_settoreid)
                    .HasConstraintName("fk_ana_ubicazioneid_08");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.ana_ubicazione)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_ana_ubicazioneid_05");
            });

            modelBuilder.Entity<api_pendingsync>(entity =>
            {
                entity.ToTable("api_pendingsync", Schema);

                entity.Property(e => e.api_pendingsyncid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_api_pendingsync'::regclass)");

                entity.Property(e => e.entityname).HasMaxLength(50);
            });

            modelBuilder.Entity<apisync_mapping>(entity =>
            {
                entity.ToTable("apisync_mapping", Schema);

                entity.Property(e => e.apisync_mappingid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_apisync_mapping'::regclass)");

                entity.Property(e => e.entityname).HasMaxLength(100);

                entity.Property(e => e.valuestring).HasMaxLength(500);
            });

            modelBuilder.Entity<apisync_user>(entity =>
            {
                entity.ToTable("apisync_user", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_apisync_user_01");

                entity.HasIndex(e => e.config_apisyncid)
                    .HasName("idx_apisync_user_02");

                entity.Property(e => e.apisync_userid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_apisync_user'::regclass)");

                entity.Property(e => e.pwd).HasColumnType("character varying");

                entity.Property(e => e.tipo).HasMaxLength(1);

                entity.Property(e => e.usr).HasMaxLength(50);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.apisync_user)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_apisync_user_01");

                entity.HasOne(d => d.config_apisync)
                    .WithMany(p => p.apisync_user)
                    .HasForeignKey(d => d.config_apisyncid)
                    .HasConstraintName("fk_apisync_user_02");
            });

            modelBuilder.Entity<appgroup>(entity =>
            {
                entity.ToTable("appgroup", Schema);

                entity.Property(e => e.description).HasMaxLength(50);

                entity.Property(e => e.diritti).HasMaxLength(50);

                entity.Property(e => e.enabled)
                    .IsRequired()
                    .HasDefaultValueSql("true");

                entity.Property(e => e.name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<appuser>(entity =>
            {
                entity.ToTable("appuser", Schema);

                entity.Property(e => e.pckey).HasMaxLength(50);

                entity.Property(e => e.username)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<appuser_ana_cate>(entity =>
            {
                entity.ToTable("appuser_ana_cate", Schema);

                entity.Property(e => e.appuser_ana_cateid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_appuser_ana_cate'::regclass)");
            });

            modelBuilder.Entity<appuser_group>(entity =>
            {
                entity.ToTable("appuser_group", Schema);

                entity.HasIndex(e => new { e.appuserid, e.groupid })
                    .HasName("idx_user_group_01")
                    .IsUnique();
            });

            modelBuilder.Entity<appuser_tocken>(entity =>
            {
                entity.ToTable("appuser_tocken", Schema);
            });

            modelBuilder.Entity<appuserpassword>(entity =>
            {
                entity.ToTable("appuserpassword", Schema);
            });

            modelBuilder.Entity<articolirapidi>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("articolirapidi", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.gru_arti)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<articoloclass>(entity =>
            {
                entity.ToTable("articoloclass", Schema);

                entity.Property(e => e.articoloclassid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_articoloclass'::regclass)");

                entity.Property(e => e.codarti).HasMaxLength(100);

                entity.Property(e => e.codicesito).HasMaxLength(200);

                entity.Property(e => e.idgroup).HasMaxLength(200);
            });

            modelBuilder.Entity<bilancio>(entity =>
            {
                entity.ToTable("bilancio", Schema);

                entity.Property(e => e.bilancioid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_bilancio'::regclass)");

                entity.Property(e => e.anno)
                    .IsRequired()
                    .HasMaxLength(4);
            });

            modelBuilder.Entity<cal_even>(entity =>
            {
                entity.ToTable("cal_even", Schema);

                entity.Property(e => e.cal_evenid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_cal_even'::regclass)");

                entity.Property(e => e.goog_evenid)
                    .HasMaxLength(100)
                    .IsFixedLength();
            });

            modelBuilder.Entity<causale>(entity =>
            {
                entity.ToTable("causale", Schema);

                entity.HasIndex(e => e.registroivaid)
                    .HasName("idx_causale_01");

                entity.HasIndex(e => e.tipomovimentoid)
                    .HasName("idx_causale_02");

                entity.Property(e => e.causaleid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_causale'::regclass)");

                entity.Property(e => e.anno).HasMaxLength(4);

                entity.Property(e => e.codice).HasMaxLength(3);

                entity.Property(e => e.descrizione).HasMaxLength(100);

                entity.Property(e => e.descrizioneprimanota).HasMaxLength(100);

                entity.Property(e => e.letteranol).HasMaxLength(50);

                entity.Property(e => e.tipoquadro).HasMaxLength(10);

                entity.HasOne(d => d.registroiva)
                    .WithMany(p => p.causale)
                    .HasForeignKey(d => d.registroivaid)
                    .HasConstraintName("fk_causale_01");

                entity.HasOne(d => d.tipomovimento)
                    .WithMany(p => p.causale)
                    .HasForeignKey(d => d.tipomovimentoid)
                    .HasConstraintName("fk_causale_02");
            });

            modelBuilder.Entity<causale_contropartita>(entity =>
            {
                entity.ToTable("causale_contropartita", Schema);

                entity.Property(e => e.causale_contropartitaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_causale_contropartita'::regclass)");
            });

            modelBuilder.Entity<causale_iva>(entity =>
            {
                entity.ToTable("causale_iva", Schema);

                entity.HasIndex(e => e.causaleid)
                    .HasName("idx_causale_iva_01");

                entity.HasIndex(e => e.pianodeicontiid)
                    .HasName("idx_causale_iva_02");

                entity.Property(e => e.causale_ivaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_causale_iva'::regclass)");

                entity.HasOne(d => d.causale)
                    .WithMany(p => p.causale_iva)
                    .HasForeignKey(d => d.causaleid)
                    .HasConstraintName("fk_causale_iva_01");

                entity.HasOne(d => d.pianodeiconti)
                    .WithMany(p => p.causale_iva)
                    .HasForeignKey(d => d.pianodeicontiid)
                    .HasConstraintName("fk_causale_iva_02");
            });

            modelBuilder.Entity<codice_carica>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("codice_carica", Schema);

                entity.Property(e => e.descrizione_carica).HasMaxLength(500);
            });

            modelBuilder.Entity<codici_ateco>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("codici_ateco", Schema);

                entity.Property(e => e.codice).HasMaxLength(255);

                entity.Property(e => e.descrizione)
                    .HasMaxLength(255)
                    .HasComment("Flag di modifica valido a partire dal 1 gennaio 2009");
            });

            modelBuilder.Entity<codiciabarre>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("codiciabarre", Schema);

                entity.Property(e => e.attuale).HasMaxLength(255);

                entity.Property(e => e.nuovo).HasMaxLength(255);
            });

            modelBuilder.Entity<config_apisync>(entity =>
            {
                entity.ToTable("config_apisync", Schema);

                entity.Property(e => e.config_apisyncid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_config_apisync'::regclass)");

                entity.Property(e => e.pwd).HasMaxLength(200);

                entity.Property(e => e.tipoapi)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.url)
                    .IsRequired()
                    .HasMaxLength(800);

                entity.Property(e => e.usr)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<configuration>(entity =>
            {
                entity.ToTable("configuration", Schema);

                entity.HasIndex(e => e.category)
                    .HasName("idx_configuration_01");

                entity.HasIndex(e => e.name)
                    .HasName("idx_configuration_02");

                entity.HasIndex(e => e.version)
                    .HasName("idx_configuration_03");

                entity.Property(e => e.category)
                    .IsRequired()
                    .HasMaxLength(32);

                entity.Property(e => e.name)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.value).IsRequired();

                entity.Property(e => e.version)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<configurazione>(entity =>
            {
                entity.ToTable("configurazione", Schema);

                entity.HasIndex(e => e.registroiva_liqid)
                    .HasName("idx_configurazione_01");

                entity.Property(e => e.configurazioneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_configurazione'::regclass)");

                entity.Property(e => e.anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.giornale_avere).HasColumnType("numeric(20,5)");

                entity.Property(e => e.giornale_dare).HasColumnType("numeric(20,5)");

                entity.Property(e => e.giornale_mese).HasColumnType("numeric(2,0)");

                entity.Property(e => e.giornale_ultimapagina).HasColumnType("numeric(10,0)");

                entity.Property(e => e.giornale_ultimoprogressivo).HasColumnType("numeric(10,0)");

                entity.Property(e => e.ivaagosto).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivaannoprecedente).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivaaprile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivadicembre).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivafebbraio).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivagennaio).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivagiugno).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivaluglio).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivamaggio).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivamarzo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivanovembre).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivaottobre).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ivasettembre).HasColumnType("numeric(20,5)");

                entity.Property(e => e.modalitastampaliqregistroiva)
                    .HasMaxLength(100)
                    .HasComment("0: Vendite - 1: Riepilogativo");

                entity.Property(e => e.progressivoprimanota).HasColumnType("numeric(10,0)");

                entity.Property(e => e.riepilogo_mese).HasColumnType("numeric(2,0)");

                entity.Property(e => e.riepilogo_ultimapagina).HasColumnType("numeric(10,0)");

                entity.Property(e => e.tipoiva).HasMaxLength(20);

                entity.HasOne(d => d.registroiva_liq)
                    .WithMany(p => p.configurazione)
                    .HasForeignKey(d => d.registroiva_liqid)
                    .HasConstraintName("fk_configurazione_01");
            });

            modelBuilder.Entity<contabilizzazione>(entity =>
            {
                entity.ToTable("contabilizzazione", Schema);

                entity.Property(e => e.contabilizzazioneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_contabilizzazione'::regclass)");
            });

            modelBuilder.Entity<ddt_gene>(entity =>
            {
                entity.ToTable("ddt_gene", Schema);

                entity.HasIndex(e => e.age_bankid)
                    .HasName("idx_ddt_gene_01");

                entity.HasIndex(e => e.age_cdcid)
                    .HasName("idx_ddt_gene_02");

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ddt_gene_03");

                entity.HasIndex(e => e.age_nomiidrap_1)
                    .HasName("idx_ddt_gene_04");

                entity.HasIndex(e => e.age_nomiidrap_2)
                    .HasName("idx_ddt_gene_05");

                entity.HasIndex(e => e.age_pagaid)
                    .HasName("idx_ddt_gene_06");

                entity.HasIndex(e => e.age_statid)
                    .HasName("idx_ddt_gene_07");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_ddt_gene_08");

                entity.HasIndex(e => e.age_valuid)
                    .HasName("idx_ddt_gene_09");

                entity.HasIndex(e => e.top_causid)
                    .HasName("idx_ddt_gene_10");

                entity.HasIndex(e => e.top_listid)
                    .HasName("idx_ddt_gene_11");

                entity.HasIndex(e => e.top_maga_posizioneid)
                    .HasName("idx_ddt_gene_12");

                entity.HasIndex(e => e.top_maga_scaffaleid)
                    .HasName("idx_ddt_gene_13");

                entity.HasIndex(e => e.top_maga_settoreid)
                    .HasName("idx_ddt_gene_14");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_ddt_gene_15");

                entity.Property(e => e.ddt_geneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ddt_gene'::regclass)");

                entity.Property(e => e.anno_oggetto).HasMaxLength(250);

                entity.Property(e => e.chg_valu).HasColumnType("numeric(20,10)");

                entity.Property(e => e.chilometri_oggetto).HasMaxLength(250);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_tari).HasMaxLength(3);

                entity.Property(e => e.codice_pin).HasMaxLength(50);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.controllo_documento).HasMaxLength(50);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.data_consegna).HasColumnType("date");

                entity.Property(e => e.des_avvi).HasMaxLength(10);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_quad).HasMaxLength(20);

                entity.Property(e => e.doc_resa).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.doc_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.importo_pagato).HasColumnType("numeric(20,5)");

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_sede).HasMaxLength(10);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.modello_oggetto).HasMaxLength(250);

                entity.Property(e => e.num_boll).HasMaxLength(13);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.ora).HasMaxLength(50);

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_epal).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.targa_oggetto).HasMaxLength(250);

                entity.Property(e => e.telaio_oggetto).HasMaxLength(250);

                entity.Property(e => e.telefonocliente).HasMaxLength(250);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_vari).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.age_bank)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.age_bankid)
                    .HasConstraintName("fk_ddt_gene_01");

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_ddt_gene_02");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ddt_geneage_nomi)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ddt_gene_03");

                entity.HasOne(d => d.age_nomiidrap_1Navigation)
                    .WithMany(p => p.ddt_geneage_nomiidrap_1Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_1)
                    .HasConstraintName("fk_ddt_gene_04");

                entity.HasOne(d => d.age_nomiidrap_2Navigation)
                    .WithMany(p => p.ddt_geneage_nomiidrap_2Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_2)
                    .HasConstraintName("fk_ddt_gene_05");

                entity.HasOne(d => d.age_paga)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.age_pagaid)
                    .HasConstraintName("fk_ddt_gene_06");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_ddt_gene_07");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_ddt_gene_08");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_ddt_gene_09");

                entity.HasOne(d => d.top_caus)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.top_causid)
                    .HasConstraintName("fk_ddt_gene_10");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_ddt_gene_11");

                entity.HasOne(d => d.top_maga_posizione)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.top_maga_posizioneid)
                    .HasConstraintName("fk_ddt_gene_13");

                entity.HasOne(d => d.top_maga_scaffale)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.top_maga_scaffaleid)
                    .HasConstraintName("fk_ddt_gene_14");

                entity.HasOne(d => d.top_maga_settore)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.top_maga_settoreid)
                    .HasConstraintName("fk_ddt_gene_15");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.ddt_gene)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_ddt_gene_12");
            });

            modelBuilder.Entity<delete_vmage_cont_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("delete_vmage_cont_view", Schema);

                entity.Property(e => e.cnt_mail).HasColumnType("character varying");

                entity.Property(e => e.cnt_nome).HasMaxLength(50);

                entity.Property(e => e.tel_ncel).HasColumnType("character varying");

                entity.Property(e => e.tel_nfax).HasColumnType("character varying");

                entity.Property(e => e.tel_nume).HasColumnType("character varying");
            });

            modelBuilder.Entity<distinta_base>(entity =>
            {
                entity.ToTable("distinta_base", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_distinta_base_01");

                entity.Property(e => e.distinta_baseid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_distinta_base'::regclass)");

                entity.Property(e => e.qta).HasColumnType("numeric(18,5)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.distinta_base)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_distinta_base_01");
            });

            modelBuilder.Entity<doc_arti>(entity =>
            {
                entity.ToTable("doc_arti", Schema);

                entity.HasIndex(e => e.age_cdcid)
                    .HasName("idx_doc_arti_01");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_doc_arti_02");

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_doc_arti_03");

                entity.HasIndex(e => e.ana_loscid)
                    .HasName("idx_doc_arti_04");

                entity.HasIndex(e => e.ddt_geneid)
                    .HasName("idx_doc_arti_05");

                entity.HasIndex(e => e.doc_arti_padreid)
                    .HasName("idx_doc_arti_06");

                entity.HasIndex(e => e.fat_geneid)
                    .HasName("idx_doc_arti_07");

                entity.HasIndex(e => e.fidelityage_nomiid)
                    .HasName("idx_doc_arti_08");

                entity.HasIndex(e => e.mov_geneid)
                    .HasName("idx_doc_arti_09");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_doc_arti_10");

                entity.Property(e => e.doc_artiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_doc_arti'::regclass)");

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_matr).HasColumnType("character varying");

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_rife).HasMaxLength(20);

                entity.Property(e => e.cod_type).HasColumnType("character varying");

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.des_arti_sostitutiva).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_arti).HasMaxLength(36);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_rife).HasMaxLength(13);

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pro_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pro_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.prv_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_coll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_conf).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_distinta).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_logi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.tipo_doc_importazione).HasMaxLength(50);

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_doc_arti_01");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_doc_arti_03");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_doc_arti_04");

                entity.HasOne(d => d.ana_losc)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.ana_loscid)
                    .HasConstraintName("fk_doc_arti_05");

                entity.HasOne(d => d.ddt_gene)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.ddt_geneid)
                    .HasConstraintName("fk_doc_arti_06");

                entity.HasOne(d => d.doc_arti_padre)
                    .WithMany(p => p.Inversedoc_arti_padre)
                    .HasForeignKey(d => d.doc_arti_padreid)
                    .HasConstraintName("fk_doc_arti_07");

                entity.HasOne(d => d.fat_gene)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.fat_geneid)
                    .HasConstraintName("fk_doc_arti_08");

                entity.HasOne(d => d.fidelityage_nomi)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.fidelityage_nomiid)
                    .HasConstraintName("fk_doc_arti_02");

                entity.HasOne(d => d.mov_gene)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.mov_geneid)
                    .HasConstraintName("fk_doc_arti_09");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.doc_arti)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_doc_arti_10");
            });

            modelBuilder.Entity<fasce_orarie>(entity =>
            {
                entity.ToTable("fasce_orarie", Schema);

                entity.Property(e => e.fasce_orarieid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fasce_orarie'::regclass)");

                entity.Property(e => e.aora)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsFixedLength();

                entity.Property(e => e.daora)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsFixedLength();
            });

            modelBuilder.Entity<fat_gene>(entity =>
            {
                entity.ToTable("fat_gene", Schema);

                entity.HasIndex(e => e.age_bankid)
                    .HasName("idx_fat_gene_01");

                entity.HasIndex(e => e.age_cdcid)
                    .HasName("idx_fat_gene_02");

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_fat_gene_03");

                entity.HasIndex(e => e.age_nomiidrap_1)
                    .HasName("idx_fat_gene_04");

                entity.HasIndex(e => e.age_nomiidrap_2)
                    .HasName("idx_fat_gene_05");

                entity.HasIndex(e => e.age_pagaid)
                    .HasName("idx_fat_gene_06");

                entity.HasIndex(e => e.age_statid)
                    .HasName("idx_fat_gene_07");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_fat_gene_08");

                entity.HasIndex(e => e.age_valuid)
                    .HasName("idx_fat_gene_09");

                entity.HasIndex(e => e.top_causid)
                    .HasName("idx_fat_gene_10");

                entity.HasIndex(e => e.top_listid)
                    .HasName("idx_fat_gene_11");

                entity.HasIndex(e => e.top_maga_posizioneid)
                    .HasName("idx_fat_gene_12");

                entity.HasIndex(e => e.top_maga_scaffaleid)
                    .HasName("idx_fat_gene_13");

                entity.HasIndex(e => e.top_maga_settoreid)
                    .HasName("idx_fat_gene_14");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_fat_gene_15");

                entity.Property(e => e.fat_geneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fat_gene'::regclass)");

                entity.Property(e => e.chg_valu).HasColumnType("numeric(20,10)");

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_tari).HasMaxLength(13);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_avvi).HasMaxLength(10);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_quad).HasMaxLength(20);

                entity.Property(e => e.doc_resa).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.fel_fatt_ele_xml).HasColumnType("xml");

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_sede).HasMaxLength(10);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_fatt).HasMaxLength(13);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.olsa_uid).HasMaxLength(50);

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ritenuta_acconto_cau_paga).HasMaxLength(3);

                entity.Property(e => e.ritenuta_importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ritenuta_percentuale).HasColumnType("numeric(18,5)");

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_epal).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_vari).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.age_bank)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.age_bankid)
                    .HasConstraintName("fk_fat_gene_01");

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_fat_gene_02");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.fat_geneage_nomi)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_fat_gene_03");

                entity.HasOne(d => d.age_nomiidrap_1Navigation)
                    .WithMany(p => p.fat_geneage_nomiidrap_1Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_1)
                    .HasConstraintName("fk_fat_gene_04");

                entity.HasOne(d => d.age_nomiidrap_2Navigation)
                    .WithMany(p => p.fat_geneage_nomiidrap_2Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_2)
                    .HasConstraintName("fk_fat_gene_05");

                entity.HasOne(d => d.age_paga)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.age_pagaid)
                    .HasConstraintName("fk_fat_gene_06");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_fat_gene_07");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_fat_gene_08");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_fat_gene_09");

                entity.HasOne(d => d.top_caus)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.top_causid)
                    .HasConstraintName("fk_fat_gene_10");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_fat_gene_11");

                entity.HasOne(d => d.top_maga_posizione)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.top_maga_posizioneid)
                    .HasConstraintName("fk_fat_gene_13");

                entity.HasOne(d => d.top_maga_scaffale)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.top_maga_scaffaleid)
                    .HasConstraintName("fk_fat_gene_14");

                entity.HasOne(d => d.top_maga_settore)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.top_maga_settoreid)
                    .HasConstraintName("fk_fat_gene_15");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.fat_gene)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_fat_gene_12");
            });

            modelBuilder.Entity<fat_gene_rate>(entity =>
            {
                entity.ToTable("fat_gene_rate", Schema);

                entity.HasIndex(e => e.fat_geneid)
                    .HasName("idx_fat_gene_rate_01");

                entity.HasIndex(e => e.tip_pagaid)
                    .HasName("idx_fat_gene_rate_02");

                entity.Property(e => e.fat_gene_rateid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fat_gene_rate'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.fat_gene)
                    .WithMany(p => p.fat_gene_rate)
                    .HasForeignKey(d => d.fat_geneid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_fat_gene_rate_01");

                entity.HasOne(d => d.tip_paga)
                    .WithMany(p => p.fat_gene_rate)
                    .HasForeignKey(d => d.tip_pagaid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_fat_gene_rate_02");
            });

            modelBuilder.Entity<fat_gene_rate_pagamenti>(entity =>
            {
                entity.ToTable("fat_gene_rate_pagamenti", Schema);

                entity.HasIndex(e => e.fat_gene_rateid)
                    .HasName("idx_fat_gene_rate_pagamenti_01");

                entity.HasIndex(e => e.tip_pagaid)
                    .HasName("idx_fat_gene_rate_pagamenti_02");

                entity.Property(e => e.fat_gene_rate_pagamentiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fat_gene_rate_pagamenti'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.note).HasMaxLength(250);

                entity.HasOne(d => d.fat_gene_rate)
                    .WithMany(p => p.fat_gene_rate_pagamenti)
                    .HasForeignKey(d => d.fat_gene_rateid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_fat_gene_rate_pagamenti_01");

                entity.HasOne(d => d.tip_paga)
                    .WithMany(p => p.fat_gene_rate_pagamenti)
                    .HasForeignKey(d => d.tip_pagaid)
                    .HasConstraintName("fk_fat_gene_rate_pagamenti_02");
            });

            modelBuilder.Entity<fat_passiva>(entity =>
            {
                entity.ToTable("fat_passiva", Schema);

                entity.HasIndex(e => e.external_id)
                    .HasName("idx_fat_passiva_01");

                entity.Property(e => e.fat_passivaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fat_passiva'::regclass)");

                entity.Property(e => e.cod_fisc)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.denom_mitt).HasMaxLength(200);

                entity.Property(e => e.descrizione).HasMaxLength(250);

                entity.Property(e => e.external_id)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.external_path)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.key_anno)
                    .IsRequired()
                    .HasMaxLength(4);

                entity.Property(e => e.num_doc)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.num_fatt)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.par_tiva)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.payloadjson)
                    .IsRequired()
                    .HasColumnType("json");

                entity.Property(e => e.rif_amm).HasMaxLength(250);

                entity.Property(e => e.tip_doc).HasMaxLength(10);

                entity.Property(e => e.tipoprovider)
                    .IsRequired()
                    .HasMaxLength(2);
            });

            modelBuilder.Entity<fat_passiva_rate>(entity =>
            {
                entity.ToTable("fat_passiva_rate", Schema);

                entity.HasIndex(e => e.fat_passivaid)
                    .HasName("idx_fat_passiva_rate_01");

                entity.HasIndex(e => e.mod_paga)
                    .HasName("idx_fat_passiva_rate_02");

                entity.Property(e => e.fat_passiva_rateid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fat_passiva_rate'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.mod_paga).HasMaxLength(10);

                entity.HasOne(d => d.fat_passiva)
                    .WithMany(p => p.fat_passiva_rate)
                    .HasForeignKey(d => d.fat_passivaid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_fat_passiva_rate_01");
            });

            modelBuilder.Entity<fat_passiva_rate_pagamenti>(entity =>
            {
                entity.ToTable("fat_passiva_rate_pagamenti", Schema);

                entity.HasIndex(e => e.fat_passiva_rateid)
                    .HasName("idx_fat_passiva_rate_pagamenti_01");

                entity.HasIndex(e => e.tip_pagaid)
                    .HasName("idx_fat_passiva_rate_pagamenti_02");

                entity.Property(e => e.fat_passiva_rate_pagamentiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fat_passiva_rate_pagamenti'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.note).HasMaxLength(250);

                entity.HasOne(d => d.fat_passiva_rate)
                    .WithMany(p => p.fat_passiva_rate_pagamenti)
                    .HasForeignKey(d => d.fat_passiva_rateid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_fat_passiva_rate_pagamenti_01");

                entity.HasOne(d => d.tip_paga)
                    .WithMany(p => p.fat_passiva_rate_pagamenti)
                    .HasForeignKey(d => d.tip_pagaid)
                    .HasConstraintName("fk_fat_passiva_rate_pagamenti_02");
            });

            modelBuilder.Entity<fel_fat_gene_stato>(entity =>
            {
                entity.ToTable("fel_fat_gene_stato", Schema);

                entity.HasIndex(e => e.fat_geneid)
                    .HasName("idx_fel_fat_gene_stato_01");

                entity.HasIndex(e => e.fel_statoid)
                    .HasName("idx_fel_fat_gene_stato_02");

                entity.Property(e => e.fel_fat_gene_statoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fel_fat_gene_stato'::regclass)");

                entity.HasOne(d => d.fat_gene)
                    .WithMany(p => p.fel_fat_gene_stato)
                    .HasForeignKey(d => d.fat_geneid)
                    .HasConstraintName("fk_fel_fat_gene_stato_01");

                entity.HasOne(d => d.fel_stato)
                    .WithMany(p => p.fel_fat_gene_stato)
                    .HasForeignKey(d => d.fel_statoid)
                    .HasConstraintName("fk_fel_fat_gene_stato_02");
            });

            modelBuilder.Entity<fel_map_age_tiva>(entity =>
            {
                entity.ToTable("fel_map_age_tiva", Schema);

                entity.Property(e => e.fel_map_age_tivaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fel_map_age_tiva'::regclass)");

                entity.Property(e => e.natura).HasMaxLength(2);

                entity.Property(e => e.riferimento_normativo).HasMaxLength(100);
            });

            modelBuilder.Entity<fel_progr>(entity =>
            {
                entity.ToTable("fel_progr", Schema);

                entity.HasIndex(e => e.fat_geneid)
                    .HasName("idx_fel_progr_01");

                entity.Property(e => e.fel_progrid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fel_progr'::regclass)");

                entity.Property(e => e.progressivo)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.HasOne(d => d.fat_gene)
                    .WithMany(p => p.fel_progr)
                    .HasForeignKey(d => d.fat_geneid)
                    .HasConstraintName("fk_fel_progr_01");
            });

            modelBuilder.Entity<fel_stato>(entity =>
            {
                entity.ToTable("fel_stato", Schema);

                entity.Property(e => e.fel_statoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_fel_stato'::regclass)");

                entity.Property(e => e.cod_stato).HasMaxLength(50);

                entity.Property(e => e.des_stato).HasMaxLength(250);
            });

            modelBuilder.Entity<group_tocken>(entity =>
            {
                entity.ToTable("group_tocken", Schema);

                entity.HasIndex(e => new { e.appgroupid, e.tockenid })
                    .HasName("idx_group_tocken_01")
                    .IsUnique();

                entity.Property(e => e.group_tockenid).ValueGeneratedNever();
            });

            modelBuilder.Entity<ho_abbinamenti>(entity =>
            {
                entity.ToTable("ho_abbinamenti", Schema);

                entity.HasIndex(e => e.ana_artiabbinamentoid)
                    .HasName("idx_ho_abbinamenti_01");

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ho_abbinamenti_02");

                entity.Property(e => e.ho_abbinamentiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_abbinamenti'::regclass)");

                entity.HasOne(d => d.ana_artiabbinamento)
                    .WithMany(p => p.ho_abbinamentiana_artiabbinamento)
                    .HasForeignKey(d => d.ana_artiabbinamentoid)
                    .HasConstraintName("fk_ho_abbinamenti_02");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ho_abbinamentiana_arti)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ho_abbinamenti_01");
            });

            modelBuilder.Entity<ho_configurazionestampante>(entity =>
            {
                entity.ToTable("ho_configurazionestampante", Schema);

                entity.HasIndex(e => e.id_ana_cate)
                    .HasName("idx_ho_configurazionestampante_01");

                entity.HasIndex(e => e.id_ana_subcate)
                    .HasName("idx_ho_configurazionestampante_02");

                entity.HasIndex(e => e.id_mapmenuprod)
                    .HasName("idx_ho_configurazionestampante_03");

                entity.HasIndex(e => e.id_sala)
                    .HasName("idx_ho_configurazionestampante_04");

                entity.Property(e => e.ho_configurazionestampanteid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_configurazionestampante'::regclass)");

                entity.Property(e => e.id_stampante).HasMaxLength(200);

                entity.HasOne(d => d.id_ana_cateNavigation)
                    .WithMany(p => p.ho_configurazionestampante)
                    .HasForeignKey(d => d.id_ana_cate)
                    .HasConstraintName("fk_ho_configurazionestampante_01");

                entity.HasOne(d => d.id_ana_subcateNavigation)
                    .WithMany(p => p.ho_configurazionestampante)
                    .HasForeignKey(d => d.id_ana_subcate)
                    .HasConstraintName("fk_ho_configurazionestampante_02");

                entity.HasOne(d => d.id_mapmenuprodNavigation)
                    .WithMany(p => p.ho_configurazionestampante)
                    .HasForeignKey(d => d.id_mapmenuprod)
                    .HasConstraintName("fk_ho_configurazionestampante_03");

                entity.HasOne(d => d.id_salaNavigation)
                    .WithMany(p => p.ho_configurazionestampante)
                    .HasForeignKey(d => d.id_sala)
                    .HasConstraintName("fk_ho_configurazionestampante_04");
            });

            modelBuilder.Entity<ho_conti>(entity =>
            {
                entity.ToTable("ho_conti", Schema);

                entity.HasIndex(e => e.id_map_sala_tavolo)
                    .HasName("idx_ho_conti_01");

                entity.Property(e => e.ho_contiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_conti'::regclass)");

                entity.Property(e => e.id_group).HasMaxLength(50);

                entity.Property(e => e.totale_conto).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.id_map_sala_tavoloNavigation)
                    .WithMany(p => p.ho_conti)
                    .HasForeignKey(d => d.id_map_sala_tavolo)
                    .HasConstraintName("fk_ho_conti_01");
            });

            modelBuilder.Entity<ho_contopiet>(entity =>
            {
                entity.ToTable("ho_contopiet", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ho_contopiet_01");

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ho_contopiet_02");

                entity.HasIndex(e => e.ho_gruppoordinazioneid)
                    .HasName("idx_ho_contopiet_03");

                entity.HasIndex(e => e.ho_tavoloanagraficaorigine)
                    .HasName("idx_ho_contopiet_04");

                entity.HasIndex(e => e.id_anasubcate)
                    .HasName("idx_ho_contopiet_05");

                entity.HasIndex(e => e.id_map_menu_prod)
                    .HasName("idx_ho_contopiet_06");

                entity.HasIndex(e => e.id_padre)
                    .HasName("idx_ho_contopiet_07");

                entity.Property(e => e.ho_contopietid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_contopiet'::regclass)");

                entity.Property(e => e.art_misu2).HasMaxLength(50);

                entity.Property(e => e.commento).HasMaxLength(250);

                entity.Property(e => e.descrizione).HasMaxLength(500);

                entity.Property(e => e.id_group)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.peso).HasColumnType("numeric(9,4)");

                entity.Property(e => e.prezzo).HasColumnType("numeric(9,5)");

                entity.Property(e => e.quantita).HasColumnType("numeric(9,2)");

                entity.Property(e => e.sconto).HasColumnType("numeric(9,6)");

                entity.Property(e => e.subtotale).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tipo).HasMaxLength(50);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ho_contopiet)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ho_contopiet_01");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ho_contopiet)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ho_contopiet_02");

                entity.HasOne(d => d.ho_gruppoordinazione)
                    .WithMany(p => p.ho_contopiet)
                    .HasForeignKey(d => d.ho_gruppoordinazioneid)
                    .HasConstraintName("fk_ho_contopiet_05");

                entity.HasOne(d => d.ho_tavoloanagraficaorigineNavigation)
                    .WithMany(p => p.ho_contopiet)
                    .HasForeignKey(d => d.ho_tavoloanagraficaorigine)
                    .HasConstraintName("fk_ho_contopiet_07");

                entity.HasOne(d => d.id_anasubcateNavigation)
                    .WithMany(p => p.ho_contopiet)
                    .HasForeignKey(d => d.id_anasubcate)
                    .HasConstraintName("fk_ho_contopiet_03");

                entity.HasOne(d => d.id_map_menu_prodNavigation)
                    .WithMany(p => p.ho_contopiet)
                    .HasForeignKey(d => d.id_map_menu_prod)
                    .HasConstraintName("fk_ho_contopiet_06");

                entity.HasOne(d => d.id_padreNavigation)
                    .WithMany(p => p.Inverseid_padreNavigation)
                    .HasForeignKey(d => d.id_padre)
                    .HasConstraintName("fk_ho_contopiet_04");
            });

            modelBuilder.Entity<ho_gruppoordinazione>(entity =>
            {
                entity.ToTable("ho_gruppoordinazione", Schema);

                entity.Property(e => e.ho_gruppoordinazioneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_gruppoordinazione'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(100);

                entity.Property(e => e.colore).HasMaxLength(50);

                entity.Property(e => e.descrizione).HasMaxLength(100);
            });

            modelBuilder.Entity<ho_log>(entity =>
            {
                entity.ToTable("ho_log", Schema);

                entity.HasIndex(e => e.id_contopiet)
                    .HasName("idx_ho_log_01");

                entity.HasIndex(e => e.id_prenotazione)
                    .HasName("idx_ho_log_02");

                entity.HasIndex(e => e.id_tavoloanagrafica)
                    .HasName("idx_ho_log_03");

                entity.HasIndex(e => e.id_tavoloanagrafica_out)
                    .HasName("idx_ho_log_04");

                entity.Property(e => e.ho_logid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_log'::regclass)");

                entity.Property(e => e.azione).HasMaxLength(100);

                entity.Property(e => e.extensiondata).HasColumnType("xml");

                entity.Property(e => e.id_groupconto).HasMaxLength(50);

                entity.Property(e => e.nomepostazione).HasMaxLength(500);

                entity.Property(e => e.stampante).HasMaxLength(200);

                entity.HasOne(d => d.id_contopietNavigation)
                    .WithMany(p => p.ho_log)
                    .HasForeignKey(d => d.id_contopiet)
                    .HasConstraintName("fk_ho_log_ho_01");

                entity.HasOne(d => d.id_prenotazioneNavigation)
                    .WithMany(p => p.ho_log)
                    .HasForeignKey(d => d.id_prenotazione)
                    .HasConstraintName("fk_ho_log_ho_02");

                entity.HasOne(d => d.id_tavoloanagraficaNavigation)
                    .WithMany(p => p.ho_logid_tavoloanagraficaNavigation)
                    .HasForeignKey(d => d.id_tavoloanagrafica)
                    .HasConstraintName("fk_ho_log_ho_03");

                entity.HasOne(d => d.id_tavoloanagrafica_outNavigation)
                    .WithMany(p => p.ho_logid_tavoloanagrafica_outNavigation)
                    .HasForeignKey(d => d.id_tavoloanagrafica_out)
                    .HasConstraintName("fk_ho_log_ho_04");
            });

            modelBuilder.Entity<ho_mapaggiunta>(entity =>
            {
                entity.ToTable("ho_mapaggiunta", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ho_mapaggiunta_01");

                entity.HasIndex(e => e.ana_cateid)
                    .HasName("idx_ho_mapaggiunta_02");

                entity.HasIndex(e => e.ana_subcateid)
                    .HasName("idx_ho_mapaggiunta_03");

                entity.HasIndex(e => e.pietanzaid)
                    .HasName("idx_ho_mapaggiunta_04");

                entity.Property(e => e.ho_mapaggiuntaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_mapaggiunta'::regclass)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ho_mapaggiuntaana_arti)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ho_mapaggiunta_02");

                entity.HasOne(d => d.ana_cate)
                    .WithMany(p => p.ho_mapaggiunta)
                    .HasForeignKey(d => d.ana_cateid)
                    .HasConstraintName("fk_ho_mapaggiunta_01");

                entity.HasOne(d => d.ana_subcate)
                    .WithMany(p => p.ho_mapaggiunta)
                    .HasForeignKey(d => d.ana_subcateid)
                    .HasConstraintName("fk_ho_mapaggiunta_03");

                entity.HasOne(d => d.pietanza)
                    .WithMany(p => p.ho_mapaggiuntapietanza)
                    .HasForeignKey(d => d.pietanzaid)
                    .HasConstraintName("fk_ho_mapaggiunta_04");
            });

            modelBuilder.Entity<ho_mapmenuprod>(entity =>
            {
                entity.ToTable("ho_mapmenuprod", Schema);

                entity.HasIndex(e => e.id_anaarti)
                    .HasName("idx_ho_mapmenuprod_01");

                entity.HasIndex(e => e.id_anacate)
                    .HasName("idx_ho_mapmenuprod_02");

                entity.HasIndex(e => e.id_anasubcate)
                    .HasName("idx_ho_mapmenuprod_03");

                entity.Property(e => e.ho_mapmenuprodid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_mapmenuprod'::regclass)");

                entity.Property(e => e.cod_group).HasMaxLength(50);

                entity.Property(e => e.prezzoaggxmenu).HasColumnType("numeric(9,2)");

                entity.HasOne(d => d.id_anaartiNavigation)
                    .WithMany(p => p.ho_mapmenuprod)
                    .HasForeignKey(d => d.id_anaarti)
                    .HasConstraintName("fk_ho_mapmenuprod_01");

                entity.HasOne(d => d.id_anacateNavigation)
                    .WithMany(p => p.ho_mapmenuprod)
                    .HasForeignKey(d => d.id_anacate)
                    .HasConstraintName("fk_ho_mapmenuprod_02");

                entity.HasOne(d => d.id_anasubcateNavigation)
                    .WithMany(p => p.ho_mapmenuprod)
                    .HasForeignKey(d => d.id_anasubcate)
                    .HasConstraintName("fk_ho_mapmenuprod_03");
            });

            modelBuilder.Entity<ho_notesala>(entity =>
            {
                entity.ToTable("ho_notesala", Schema);

                entity.HasIndex(e => e.ho_salaanagraficaid)
                    .HasName("idx_ho_notesala_01");

                entity.Property(e => e.ho_notesalaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_notesala'::regclass)");

                entity.HasOne(d => d.ho_salaanagrafica)
                    .WithMany(p => p.ho_notesala)
                    .HasForeignKey(d => d.ho_salaanagraficaid)
                    .HasConstraintName("fk_ho_notesala_01");
            });

            modelBuilder.Entity<ho_prenotazioni>(entity =>
            {
                entity.ToTable("ho_prenotazioni", Schema);

                entity.HasIndex(e => e.id_age_nomi)
                    .HasName("idx_ho_prenotazioni_01");

                entity.Property(e => e.ho_prenotazioniid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_prenotazioni'::regclass)");

                entity.Property(e => e.nome).HasMaxLength(100);

                entity.HasOne(d => d.id_age_nomiNavigation)
                    .WithMany(p => p.ho_prenotazioni)
                    .HasForeignKey(d => d.id_age_nomi)
                    .HasConstraintName("fk_ho_prenotazioni_01");
            });

            modelBuilder.Entity<ho_salaanagrafica>(entity =>
            {
                entity.ToTable("ho_salaanagrafica", Schema);

                entity.Property(e => e.ho_salaanagraficaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_salaanagrafica'::regclass)");

                entity.Property(e => e.codsala).HasMaxLength(250);

                entity.Property(e => e.larghezza_metri).HasColumnType("numeric(9,2)");

                entity.Property(e => e.lunghezza_metri).HasColumnType("numeric(9,2)");

                entity.Property(e => e.salnomesala)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<ho_tavoloanagrafica>(entity =>
            {
                entity.ToTable("ho_tavoloanagrafica", Schema);

                entity.HasIndex(e => e.ho_salaanagraficaid)
                    .HasName("idx_ho_tavoloanagrafica_01");

                entity.HasIndex(e => e.ho_tipotavoloid)
                    .HasName("idx_ho_tavoloanagrafica_02");

                entity.Property(e => e.ho_tavoloanagraficaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_tavoloanagrafica'::regclass)");

                entity.Property(e => e.idgroupcontoaperto).HasMaxLength(50);

                entity.Property(e => e.nome).HasMaxLength(100);

                entity.Property(e => e.tavstato).HasMaxLength(50);

                entity.HasOne(d => d.ho_salaanagrafica)
                    .WithMany(p => p.ho_tavoloanagrafica)
                    .HasForeignKey(d => d.ho_salaanagraficaid)
                    .HasConstraintName("fk_ho_tavolianagrafica_01");

                entity.HasOne(d => d.ho_tipotavolo)
                    .WithMany(p => p.ho_tavoloanagrafica)
                    .HasForeignKey(d => d.ho_tipotavoloid)
                    .HasConstraintName("fk_ho_tavolianagrafica_02");
            });

            modelBuilder.Entity<ho_tavoloprenotazione>(entity =>
            {
                entity.ToTable("ho_tavoloprenotazione", Schema);

                entity.HasIndex(e => e.id_map_sala_tavolo)
                    .HasName("idx_ho_tavoloprenotazione_01");

                entity.HasIndex(e => e.id_prenotazione)
                    .HasName("idx_ho_tavoloprenotazione_02");

                entity.Property(e => e.ho_tavoloprenotazioneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_tavoloprenotazione'::regclass)");

                entity.HasOne(d => d.id_map_sala_tavoloNavigation)
                    .WithMany(p => p.ho_tavoloprenotazione)
                    .HasForeignKey(d => d.id_map_sala_tavolo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ho_tavoloprenotazione_02");

                entity.HasOne(d => d.id_prenotazioneNavigation)
                    .WithMany(p => p.ho_tavoloprenotazione)
                    .HasForeignKey(d => d.id_prenotazione)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ho_tavoloprenotazione_01");
            });

            modelBuilder.Entity<ho_tipotavolo>(entity =>
            {
                entity.ToTable("ho_tipotavolo", Schema);

                entity.Property(e => e.ho_tipotavoloid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_tipotavolo'::regclass)");

                entity.Property(e => e.larghezza_metri).HasColumnType("numeric(9,2)");

                entity.Property(e => e.lunghezza_metri).HasColumnType("numeric(9,2)");

                entity.Property(e => e.tipnometavolo).HasMaxLength(50);
            });

            modelBuilder.Entity<ho_varianteconto>(entity =>
            {
                entity.ToTable("ho_varianteconto", Schema);

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ho_varianteconto_01");

                entity.HasIndex(e => e.distinta_baseid)
                    .HasName("idx_ho_varianteconto_02");

                entity.HasIndex(e => e.ho_contopietid)
                    .HasName("idx_ho_varianteconto_03");

                entity.Property(e => e.ho_variantecontoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ho_varianteconto'::regclass)");

                entity.Property(e => e.prezzo).HasColumnType("numeric(9,5)");

                entity.Property(e => e.qta).HasColumnType("numeric(9,5)");

                entity.Property(e => e.subtotale).HasColumnType("numeric(9,5)");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ho_varianteconto)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ho_variante_01");

                entity.HasOne(d => d.distinta_base)
                    .WithMany(p => p.ho_varianteconto)
                    .HasForeignKey(d => d.distinta_baseid)
                    .HasConstraintName("fk_ho_variante_02");

                entity.HasOne(d => d.ho_contopiet)
                    .WithMany(p => p.ho_varianteconto)
                    .HasForeignKey(d => d.ho_contopietid)
                    .HasConstraintName("fk_ho_variante_03");
            });

            modelBuilder.Entity<incasso_ddt_gene>(entity =>
            {
                entity.ToTable("incasso_ddt_gene", Schema);

                entity.Property(e => e.incasso_ddt_geneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_incasso_ddt_gene'::regclass)");
            });

            modelBuilder.Entity<livello>(entity =>
            {
                entity.ToTable("livello", Schema);

                entity.Property(e => e.livelloid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_livello'::regclass)");

                entity.Property(e => e.anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.espressione).HasMaxLength(100);
            });

            modelBuilder.Entity<mov_cassa>(entity =>
            {
                entity.ToTable("mov_cassa", Schema);

                entity.Property(e => e.mov_cassaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_mov_cassa'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(18,2)");

                entity.Property(e => e.tipo).HasMaxLength(50);
            });

            modelBuilder.Entity<mov_gene>(entity =>
            {
                entity.ToTable("mov_gene", Schema);

                entity.HasIndex(e => e.age_bankid)
                    .HasName("idx_mov_gene_01");

                entity.HasIndex(e => e.age_cdcid)
                    .HasName("idx_mov_gene_02");

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_mov_gene_03");

                entity.HasIndex(e => e.age_nomiidrap_1)
                    .HasName("idx_mov_gene_04");

                entity.HasIndex(e => e.age_nomiidrap_2)
                    .HasName("idx_mov_gene_05");

                entity.HasIndex(e => e.age_pagaid)
                    .HasName("idx_mov_gene_06");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_mov_gene_07");

                entity.HasIndex(e => e.age_valuid)
                    .HasName("idx_mov_gene_08");

                entity.HasIndex(e => e.top_causid)
                    .HasName("idx_mov_gene_09");

                entity.HasIndex(e => e.top_listid)
                    .HasName("idx_mov_gene_10");

                entity.HasIndex(e => e.top_maga_posizioneid)
                    .HasName("idx_mov_gene_11");

                entity.HasIndex(e => e.top_maga_scaffaleid)
                    .HasName("idx_mov_gene_12");

                entity.HasIndex(e => e.top_maga_settoreid)
                    .HasName("idx_mov_gene_13");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_mov_gene_14");

                entity.Property(e => e.mov_geneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_mov_gene'::regclass)");

                entity.Property(e => e.chg_valu).HasColumnType("numeric(20,10)");

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_tari).HasMaxLength(3);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_avvi).HasMaxLength(10);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(50);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_quad).HasMaxLength(20);

                entity.Property(e => e.des_stat)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.doc_resa).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.idcassa).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_sede).HasMaxLength(10);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_movi).HasMaxLength(13);

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_epal).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_vari).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.age_bank)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.age_bankid)
                    .HasConstraintName("fk_mov_gene_01");

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_mov_gene_02");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.mov_geneage_nomi)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_mov_gene_03");

                entity.HasOne(d => d.age_nomiidrap_1Navigation)
                    .WithMany(p => p.mov_geneage_nomiidrap_1Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_1)
                    .HasConstraintName("fk_mov_gene_04");

                entity.HasOne(d => d.age_nomiidrap_2Navigation)
                    .WithMany(p => p.mov_geneage_nomiidrap_2Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_2)
                    .HasConstraintName("fk_mov_gene_05");

                entity.HasOne(d => d.age_paga)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.age_pagaid)
                    .HasConstraintName("fk_mov_gene_06");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_mov_gene_07");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_mov_gene_08");

                entity.HasOne(d => d.top_caus)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.top_causid)
                    .HasConstraintName("fk_mov_gene_09");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_mov_gene_10");

                entity.HasOne(d => d.top_maga_posizione)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.top_maga_posizioneid)
                    .HasConstraintName("fk_mov_gene_12");

                entity.HasOne(d => d.top_maga_scaffale)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.top_maga_scaffaleid)
                    .HasConstraintName("fk_mov_gene_13");

                entity.HasOne(d => d.top_maga_settore)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.top_maga_settoreid)
                    .HasConstraintName("fk_mov_gene_14");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.mov_gene)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_mov_gene_11");
            });

            modelBuilder.Entity<move_gene_taglie>(entity =>
            {
                entity.ToTable("move_gene_taglie", Schema);

                entity.HasIndex(e => e.doc_artiid)
                    .HasName("idx_move_gene_taglie_01");

                entity.Property(e => e.id).HasDefaultValueSql($"nextval('{Schema}.seq_pk_move_gene_taglie'::regclass)");

                entity.Property(e => e.quantita).HasMaxLength(50);

                entity.Property(e => e.taglia).HasMaxLength(50);

                entity.HasOne(d => d.doc_arti)
                    .WithMany(p => p.move_gene_taglie)
                    .HasForeignKey(d => d.doc_artiid)
                    .HasConstraintName("fk_move_gene_taglie_01");
            });

            modelBuilder.Entity<movimento>(entity =>
            {
                entity.ToTable("movimento", Schema);

                entity.HasIndex(e => e.causaleid)
                    .HasName("idx_movimento_01");

                entity.HasIndex(e => e.pianodeicontiid)
                    .HasName("idx_movimento_02");

                entity.HasIndex(e => e.registroivaid)
                    .HasName("idx_movimento_03");

                entity.Property(e => e.movimentoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_movimento'::regclass)");

                entity.Property(e => e.aliquotaritenuta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.descrizione).HasMaxLength(150);

                entity.Property(e => e.imponibile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importoritenuta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.imposta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.impostadetraibile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.impostaindetraibile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.numero).HasMaxLength(13);

                entity.Property(e => e.numerodocumento).HasMaxLength(50);

                entity.Property(e => e.protocollo).HasMaxLength(50);

                entity.Property(e => e.totaledocumento).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.causale)
                    .WithMany(p => p.movimento)
                    .HasForeignKey(d => d.causaleid)
                    .HasConstraintName("fk_movimento_01");

                entity.HasOne(d => d.pianodeiconti)
                    .WithMany(p => p.movimento)
                    .HasForeignKey(d => d.pianodeicontiid)
                    .HasConstraintName("fk_movimento_02");

                entity.HasOne(d => d.registroiva)
                    .WithMany(p => p.movimento)
                    .HasForeignKey(d => d.registroivaid)
                    .HasConstraintName("fk_movimento_03");
            });

            modelBuilder.Entity<movimento_contropartita>(entity =>
            {
                entity.ToTable("movimento_contropartita", Schema);

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_movimento_contropartita_01");

                entity.HasIndex(e => e.movimentoid)
                    .HasName("idx_movimento_contropartita_02");

                entity.HasIndex(e => e.pianodeicontiid)
                    .HasName("idx_movimento_contropartita_03");

                entity.Property(e => e.movimento_contropartitaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_movimento_contropartita'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,2)");

                entity.Property(e => e.note).HasMaxLength(300);

                entity.HasOne(d => d.movimento)
                    .WithMany(p => p.movimento_contropartita)
                    .HasForeignKey(d => d.movimentoid)
                    .HasConstraintName("fk_movimento_contropartita_01");

                entity.HasOne(d => d.pianodeiconti)
                    .WithMany(p => p.movimento_contropartita)
                    .HasForeignKey(d => d.pianodeicontiid)
                    .HasConstraintName("fk_movimento_contropartita_02");
            });

            modelBuilder.Entity<movimento_daticassaprevidenziale>(entity =>
            {
                entity.ToTable("movimento_daticassaprevidenziale", Schema);

                entity.Property(e => e.movimento_daticassaprevidenzialeid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_movimento_daticassaprevidenziale'::regclass)");

                entity.Property(e => e.alcassa).HasColumnType("numeric(20,5)");

                entity.Property(e => e.aliquotaiva).HasColumnType("numeric(20,5)");

                entity.Property(e => e.imponibilecassa).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importocontributocassa).HasColumnType("numeric(20,5)");

                entity.Property(e => e.natura).HasMaxLength(2);

                entity.Property(e => e.riferimentoamministrazione).HasMaxLength(20);

                entity.Property(e => e.ritenuta).HasMaxLength(2);

                entity.Property(e => e.tipocassa).HasMaxLength(4);
            });

            modelBuilder.Entity<movimento_iva>(entity =>
            {
                entity.ToTable("movimento_iva", Schema);

                entity.HasIndex(e => e.movimentoid)
                    .HasName("idx_movimento_iva_01");

                entity.HasIndex(e => e.pianodeicontiid)
                    .HasName("idx_movimento_iva_02");

                entity.Property(e => e.movimento_ivaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_movimento_iva'::regclass)");

                entity.Property(e => e.imponibile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.imposta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.impostadetraibile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.impostaindetraibile).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.movimento)
                    .WithMany(p => p.movimento_iva)
                    .HasForeignKey(d => d.movimentoid)
                    .HasConstraintName("fk_movimento_iva_01");

                entity.HasOne(d => d.pianodeiconti)
                    .WithMany(p => p.movimento_iva)
                    .HasForeignKey(d => d.pianodeicontiid)
                    .HasConstraintName("fk_movimento_iva_02");
            });

            modelBuilder.Entity<ord_arti>(entity =>
            {
                entity.ToTable("ord_arti", Schema);

                entity.HasIndex(e => e.age_cdcid)
                    .HasName("idx_ord_arti_01");

                entity.HasIndex(e => e.age_tivaid)
                    .HasName("idx_ord_arti_02");

                entity.HasIndex(e => e.ana_artiid)
                    .HasName("idx_ord_arti_03");

                entity.HasIndex(e => e.ana_loscid)
                    .HasName("idx_ord_arti_04");

                entity.HasIndex(e => e.ord_arti_padreid)
                    .HasName("idx_ord_arti_05");

                entity.HasIndex(e => e.ord_geneid)
                    .HasName("idx_ord_arti_06");

                entity.HasIndex(e => e.top_magaid)
                    .HasName("idx_ord_arti_07");

                entity.Property(e => e.ord_artiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ord_arti'::regclass)");

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_rife).HasMaxLength(20);

                entity.Property(e => e.cod_subt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_arti).HasMaxLength(36);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_rife).HasMaxLength(13);

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_distinta).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_logi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.ord_arti)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_ord_arti_01");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.ord_arti)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_ord_arti_02");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.ord_arti)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_ord_arti_03");

                entity.HasOne(d => d.ana_losc)
                    .WithMany(p => p.ord_arti)
                    .HasForeignKey(d => d.ana_loscid)
                    .HasConstraintName("fk_ord_arti_04");

                entity.HasOne(d => d.ord_arti_padre)
                    .WithMany(p => p.Inverseord_arti_padre)
                    .HasForeignKey(d => d.ord_arti_padreid)
                    .HasConstraintName("fk_ord_arti_05");

                entity.HasOne(d => d.ord_gene)
                    .WithMany(p => p.ord_arti)
                    .HasForeignKey(d => d.ord_geneid)
                    .HasConstraintName("fk_ord_arti_06");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.ord_arti)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_ord_arti_07");
            });

            modelBuilder.Entity<ord_gene>(entity =>
            {
                entity.ToTable("ord_gene", Schema);

                entity.HasIndex(e => e.age_bankid)
                    .HasName("idx_ord_gene_01");

                entity.HasIndex(e => e.age_cdcid)
                    .HasName("idx_ord_gene_02");

                entity.HasIndex(e => e.age_nomiid)
                    .HasName("idx_ord_gene_03");

                entity.HasIndex(e => e.age_pagaid)
                    .HasName("idx_ord_gene_04");

                entity.Property(e => e.ord_geneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_ord_gene'::regclass)");

                entity.Property(e => e.chg_valu).HasColumnType("numeric(20,10)");

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_tari).HasMaxLength(3);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_avvi).HasMaxLength(10);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_quad).HasMaxLength(20);

                entity.Property(e => e.doc_resa).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.doc_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_sede).HasMaxLength(10);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_10).HasMaxLength(250);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.log_5).HasMaxLength(250);

                entity.Property(e => e.log_6).HasMaxLength(250);

                entity.Property(e => e.log_7).HasMaxLength(250);

                entity.Property(e => e.log_8).HasMaxLength(250);

                entity.Property(e => e.log_9).HasMaxLength(250);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_ordi).HasMaxLength(13);

                entity.Property(e => e.pre_cons).HasMaxLength(250);

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_epal).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_vari).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.age_bank)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.age_bankid)
                    .HasConstraintName("fk_ord_gene_01");

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_ord_gene_02");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_ord_gene_03");

                entity.HasOne(d => d.age_paga)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.age_pagaid)
                    .HasConstraintName("fk_ord_gene_04");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_ord_gene_05");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_ord_gene_06");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_ord_gene_07");

                entity.HasOne(d => d.ana_list)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.ana_listid)
                    .HasConstraintName("fk_ord_gene_08");

                entity.HasOne(d => d.top_caus)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.top_causid)
                    .HasConstraintName("fk_ord_gene_09");

                entity.HasOne(d => d.top_maga_posizione)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.top_maga_posizioneid)
                    .HasConstraintName("fk_ord_gene_11");

                entity.HasOne(d => d.top_maga_scaffale)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.top_maga_scaffaleid)
                    .HasConstraintName("fk_ord_gene_12");

                entity.HasOne(d => d.top_maga_settore)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.top_maga_settoreid)
                    .HasConstraintName("fk_ord_gene_13");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.ord_gene)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_ord_gene_10");
            });

            modelBuilder.Entity<oxnotifica>(entity =>
            {
                entity.ToTable("oxnotifica", Schema);

                entity.Property(e => e.oxnotificaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_oxnotifica'::regclass)");

                entity.Property(e => e.filename).HasColumnType("character varying");

                entity.Property(e => e.message).HasColumnType("character varying");

                entity.Property(e => e.title).HasMaxLength(50);

                entity.Property(e => e.users).HasColumnType("xml");

                entity.Property(e => e.users_descr).HasColumnType("character varying");
            });

            modelBuilder.Entity<oxproposte_incasso>(entity =>
            {
                entity.ToTable("oxproposte_incasso", Schema);

                entity.Property(e => e.oxproposte_incassoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_oxproposte_incasso'::regclass)");

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.note).HasColumnType("character varying");

                entity.Property(e => e.tip_pagacodice).HasMaxLength(50);
            });

            modelBuilder.Entity<oxproposte_incasso_doc>(entity =>
            {
                entity.ToTable("oxproposte_incasso_doc", Schema);

                entity.Property(e => e.oxproposte_incasso_docid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_oxproposte_incasso_doc'::regclass)");

                entity.Property(e => e.importo_coperto).HasColumnType("numeric(20,5)");

                entity.Property(e => e.num_fatt).HasMaxLength(50);
            });

            modelBuilder.Entity<park_sco>(entity =>
            {
                entity.ToTable("park_sco", Schema);

                entity.Property(e => e.park_scoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_park_sco'::regclass)");

                entity.Property(e => e.nome).HasMaxLength(150);
            });

            modelBuilder.Entity<pianodeiconti>(entity =>
            {
                entity.ToTable("pianodeiconti", Schema);

                entity.Property(e => e.pianodeicontiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_pianodeiconti'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(50);

                entity.Property(e => e.descrizione).HasMaxLength(250);

                entity.Property(e => e.sezione).HasMaxLength(50);

                entity.Property(e => e.tipologia).HasMaxLength(50);
            });

            modelBuilder.Entity<pre_arti>(entity =>
            {
                entity.ToTable("pre_arti", Schema);

                entity.Property(e => e.pre_artiid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_pre_arti'::regclass)");

                entity.Property(e => e.art_cons).HasColumnType("numeric(3,0)");

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_rife).HasMaxLength(20);

                entity.Property(e => e.cod_subt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_arti).HasMaxLength(36);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_rife).HasMaxLength(13);

                entity.Property(e => e.pre_cost).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pro_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pro_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.prv_cost).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_coll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_conf).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_distinta).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_logi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_misu_vendita).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.pre_arti)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_pre_arti_01");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.pre_arti)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_pre_arti_02");

                entity.HasOne(d => d.ana_arti)
                    .WithMany(p => p.pre_arti)
                    .HasForeignKey(d => d.ana_artiid)
                    .HasConstraintName("fk_pre_arti_03");

                entity.HasOne(d => d.ana_losc)
                    .WithMany(p => p.pre_arti)
                    .HasForeignKey(d => d.ana_loscid)
                    .HasConstraintName("fk_pre_arti_04");

                entity.HasOne(d => d.ana_misuidvenditaNavigation)
                    .WithMany(p => p.pre_arti)
                    .HasForeignKey(d => d.ana_misuidvendita)
                    .HasConstraintName("fk_pre_arti_05");

                entity.HasOne(d => d.pre_arti_padre)
                    .WithMany(p => p.Inversepre_arti_padre)
                    .HasForeignKey(d => d.pre_arti_padreid)
                    .HasConstraintName("fk_pre_arti_06");

                entity.HasOne(d => d.pre_gene)
                    .WithMany(p => p.pre_arti)
                    .HasForeignKey(d => d.pre_geneid)
                    .HasConstraintName("fk_pre_arti_07");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.pre_arti)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_pre_arti_08");
            });

            modelBuilder.Entity<pre_gene>(entity =>
            {
                entity.ToTable("pre_gene", Schema);

                entity.Property(e => e.pre_geneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_pre_gene'::regclass)");

                entity.Property(e => e.cap).HasMaxLength(250);

                entity.Property(e => e.chg_valu).HasColumnType("numeric(20,10)");

                entity.Property(e => e.civico).HasMaxLength(250);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.cod_tari).HasMaxLength(3);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cov_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.data_consegna).HasColumnType("date");

                entity.Property(e => e.des_avvi).HasMaxLength(10);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_quad).HasMaxLength(20);

                entity.Property(e => e.doc_resa).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.doc_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.indirizzo).HasMaxLength(250);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.key_sede).HasMaxLength(10);

                entity.Property(e => e.localita).HasMaxLength(250);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_10).HasColumnType("character varying");

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.log_5).HasMaxLength(250);

                entity.Property(e => e.log_6).HasMaxLength(250);

                entity.Property(e => e.log_7).HasMaxLength(250);

                entity.Property(e => e.log_8).HasMaxLength(250);

                entity.Property(e => e.log_9).HasMaxLength(250);

                entity.Property(e => e.note_stato).HasMaxLength(50);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_prev).HasMaxLength(13);

                entity.Property(e => e.num_rich).HasMaxLength(13);

                entity.Property(e => e.provincia).HasMaxLength(250);

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.rif_ordi).HasMaxLength(250);

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_epal).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tov_vari).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.age_bank)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.age_bankid)
                    .HasConstraintName("fk_pre_gene_01");

                entity.HasOne(d => d.age_cdc)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.age_cdcid)
                    .HasConstraintName("fk_pre_gene_02");

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.pre_geneage_nomi)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_pre_gene_03");

                entity.HasOne(d => d.age_nomiidrap_1Navigation)
                    .WithMany(p => p.pre_geneage_nomiidrap_1Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_1)
                    .HasConstraintName("fk_pre_gene_04");

                entity.HasOne(d => d.age_nomiidrap_2Navigation)
                    .WithMany(p => p.pre_geneage_nomiidrap_2Navigation)
                    .HasForeignKey(d => d.age_nomiidrap_2)
                    .HasConstraintName("fk_pre_gene_05");

                entity.HasOne(d => d.age_paga)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.age_pagaid)
                    .HasConstraintName("fk_pre_gene_06");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_pre_gene_07");

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_pre_gene_08");

                entity.HasOne(d => d.age_valu)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.age_valuid)
                    .HasConstraintName("fk_pre_gene_09");

                entity.HasOne(d => d.top_caus)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.top_causid)
                    .HasConstraintName("fk_pre_gene_10");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_pre_gene_11");

                entity.HasOne(d => d.top_maga_posizione)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.top_maga_posizioneid)
                    .HasConstraintName("fk_pre_gene_13");

                entity.HasOne(d => d.top_maga_scaffale)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.top_maga_scaffaleid)
                    .HasConstraintName("fk_pre_gene_14");

                entity.HasOne(d => d.top_maga_settore)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.top_maga_settoreid)
                    .HasConstraintName("fk_pre_gene_15");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.pre_gene)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_pre_gene_12");
            });

            modelBuilder.Entity<pre_ord>(entity =>
            {
                entity.ToTable("pre_ord", Schema);

                entity.Property(e => e.pre_ordid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_pre_ord'::regclass)");
            });

            modelBuilder.Entity<prov_list>(entity =>
            {
                entity.ToTable("prov_list", Schema);

                entity.Property(e => e.prov_listid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_prov_list'::regclass)");

                entity.Property(e => e.nome).HasMaxLength(250);

                entity.Property(e => e.scontoperc).HasColumnType("numeric(18,2)");
            });

            modelBuilder.Entity<prov_list_map>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("prov_list_map", Schema);
            });

            modelBuilder.Entity<registroiva>(entity =>
            {
                entity.ToTable("registroiva", Schema);

                entity.Property(e => e.registroivaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_registroiva'::regclass)");

                entity.Property(e => e.anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.codice).HasMaxLength(50);

                entity.Property(e => e.descrizione).HasMaxLength(150);

                entity.Property(e => e.stampa_ultimapagina).HasColumnType("numeric(10,0)");

                entity.Property(e => e.stampa_ultimomese).HasColumnType("numeric(2,0)");
            });

            modelBuilder.Entity<registroiva_tipomovimento>(entity =>
            {
                entity.ToTable("registroiva_tipomovimento", Schema);

                entity.HasIndex(e => e.registroivaid)
                    .HasName("idx_registroiva_tipomovimento_01");

                entity.HasIndex(e => e.tipomovimentoid)
                    .HasName("idx_registroiva_tipomovimento_02");

                entity.Property(e => e.registroiva_tipomovimentoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_registroiva_tipomovimento'::regclass)");

                entity.HasOne(d => d.registroiva)
                    .WithMany(p => p.registroiva_tipomovimento)
                    .HasForeignKey(d => d.registroivaid)
                    .HasConstraintName("fk_registroiva_tipomovimento_02");

                entity.HasOne(d => d.tipomovimento)
                    .WithMany(p => p.registroiva_tipomovimento)
                    .HasForeignKey(d => d.tipomovimentoid)
                    .HasConstraintName("fk_registroiva_tipomovimento_01");
            });

            modelBuilder.Entity<rpanacauz_darenderetotale>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rpanacauz_darenderetotale", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_da_rendere).HasColumnType("numeric(14,3)");
            });

            modelBuilder.Entity<rpcauzionidarendere_doc_arti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rpcauzionidarendere_doc_arti", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.pre_tota).HasColumnType("numeric");

                entity.Property(e => e.qta_tota).HasColumnType("numeric");
            });

            modelBuilder.Entity<rpcauzionidarendere_pre_arti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rpcauzionidarendere_pre_arti", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.pre_tota).HasColumnType("numeric");

                entity.Property(e => e.qta_tota).HasColumnType("numeric");
            });

            modelBuilder.Entity<rpddt_view_totali>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rpddt_view_totali", Schema);

                entity.Property(e => e.abi_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.anno_oggetto).HasMaxLength(250);

                entity.Property(e => e.cab_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.chilometri_oggetto).HasMaxLength(250);

                entity.Property(e => e.cod_fisc).HasColumnType("character varying");

                entity.Property(e => e.cod_tipo).HasMaxLength(20);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.controllo_documento).HasMaxLength(50);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_avvi).HasColumnType("character varying");

                entity.Property(e => e.des_bank).HasMaxLength(60);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_valu).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.modello_oggetto).HasMaxLength(250);

                entity.Property(e => e.num_boll).HasMaxLength(13);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.par_tiva).HasColumnType("character varying");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_1_tel_nume).HasColumnType("character varying");

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(500);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.spo_bank).HasMaxLength(60);

                entity.Property(e => e.targa_oggetto).HasMaxLength(250);

                entity.Property(e => e.tel_nume).HasColumnType("character varying");

                entity.Property(e => e.telaio_oggetto).HasMaxLength(250);

                entity.Property(e => e.telefonocliente).HasMaxLength(250);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.vettore).HasMaxLength(100);
            });

            modelBuilder.Entity<rpfat_view_totali>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rpfat_view_totali", Schema);

                entity.Property(e => e.abi_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cab_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_exte).HasMaxLength(250);

                entity.Property(e => e.cod_fisc).HasColumnType("character varying");

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_avvi).HasColumnType("character varying");

                entity.Property(e => e.des_bank).HasMaxLength(60);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_valu).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_fatt).HasMaxLength(13);

                entity.Property(e => e.pag_escl).HasMaxLength(20);

                entity.Property(e => e.pag_gfis).HasColumnType("numeric(3,0)");

                entity.Property(e => e.pag_iniz).HasColumnType("numeric(3,0)");

                entity.Property(e => e.pag_ivam).HasMaxLength(20);

                entity.Property(e => e.pag_mese).HasMaxLength(20);

                entity.Property(e => e.pag_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pag_rate).HasColumnType("numeric(3,0)");

                entity.Property(e => e.pag_scon).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pag_skip).HasColumnType("numeric(2,0)");

                entity.Property(e => e.pag_spem).HasMaxLength(20);

                entity.Property(e => e.pag_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pag_step).HasColumnType("numeric(3,0)");

                entity.Property(e => e.par_tiva).HasColumnType("character varying");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_1_tel_nume).HasColumnType("character varying");

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(500);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.spo_bank).HasMaxLength(60);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_movi).HasColumnType("numeric");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.vettore).HasMaxLength(100);
            });

            modelBuilder.Entity<rpmov_view_totali>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rpmov_view_totali", Schema);

                entity.Property(e => e.abi_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cab_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cod_fisc).HasColumnType("character varying");

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_avvi).HasColumnType("character varying");

                entity.Property(e => e.des_bank).HasMaxLength(60);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(50);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_valu).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_movi).HasMaxLength(13);

                entity.Property(e => e.par_tiva).HasColumnType("character varying");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(500);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.spo_bank).HasMaxLength(60);

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.vettore).HasMaxLength(100);
            });

            modelBuilder.Entity<rvw_agenti_fatturato>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_agenti_fatturato", Schema);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.num_fatt).HasMaxLength(13);

                entity.Property(e => e.pro_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pro_impo).HasColumnType("numeric");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<rvw_ana_arti_dett>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_ana_arti_dett", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.caricati).HasColumnType("numeric");

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.expr1).HasColumnType("character varying");

                entity.Property(e => e.giacenza).HasColumnType("numeric");

                entity.Property(e => e.inventario).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.marca).HasMaxLength(50);

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.scaricati).HasColumnType("numeric");

                entity.Property(e => e.val_magazzino).HasColumnType("numeric");

                entity.Property(e => e.val_pre_vend).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_articolisottoscorta_internal>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_articolisottoscorta_internal", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.gru_arti).HasMaxLength(250);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.magazzino)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.marca).HasMaxLength(50);

                entity.Property(e => e.oqta_tota).HasColumnType("numeric");

                entity.Property(e => e.pqta_tota).HasColumnType("numeric");

                entity.Property(e => e.qta_giac).HasColumnType("numeric");

                entity.Property(e => e.qta_rior).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_scor).HasColumnType("numeric(14,3)");
            });

            modelBuilder.Entity<rvw_catalogo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_catalogo", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.cod_list)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.gru_arti).HasMaxLength(250);

                entity.Property(e => e.mar_arti).HasMaxLength(50);

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<rvw_ddt_gene_collo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_ddt_gene_collo", Schema);

                entity.Property(e => e.dat_boll).HasColumnType("date");

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.num_boll).HasMaxLength(13);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");
            });

            modelBuilder.Entity<rvw_ddt_gene_dett>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_ddt_gene_dett", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.cod_matr).HasColumnType("character varying");

                entity.Property(e => e.cod_type).HasColumnType("character varying");

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.expr1).HasMaxLength(5);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<rvw_doc_pre0_page>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_doc_pre0_page", Schema);

                entity.Property(e => e.abi_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.bankavvi)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.bankcitt).HasMaxLength(50);

                entity.Property(e => e.bankcivi).HasMaxLength(10);

                entity.Property(e => e.bankindi).HasMaxLength(50);

                entity.Property(e => e.bankprov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.cab_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.causale).HasMaxLength(3);

                entity.Property(e => e.chg_valu).HasColumnType("numeric(20,10)");

                entity.Property(e => e.cod_fisc).HasColumnType("character varying");

                entity.Property(e => e.cod_list)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_paga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_avvi).HasMaxLength(10);

                entity.Property(e => e.des_bank).HasMaxLength(60);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_list).HasMaxLength(50);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_valu).HasMaxLength(50);

                entity.Property(e => e.doc_resa).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.doc_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_10).HasColumnType("character varying");

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.log_5).HasMaxLength(250);

                entity.Property(e => e.log_6).HasMaxLength(250);

                entity.Property(e => e.log_7).HasMaxLength(250);

                entity.Property(e => e.log_8).HasMaxLength(250);

                entity.Property(e => e.log_9).HasMaxLength(250);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_prev).HasMaxLength(13);

                entity.Property(e => e.par_tiva).HasColumnType("character varying");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(500);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_ordi).HasMaxLength(250);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_epal).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.spo_bank).HasMaxLength(60);

                entity.Property(e => e.tel_nume).HasColumnType("character varying");

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_cauz).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_omag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<rvw_docmov0_page>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_docmov0_page", Schema);

                entity.Property(e => e.abi_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cab_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cod_fisc).HasColumnType("character varying");

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.cos_acce).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_boll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_imba).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_inca).HasColumnType("numeric(20,5)");

                entity.Property(e => e.cos_tras).HasColumnType("numeric(20,5)");

                entity.Property(e => e.des_bank).HasMaxLength(60);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_maga).HasMaxLength(50);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.des_valu).HasMaxLength(50);

                entity.Property(e => e.doc_note).HasMaxLength(2000);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_movi).HasMaxLength(13);

                entity.Property(e => e.par_tiva).HasColumnType("character varying");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(500);

                entity.Property(e => e.rif_mail).HasColumnType("character varying");

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_coll).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_cuba).HasColumnType("numeric(10,3)");

                entity.Property(e => e.spe_epal).HasColumnType("numeric(10,0)");

                entity.Property(e => e.spe_peso).HasColumnType("numeric(10,2)");

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_time).HasMaxLength(5);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.spo_bank).HasMaxLength(60);

                entity.Property(e => e.tel_nfax).HasColumnType("character varying");

                entity.Property(e => e.tel_nume).HasColumnType("character varying");

                entity.Property(e => e.tot_abbu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_arti).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_merc).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_paga).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_prod).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_scon).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_serv).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_spes).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_vari).HasColumnType("numeric(20,5)");

                entity.Property(e => e.vettore).HasMaxLength(100);
            });

            modelBuilder.Entity<rvw_docorf0_detailreport>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_docorf0_detailreport", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.des_arti_eng).HasMaxLength(100);

                entity.Property(e => e.for_arti).HasMaxLength(50);

                entity.Property(e => e.iva_perc).HasColumnType("character varying");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_totabo).HasColumnType("numeric");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.subtotale).HasColumnType("numeric");

                entity.Property(e => e.subtotalebo).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_docorf0_page>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_docorf0_page", Schema);

                entity.Property(e => e.abi_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cab_code)
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_paga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_vett).HasMaxLength(10);

                entity.Property(e => e.con_bank).HasMaxLength(20);

                entity.Property(e => e.con_iban).HasMaxLength(34);

                entity.Property(e => e.des_avvi).HasMaxLength(10);

                entity.Property(e => e.des_bank).HasMaxLength(60);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_citt).HasMaxLength(50);

                entity.Property(e => e.des_indi).HasMaxLength(500);

                entity.Property(e => e.des_nciv).HasMaxLength(10);

                entity.Property(e => e.des_nome).HasMaxLength(100);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.des_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.des_valu).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.doc_tipo).HasMaxLength(20);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_10).HasMaxLength(250);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.log_5).HasMaxLength(250);

                entity.Property(e => e.log_6).HasMaxLength(250);

                entity.Property(e => e.log_7).HasMaxLength(250);

                entity.Property(e => e.log_8).HasMaxLength(250);

                entity.Property(e => e.log_9).HasMaxLength(250);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_ordi).HasMaxLength(13);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(500);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.sco_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_esen).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_osta).HasColumnType("numeric(20,5)");

                entity.Property(e => e.vettore).HasMaxLength(100);
            });

            modelBuilder.Entity<rvw_elencoarticoli_nomagazzini>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_elencoarticoli_nomagazzini", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.caricati).HasColumnType("numeric");

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.giacenza).HasColumnType("numeric");

                entity.Property(e => e.gru_arti).HasMaxLength(250);

                entity.Property(e => e.inventario).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.marca).HasMaxLength(50);

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.scaricati).HasColumnType("numeric");

                entity.Property(e => e.val_magazzino).HasColumnType("numeric");

                entity.Property(e => e.val_pre_vendita).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_fat_gene_dett>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_fat_gene_dett", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.cod_matr).HasColumnType("character varying");

                entity.Property(e => e.cod_type).HasColumnType("character varying");

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.num_boll).HasMaxLength(13);

                entity.Property(e => e.num_fatt).HasMaxLength(13);

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.subtotale).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_fidelity>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_fidelity", Schema);

                entity.Property(e => e.anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.articolo).HasMaxLength(100);

                entity.Property(e => e.cliente).HasMaxLength(100);

                entity.Property(e => e.codice).HasMaxLength(10);

                entity.Property(e => e.codice_carta).HasMaxLength(50);

                entity.Property(e => e.ivato).HasColumnType("numeric(20,5)");

                entity.Property(e => e.note_articolo).HasMaxLength(50);
            });

            modelBuilder.Entity<rvw_maga_mov>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_maga_mov", Schema);

                entity.Property(e => e.acconto).HasColumnType("numeric");

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_tipo).HasMaxLength(20);

                entity.Property(e => e.differenza).HasColumnType("numeric");

                entity.Property(e => e.imponibile).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_movi).HasMaxLength(13);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.totale).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_mov_gene_dett>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_mov_gene_dett", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ivato).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.subtotale).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_onemobile_articoli>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_onemobile_articoli", Schema);

                entity.Property(e => e.art_peso).HasColumnType("numeric(18,5)");

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.cod_list)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.for_arti).HasMaxLength(50);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.nome).HasMaxLength(50);

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_coll).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_conf).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_disp).HasColumnType("numeric");

                entity.Property(e => e.qta_entr).HasColumnType("numeric");

                entity.Property(e => e.qta_giac).HasColumnType("numeric");

                entity.Property(e => e.qta_impe).HasColumnType("numeric");

                entity.Property(e => e.qta_inve).HasColumnType("numeric");

                entity.Property(e => e.qta_maxi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_ordi).HasColumnType("numeric");

                entity.Property(e => e.qta_rior).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_scor).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_usci).HasColumnType("numeric");

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.rif_arti).HasMaxLength(50);

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");
            });

            modelBuilder.Entity<rvw_ord_arti_dett>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_ord_arti_dett", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.des_arti_eng).HasMaxLength(100);

                entity.Property(e => e.for_arti).HasMaxLength(50);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_totabo).HasColumnType("numeric");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.subtotale).HasColumnType("numeric");

                entity.Property(e => e.subtotalebo).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_pre_arti_dett>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_pre_arti_dett", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.art_misu_vendita).HasMaxLength(10);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_coll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota_vendita).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_totabo).HasColumnType("numeric");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.subtotale).HasColumnType("numeric");

                entity.Property(e => e.subtotalebo).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_repart0_detail>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_repart0_detail", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.des_list).HasMaxLength(50);

                entity.Property(e => e.expr1).HasColumnType("character varying");

                entity.Property(e => e.for_arti).HasMaxLength(50);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.mar_arti).HasMaxLength(50);

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_entr).HasColumnType("numeric");

                entity.Property(e => e.qta_giac).HasColumnType("numeric");

                entity.Property(e => e.qta_usci).HasColumnType("numeric");

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<rvw_repddt0_page>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_repddt0_page", Schema);

                entity.Property(e => e.cap).HasMaxLength(10);

                entity.Property(e => e.civico).HasMaxLength(10);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.codice_pin).HasMaxLength(50);

                entity.Property(e => e.controllo_documento).HasMaxLength(50);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_maga).HasMaxLength(50);

                entity.Property(e => e.descrizione).HasMaxLength(50);

                entity.Property(e => e.evaso).HasMaxLength(20);

                entity.Property(e => e.filiale).HasMaxLength(100);

                entity.Property(e => e.filiale_cap).HasMaxLength(10);

                entity.Property(e => e.filiale_civico).HasMaxLength(10);

                entity.Property(e => e.filiale_indirizzo).HasMaxLength(500);

                entity.Property(e => e.filiale_localita).HasMaxLength(50);

                entity.Property(e => e.filiale_provincia)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.filtro_4).HasMaxLength(50);

                entity.Property(e => e.filtro_5).HasMaxLength(50);

                entity.Property(e => e.indirizzo).HasMaxLength(500);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.localita).HasMaxLength(50);

                entity.Property(e => e.num_boll).HasMaxLength(13);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.ora).HasMaxLength(50);

                entity.Property(e => e.provincia).HasMaxLength(2);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.telefono).HasColumnType("character varying");

                entity.Property(e => e.tot_docu).HasColumnType("numeric");

                entity.Property(e => e.zona).HasMaxLength(50);
            });

            modelBuilder.Entity<rvw_repnom0_detail>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_repnom0_detail", Schema);

                entity.Property(e => e.cli_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.des_cate).HasMaxLength(50);

                entity.Property(e => e.des_pref).HasMaxLength(50);

                entity.Property(e => e.for_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_iweb).HasMaxLength(50);

                entity.Property(e => e.rif_mail).HasColumnType("character varying");

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.tel_nfax).HasColumnType("character varying");

                entity.Property(e => e.tel_nume).HasColumnType("character varying");

                entity.Property(e => e.vet_pref).HasColumnType("numeric(1,0)");
            });

            modelBuilder.Entity<rvw_stampaetichette>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_stampaetichette", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.expr1).HasColumnType("character varying");

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.mar_arti).HasMaxLength(50);

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.rif_arti).HasMaxLength(50);

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<rvw_valorizzazionemagazzino>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_valorizzazionemagazzino", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.caricati).HasColumnType("numeric");

                entity.Property(e => e.cmp).HasColumnType("numeric");

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.giacenza).HasColumnType("numeric");

                entity.Property(e => e.gru_arti).HasMaxLength(250);

                entity.Property(e => e.inventario).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.marca).HasMaxLength(50);

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.scaricati).HasColumnType("numeric");

                entity.Property(e => e.val_maga_giac).HasColumnType("numeric");

                entity.Property(e => e.val_magazzino).HasColumnType("numeric");

                entity.Property(e => e.val_pre_vend).HasColumnType("numeric");
            });

            modelBuilder.Entity<rvw_valorizzazionemagazzino_nomagazzini>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("rvw_valorizzazionemagazzino_nomagazzini", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.caricati).HasColumnType("numeric");

                entity.Property(e => e.cmp).HasColumnType("numeric");

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.giacenza).HasColumnType("numeric");

                entity.Property(e => e.gru_arti).HasMaxLength(250);

                entity.Property(e => e.inventario).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.marca).HasMaxLength(50);

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.scaricati).HasColumnType("numeric");

                entity.Property(e => e.val_magazzino).HasColumnType("numeric");

                entity.Property(e => e.val_pre_vend).HasColumnType("numeric");
            });

            modelBuilder.Entity<scadenzario>(entity =>
            {
                entity.ToTable("scadenzario", Schema);

                entity.HasIndex(e => e.movimento_contropartitaid)
                    .HasName("idx_scadenzario_01");

                entity.HasIndex(e => e.movimentoid)
                    .HasName("idx_scadenzario_02");

                entity.Property(e => e.scadenzarioid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_scadenzario'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.movimento_contropartita)
                    .WithMany(p => p.scadenzario)
                    .HasForeignKey(d => d.movimento_contropartitaid)
                    .HasConstraintName("fk_scadenzario_01");

                entity.HasOne(d => d.movimento)
                    .WithMany(p => p.scadenzario)
                    .HasForeignKey(d => d.movimentoid)
                    .HasConstraintName("fk_scadenzario_02");
            });

            modelBuilder.Entity<scadenzario_modifica>(entity =>
            {
                entity.ToTable("scadenzario_modifica", Schema);

                entity.HasIndex(e => e.scadenzario_ratamodificataid)
                    .HasName("idx_scadenzario_modifica_01");

                entity.HasIndex(e => e.scadenzarioid)
                    .HasName("idx_scadenzario_modifica_02");

                entity.Property(e => e.scadenzario_modificaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_scadenzario_modifica'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.HasOne(d => d.scadenzario_ratamodificata)
                    .WithMany(p => p.scadenzario_modifica)
                    .HasForeignKey(d => d.scadenzario_ratamodificataid)
                    .HasConstraintName("fk_scadenzario_modifica_02");

                entity.HasOne(d => d.scadenzario)
                    .WithMany(p => p.scadenzario_modifica)
                    .HasForeignKey(d => d.scadenzarioid)
                    .HasConstraintName("fk_scadenzario_modifica_01");
            });

            modelBuilder.Entity<scadenzario_rata>(entity =>
            {
                entity.ToTable("scadenzario_rata", Schema);

                entity.HasIndex(e => e.scadenzarioid)
                    .HasName("idx_scadenzario_rata_01");

                entity.Property(e => e.scadenzario_rataid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_scadenzario_rata'::regclass)");

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importopagato).HasColumnType("numeric(20,5)");

                entity.Property(e => e.numerodisposizione).HasMaxLength(50);

                entity.Property(e => e.numerodistinta).HasMaxLength(50);

                entity.HasOne(d => d.scadenzario)
                    .WithMany(p => p.scadenzario_rata)
                    .HasForeignKey(d => d.scadenzarioid)
                    .HasConstraintName("fk_scadenzario_rata_01");
            });

            modelBuilder.Entity<shop_vwdoc_arti_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("shop_vwdoc_arti_view", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_logi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.rif_arti).HasMaxLength(50);

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<shop_vwmov_gene_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("shop_vwmov_gene_view", Schema);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_movi).HasMaxLength(13);

                entity.Property(e => e.pre_tota).HasColumnType("numeric");
            });

            modelBuilder.Entity<speso_codice_carica>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("speso_codice_carica", Schema);

                entity.Property(e => e.codice_caricaid).HasMaxLength(2);

                entity.Property(e => e.descrizione_carica).HasMaxLength(500);
            });

            modelBuilder.Entity<speso_codstat>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("speso_codstat", Schema);

                entity.Property(e => e.nome_stato).HasMaxLength(255);
            });

            modelBuilder.Entity<speso_periodo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("speso_periodo", Schema);

                entity.Property(e => e.codice_periodo)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.periodo)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<sys_tablestatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("sys_tablestatus", Schema);

                entity.Property(e => e.lastchange).IsRequired();

                entity.Property(e => e.tablename)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<sys_version>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("sys_version", Schema);

                entity.Property(e => e.version).HasMaxLength(50);
            });

            modelBuilder.Entity<tbl_scontrino>(entity =>
            {
                entity.ToTable("tbl_scontrino", Schema);

                entity.Property(e => e.tbl_scontrinoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_tbl_scontrino'::regclass)");

                entity.Property(e => e.scontrino_inte).HasMaxLength(100);

                entity.Property(e => e.totale_contanti).HasColumnType("numeric(18,2)");

                entity.Property(e => e.totale_scontrino).HasColumnType("numeric(18,5)");

                entity.HasOne(d => d.tbl_scontrinoNavigation)
                    .WithOne(p => p.Inversetbl_scontrinoNavigation)
                    .HasForeignKey<tbl_scontrino>(d => d.tbl_scontrinoid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_tbl_scontrino_01");
            });

            modelBuilder.Entity<tip_paga>(entity =>
            {
                entity.ToTable("tip_paga", Schema);

                entity.Property(e => e.tip_pagaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_tip_paga'::regclass)");

                entity.Property(e => e.categoria).HasMaxLength(50);

                entity.Property(e => e.codice).HasMaxLength(50);

                entity.Property(e => e.cog_inc_contropartita).HasMaxLength(50);

                entity.Property(e => e.descrizione).HasMaxLength(200);
            });

            modelBuilder.Entity<tipomovimento>(entity =>
            {
                entity.ToTable("tipomovimento", Schema);

                entity.Property(e => e.tipomovimentoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_tipomovimento'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(3);

                entity.Property(e => e.descrizione).HasMaxLength(150);

                entity.Property(e => e.gestioneiva).HasComment("se è previsto l'impiego di iva");
            });

            modelBuilder.Entity<tocken>(entity =>
            {
                entity.ToTable("tocken", Schema);

                entity.HasIndex(e => e.name)
                    .HasName("idx_tocken_01")
                    .IsUnique();

                entity.Property(e => e.enabled)
                    .IsRequired()
                    .HasDefaultValueSql("true");

                entity.Property(e => e.name)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<top_caus>(entity =>
            {
                entity.ToTable("top_caus", Schema);

                entity.Property(e => e.top_causid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_caus'::regclass)");

                entity.Property(e => e.cau_cont).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cau_entr).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cau_impe).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cau_maga).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cau_ordi).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cau_usci).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cau_valo).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cli_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cnt_from).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cnt_info).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_docu).HasMaxLength(3);

                entity.Property(e => e.cod_nume)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_tari).HasMaxLength(3);

                entity.Property(e => e.cod_tipo).HasMaxLength(20);

                entity.Property(e => e.cog_cass).HasMaxLength(30);

                entity.Property(e => e.cog_cauz).HasMaxLength(30);

                entity.Property(e => e.cog_grup).HasMaxLength(3);

                entity.Property(e => e.cog_inc).HasMaxLength(3);

                entity.Property(e => e.cog_merc).HasMaxLength(30);

                entity.Property(e => e.cog_ncre).HasMaxLength(3);

                entity.Property(e => e.cog_nfat).HasMaxLength(30);

                entity.Property(e => e.cog_omag).HasMaxLength(30);

                entity.Property(e => e.cog_prod).HasMaxLength(30);

                entity.Property(e => e.cog_serv).HasMaxLength(30);

                entity.Property(e => e.cog_vari).HasMaxLength(30);

                entity.Property(e => e.cos_c).HasMaxLength(3);

                entity.Property(e => e.cos_p).HasMaxLength(3);

                entity.Property(e => e.ddt_from).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ddt_info).HasColumnType("numeric(1,0)");

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.for_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.int_from).HasColumnType("numeric(1,0)");

                entity.Property(e => e.int_info).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ord_from).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ord_info).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pre_from).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pre_info).HasColumnType("numeric(1,0)");

                entity.Property(e => e.rip_from).HasColumnType("numeric(1,0)");

                entity.Property(e => e.rip_info).HasColumnType("numeric(1,0)");

                entity.Property(e => e.vet_pref).HasColumnType("numeric(1,0)");

                entity.HasOne(d => d.top_causid_trasddtNavigation)
                    .WithMany(p => p.Inversetop_causid_trasddtNavigation)
                    .HasForeignKey(d => d.top_causid_trasddt)
                    .HasConstraintName("fk_top_caus_01");

                entity.HasOne(d => d.top_causid_trasfatNavigation)
                    .WithMany(p => p.Inversetop_causid_trasfatNavigation)
                    .HasForeignKey(d => d.top_causid_trasfat)
                    .HasConstraintName("fk_top_caus_02");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.top_caus)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_top_caus_03");
            });

            modelBuilder.Entity<top_gene>(entity =>
            {
                entity.ToTable("top_gene", Schema);

                entity.Property(e => e.top_geneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_gene'::regclass)");

                entity.Property(e => e.cod_buon).HasMaxLength(3);

                entity.Property(e => e.cod_vend).HasMaxLength(3);

                entity.Property(e => e.cog_abbu).HasMaxLength(13);

                entity.Property(e => e.cog_acce).HasMaxLength(13);

                entity.Property(e => e.cog_bank).HasMaxLength(13);

                entity.Property(e => e.cog_boll).HasMaxLength(13);

                entity.Property(e => e.cog_cass).HasMaxLength(13);

                entity.Property(e => e.cog_imba).HasMaxLength(13);

                entity.Property(e => e.cog_inca).HasMaxLength(13);

                entity.Property(e => e.cog_omag).HasMaxLength(13);

                entity.Property(e => e.cog_tras).HasMaxLength(13);

                entity.Property(e => e.ddt_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ddt_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ddt_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.dib_gest).HasColumnType("numeric(1,0)");

                entity.Property(e => e.fat_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.iva_acce).HasMaxLength(3);

                entity.Property(e => e.iva_boll).HasMaxLength(3);

                entity.Property(e => e.iva_imba).HasMaxLength(3);

                entity.Property(e => e.iva_inca).HasMaxLength(3);

                entity.Property(e => e.iva_omag).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.iva_tras).HasMaxLength(3);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.los_gest).HasColumnType("numeric(1,0)");

                entity.Property(e => e.mac_gest).HasColumnType("numeric(1,0)");

                entity.Property(e => e.mov_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ord_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ord_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.ord_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pre_spre).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pre_sprt).HasColumnType("numeric(1,0)");

                entity.Property(e => e.pre_stot).HasColumnType("numeric(1,0)");

                entity.Property(e => e.spe_aspe).HasMaxLength(20);

                entity.Property(e => e.spe_port).HasMaxLength(20);

                entity.Property(e => e.spe_tipo).HasMaxLength(20);

                entity.Property(e => e.val_a).HasMaxLength(3);

                entity.Property(e => e.val_b).HasMaxLength(3);

                entity.HasOne(d => d.age_tiva)
                    .WithMany(p => p.top_gene)
                    .HasForeignKey(d => d.age_tivaid)
                    .HasConstraintName("fk_top_gene_01");

                entity.HasOne(d => d.top_list)
                    .WithMany(p => p.top_gene)
                    .HasForeignKey(d => d.top_listid)
                    .HasConstraintName("fk_top_gene_02");

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.top_gene)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_top_gene_03");
            });

            modelBuilder.Entity<top_list>(entity =>
            {
                entity.ToTable("top_list", Schema);

                entity.Property(e => e.top_listid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_list'::regclass)");

                entity.Property(e => e.cod_arro).HasMaxLength(20);

                entity.Property(e => e.cod_list)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.des_list).HasMaxLength(50);
            });

            modelBuilder.Entity<top_maga>(entity =>
            {
                entity.ToTable("top_maga", Schema);

                entity.Property(e => e.top_magaid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_maga'::regclass)");

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_tari)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.des_maga).HasMaxLength(50);

                entity.Property(e => e.ldv_into).HasMaxLength(10);

                entity.Property(e => e.mag_fisc).HasColumnType("numeric(1,0)");

                entity.Property(e => e.mag_tipo).HasMaxLength(20);

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_indi).HasMaxLength(50);

                entity.Property(e => e.rif_nciv).HasMaxLength(10);

                entity.Property(e => e.rif_prov)
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.rif_quad).HasMaxLength(20);

                entity.HasOne(d => d.age_nomi)
                    .WithMany(p => p.top_maga)
                    .HasForeignKey(d => d.age_nomiid)
                    .HasConstraintName("fk_top_maga_01");

                entity.HasOne(d => d.age_stat)
                    .WithMany(p => p.top_maga)
                    .HasForeignKey(d => d.age_statid)
                    .HasConstraintName("fk_top_maga_02");
            });

            modelBuilder.Entity<top_maga_posizione>(entity =>
            {
                entity.ToTable("top_maga_posizione", Schema);

                entity.Property(e => e.top_maga_posizioneid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_maga_posizione'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(10);

                entity.Property(e => e.descrizione).HasMaxLength(150);

                entity.HasOne(d => d.top_maga_scaffale)
                    .WithMany(p => p.top_maga_posizione)
                    .HasForeignKey(d => d.top_maga_scaffaleid)
                    .HasConstraintName("fk_top_maga_posizione_01");
            });

            modelBuilder.Entity<top_maga_scaffale>(entity =>
            {
                entity.ToTable("top_maga_scaffale", Schema);

                entity.Property(e => e.top_maga_scaffaleid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_maga_scaffale'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(10);

                entity.Property(e => e.descrizione).HasMaxLength(150);

                entity.HasOne(d => d.top_maga_settore)
                    .WithMany(p => p.top_maga_scaffale)
                    .HasForeignKey(d => d.top_maga_settoreid)
                    .HasConstraintName("fk_top_maga_scaffale_01");
            });

            modelBuilder.Entity<top_maga_settore>(entity =>
            {
                entity.ToTable("top_maga_settore", Schema);

                entity.Property(e => e.top_maga_settoreid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_maga_settore'::regclass)");

                entity.Property(e => e.codice).HasMaxLength(10);

                entity.Property(e => e.descrizione).HasMaxLength(150);

                entity.HasOne(d => d.top_maga)
                    .WithMany(p => p.top_maga_settore)
                    .HasForeignKey(d => d.top_magaid)
                    .HasConstraintName("fk_top_maga_settore_01");
            });

            modelBuilder.Entity<top_nume>(entity =>
            {
                entity.ToTable("top_nume", Schema);

                entity.Property(e => e.top_numeid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_nume'::regclass)");

                entity.Property(e => e.cod_docu)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_nume)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.dat_ctrl).HasColumnType("numeric(1,0)");

                entity.Property(e => e.des_nume).HasMaxLength(50);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.pro_nume).HasColumnType("numeric(10,0)");

                entity.Property(e => e.suf_nume)
                    .HasMaxLength(4)
                    .IsFixedLength();
            });

            modelBuilder.Entity<top_repo>(entity =>
            {
                entity.ToTable("top_repo", Schema);

                entity.Property(e => e.top_repoid).HasDefaultValueSql($"nextval('{Schema}.seq_pk_top_repo'::regclass)");

                entity.Property(e => e.stp_comm)
                    .HasMaxLength(50)
                    .IsFixedLength();

                entity.Property(e => e.stp_copy).HasColumnType("numeric(3,0)");

                entity.Property(e => e.stp_docu)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.stp_enab).HasColumnType("numeric(1,0)");

                entity.Property(e => e.stp_file)
                    .HasMaxLength(100)
                    .IsFixedLength();

                entity.Property(e => e.stp_fine).HasColumnType("numeric(1,0)");

                entity.Property(e => e.stp_modo)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.stp_ncol).HasColumnType("numeric(1,0)");

                entity.Property(e => e.stp_path)
                    .HasMaxLength(100)
                    .IsFixedLength();

                entity.Property(e => e.stp_tipo)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.stp_view).HasColumnType("numeric(1,0)");
            });

            modelBuilder.Entity<view_aggiornaprezzi>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("view_aggiornaprezzi", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<vw_age_nomi>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_age_nomi", Schema);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_exte).HasMaxLength(250);

                entity.Property(e => e.cod_fisc).HasColumnType("character varying");

                entity.Property(e => e.fel_codice_dest).HasMaxLength(7);

                entity.Property(e => e.filtro_3).HasMaxLength(250);

                entity.Property(e => e.last_status_code).HasMaxLength(250);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.par_tiva).HasColumnType("character varying");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.rif_avvi).HasMaxLength(10);

                entity.Property(e => e.rif_citt).HasMaxLength(50);

                entity.Property(e => e.rif_iweb).HasMaxLength(50);

                entity.Property(e => e.rif_mail).HasColumnType("character varying");

                entity.Property(e => e.rif_prov).HasMaxLength(2);

                entity.Property(e => e.sta_next).HasMaxLength(250);

                entity.Property(e => e.tel_nfax).HasColumnType("character varying");

                entity.Property(e => e.tel_nume).HasColumnType("character varying");
            });

            modelBuilder.Entity<vw_age_nomi_ddl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_age_nomi_ddl", Schema);

                entity.Property(e => e.cli_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.for_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.par_tiva).HasColumnType("character varying");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rag_tito).HasMaxLength(20);

                entity.Property(e => e.rap_pref).HasColumnType("numeric(1,0)");

                entity.Property(e => e.vet_pref).HasColumnType("numeric(1,0)");
            });

            modelBuilder.Entity<vw_ana_arti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_arti", Schema);

                entity.Property(e => e.anno).HasMaxLength(50);

                entity.Property(e => e.art_cuba).HasColumnType("numeric(18,5)");

                entity.Property(e => e.art_iweb).HasMaxLength(100);

                entity.Property(e => e.art_misu).HasMaxLength(10);

                entity.Property(e => e.art_peso).HasColumnType("numeric(18,5)");

                entity.Property(e => e.art_ubic).HasMaxLength(100);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.cod_valu)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.colore).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.entrate).HasColumnType("numeric");

                entity.Property(e => e.filtro_pers).HasMaxLength(100);

                entity.Property(e => e.for_arti).HasMaxLength(50);

                entity.Property(e => e.giacenza).HasColumnType("numeric");

                entity.Property(e => e.gru_arti).HasColumnType("character varying");

                entity.Property(e => e.gru_arti1).HasColumnType("character varying");

                entity.Property(e => e.imponibile).HasColumnType("numeric");

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_desc).HasMaxLength(50);

                entity.Property(e => e.ivato).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.maga_riferimento).HasMaxLength(50);

                entity.Property(e => e.mar_arti).HasMaxLength(50);

                entity.Property(e => e.net_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.nev_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_arro).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_vend).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_coll).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_conf).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_maxi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_rior).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_scor).HasColumnType("numeric(14,3)");

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.rif_arti).HasMaxLength(50);

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.um).HasMaxLength(10);

                entity.Property(e => e.uscite).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_ana_arti_ana_misu>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_arti_ana_misu", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.misu_code).HasMaxLength(10);

                entity.Property(e => e.misu_code_base).HasMaxLength(10);

                entity.Property(e => e.misu_desc).HasMaxLength(100);

                entity.Property(e => e.misu_desc_base).HasMaxLength(100);

                entity.Property(e => e.qta_misu_in).HasColumnType("numeric(18,5)");

                entity.Property(e => e.qta_misu_out).HasColumnType("numeric(18,5)");
            });

            modelBuilder.Entity<vw_ana_arti_ddl>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_arti_ddl", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.descrizione).HasMaxLength(100);

                entity.Property(e => e.ff).HasMaxLength(100);

                entity.Property(e => e.for_arti).HasMaxLength(50);

                entity.Property(e => e.giacenza).HasColumnType("numeric");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.rif_arti).HasMaxLength(50);

                entity.Property(e => e.sel_arti).HasMaxLength(20);
            });

            modelBuilder.Entity<vw_ana_cauz>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_cauz", Schema);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_da_rendere).HasColumnType("numeric");

                entity.Property(e => e.qta_da_rendere_ap).HasColumnType("numeric");

                entity.Property(e => e.qta_resa).HasColumnType("numeric");

                entity.Property(e => e.qta_usci).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_ana_list>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_list", Schema);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.datainizio_in).HasColumnType("date");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pro_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pro_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.prv_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.prv_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.ric_fissa).HasColumnType("numeric(6,2)");

                entity.Property(e => e.ric_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_text).HasMaxLength(50);

                entity.Property(e => e.sconto_a).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sconto_da).HasColumnType("numeric(6,2)");
            });

            modelBuilder.Entity<vw_ana_lotto>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_lotto", Schema);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.codice).HasMaxLength(30);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(13);

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.rag_soci).HasMaxLength(100);
            });

            modelBuilder.Entity<vw_ana_maga>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_maga", Schema);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.des_maga).HasMaxLength(50);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_disp).HasColumnType("numeric");

                entity.Property(e => e.qta_entr).HasColumnType("numeric");

                entity.Property(e => e.qta_giac).HasColumnType("numeric");

                entity.Property(e => e.qta_giac_ap).HasColumnType("numeric");

                entity.Property(e => e.qta_impe).HasColumnType("numeric");

                entity.Property(e => e.qta_inve).HasColumnType("numeric");

                entity.Property(e => e.qta_ordi).HasColumnType("numeric");

                entity.Property(e => e.qta_usci).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_ana_ubicazione_nocfc>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ana_ubicazione_nocfc", Schema);

                entity.Property(e => e.quantita).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_cauzioni>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_cauzioni", Schema);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_da_rendere).HasColumnType("numeric");

                entity.Property(e => e.qta_resa).HasColumnType("numeric");

                entity.Property(e => e.qta_usci).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_cronologiadocumenti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_cronologiadocumenti", Schema);

                entity.Property(e => e.cau_usci).HasColumnType("numeric(1,0)");

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.num_docu).HasMaxLength(13);

                entity.Property(e => e.pre_tota).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.tipo)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<vw_ddt_gene>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ddt_gene", Schema);

                entity.Property(e => e.chilometri_oggetto).HasMaxLength(250);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_tipo).HasMaxLength(20);

                entity.Property(e => e.codice_pin).HasMaxLength(50);

                entity.Property(e => e.controllo_documento).HasMaxLength(50);

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.des_paga).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.note).HasMaxLength(4000);

                entity.Property(e => e.num_boll).HasMaxLength(13);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.ora).HasMaxLength(50);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.targa_oggetto).HasMaxLength(250);

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<vw_ddt_gene_stato>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ddt_gene_stato", Schema);
            });

            modelBuilder.Entity<vw_ddt_gene_tot>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ddt_gene_tot", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.tot_impo).HasColumnType("numeric");

                entity.Property(e => e.tot_scontato).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_doc_arti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_doc_arti", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_acqu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_logi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<vw_fat_gene>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fat_gene", Schema);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.fel_cod_stato).HasMaxLength(50);

                entity.Property(e => e.fel_des_stato).HasMaxLength(250);

                entity.Property(e => e.importo_dapag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importo_pag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_fatt).HasMaxLength(13);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.totale_documento).HasColumnType("numeric(20,5)");

                entity.Property(e => e.totale_imponibile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.totale_imposta).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<vw_fat_gene_rate>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fat_gene_rate", Schema);

                entity.Property(e => e.desc_pagam).HasMaxLength(200);

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importo_pagato).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_fat_gene_rate_pagamenti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fat_gene_rate_pagamenti", Schema);

                entity.Property(e => e.desc_pagam).HasMaxLength(200);

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.note).HasMaxLength(250);
            });

            modelBuilder.Entity<vw_fat_gene_tot>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fat_gene_tot", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.tot_impo).HasColumnType("numeric");

                entity.Property(e => e.tot_scontato).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_fat_passiva>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fat_passiva", Schema);

                entity.Property(e => e.cod_fisc).HasMaxLength(15);

                entity.Property(e => e.descrizione).HasMaxLength(250);

                entity.Property(e => e.external_id).HasMaxLength(50);

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importo_dapag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importo_pag).HasColumnType("numeric(20,5)");

                entity.Property(e => e.key_anno).HasMaxLength(4);

                entity.Property(e => e.mittente).HasColumnType("character varying");

                entity.Property(e => e.num_doc).HasMaxLength(50);

                entity.Property(e => e.num_fatt).HasMaxLength(50);

                entity.Property(e => e.par_tiva).HasMaxLength(15);

                entity.Property(e => e.tip_doc).HasMaxLength(10);
            });

            modelBuilder.Entity<vw_fat_passiva_rate>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fat_passiva_rate", Schema);

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.importo_pagato).HasColumnType("numeric");

                entity.Property(e => e.mod_paga).HasMaxLength(10);
            });

            modelBuilder.Entity<vw_fat_passiva_rate_pagamenti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fat_passiva_rate_pagamenti", Schema);

                entity.Property(e => e.desc_pagam).HasMaxLength(200);

                entity.Property(e => e.importo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.note).HasMaxLength(250);
            });

            modelBuilder.Entity<vw_fatt_attive_scadenze>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_fatt_attive_scadenze", Schema);

                entity.HasComment("1");

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.dapagare).HasColumnType("numeric");

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.importo).HasColumnType("numeric");

                entity.Property(e => e.importopagato).HasColumnType("numeric");

                entity.Property(e => e.num_fatt).HasMaxLength(13);
            });

            modelBuilder.Entity<vw_giacenzemagazzino>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_giacenzemagazzino", Schema);

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.qta_disp).HasColumnType("numeric");

                entity.Property(e => e.qta_entr).HasColumnType("numeric");

                entity.Property(e => e.qta_giac).HasColumnType("numeric");

                entity.Property(e => e.qta_impe).HasColumnType("numeric");

                entity.Property(e => e.qta_ordi).HasColumnType("numeric");

                entity.Property(e => e.qta_usci).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_incassi_clienti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_incassi_clienti", Schema);

                entity.HasComment("1");

                entity.Property(e => e.importogaranzia).HasColumnType("numeric");

                entity.Property(e => e.importopagato).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_mov_gene>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_mov_gene", Schema);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.imponibile).HasColumnType("numeric(20,5)");

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_docu).HasMaxLength(20);

                entity.Property(e => e.num_movi).HasMaxLength(13);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.tot_acco).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<vw_mov_gene_tot>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_mov_gene_tot", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.tot_impo).HasColumnType("numeric");

                entity.Property(e => e.tot_scontato).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_movimento_contropartita_segno>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_movimento_contropartita_segno", Schema);

                entity.Property(e => e.importo).HasColumnType("numeric(20,2)");

                entity.Property(e => e.note).HasMaxLength(300);
            });

            modelBuilder.Entity<vw_ord_gene_tot>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_ord_gene_tot", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.tot_impo).HasColumnType("numeric");

                entity.Property(e => e.tot_scontato).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_pre_gene_tot>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_pre_gene_tot", Schema);

                entity.Property(e => e.art_item).HasMaxLength(20);

                entity.Property(e => e.cod_prez).HasMaxLength(20);

                entity.Property(e => e.iva_code).HasMaxLength(3);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_1).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_2).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_3).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sco_4).HasColumnType("numeric(6,2)");

                entity.Property(e => e.sel_arti).HasMaxLength(20);

                entity.Property(e => e.tot_impo).HasColumnType("numeric");

                entity.Property(e => e.tot_scontato).HasColumnType("numeric");
            });

            modelBuilder.Entity<vw_puntivendita_movimenti>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_puntivendita_movimenti", Schema);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_list)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.key_anno)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.num_logi).HasMaxLength(20);

                entity.Property(e => e.num_movi).HasMaxLength(13);
            });

            modelBuilder.Entity<vw_saldi>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vw_saldi", Schema);

                entity.Property(e => e.saldi).HasColumnType("numeric");
            });

            modelBuilder.Entity<vwcausale>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwcausale", Schema);

                entity.Property(e => e.anno).HasMaxLength(4);

                entity.Property(e => e.codice).HasMaxLength(3);

                entity.Property(e => e.descrizione).HasMaxLength(100);

                entity.Property(e => e.ds_registroiva).HasColumnType("character varying");

                entity.Property(e => e.ds_tipomovimento).HasColumnType("character varying");
            });

            modelBuilder.Entity<vwcommessa_nomi_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwcommessa_nomi_view", Schema);

                entity.Property(e => e.cod_commessa).HasMaxLength(50);

                entity.Property(e => e.descr_commessa).HasMaxLength(50);
            });

            modelBuilder.Entity<vwcommessa_utilizzata>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwcommessa_utilizzata", Schema);

                entity.Property(e => e.cod_commessa).HasMaxLength(50);
            });

            modelBuilder.Entity<vwcustomerorder_totaleevaso>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwcustomerorder_totaleevaso", Schema);

                entity.Property(e => e.totale_evaso).HasColumnType("numeric");
            });

            modelBuilder.Entity<vwcustomerorder_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwcustomerorder_view", Schema);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_tipo).HasMaxLength(20);

                entity.Property(e => e.cod_vett).HasMaxLength(10);

                entity.Property(e => e.data_consegna).HasColumnType("date");

                entity.Property(e => e.des_caus).HasMaxLength(50);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.log_1).HasMaxLength(50);

                entity.Property(e => e.log_2).HasMaxLength(50);

                entity.Property(e => e.log_3).HasMaxLength(50);

                entity.Property(e => e.log_4).HasMaxLength(50);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_prev).HasMaxLength(13);

                entity.Property(e => e.ora_consegna).HasMaxLength(50);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.rap_1).HasMaxLength(100);

                entity.Property(e => e.rap_2).HasMaxLength(100);

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");
            });

            modelBuilder.Entity<vwdoc_arti_marca_modello>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwdoc_arti_marca_modello", Schema);

                entity.Property(e => e.marca).HasMaxLength(500);
            });

            modelBuilder.Entity<vword_arti_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vword_arti_view", Schema);

                entity.Property(e => e.art_misu).HasMaxLength(5);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_logi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<vwpre_arti_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwpre_arti_view", Schema);

                entity.Property(e => e.art_misu).HasColumnType("character varying");

                entity.Property(e => e.art_misu_base).HasMaxLength(5);

                entity.Property(e => e.cod_arti).HasMaxLength(50);

                entity.Property(e => e.des_arti).HasMaxLength(100);

                entity.Property(e => e.iva_perc).HasColumnType("numeric(6,2)");

                entity.Property(e => e.pre_impo).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_coll).HasColumnType("numeric(20,5)");

                entity.Property(e => e.qta_logi).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota).HasColumnType("numeric(14,3)");

                entity.Property(e => e.qta_tota_base).HasColumnType("numeric(14,3)");

                entity.Property(e => e.sco_text).HasMaxLength(50);
            });

            modelBuilder.Entity<vwsupplierorder_view>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("vwsupplierorder_view", Schema);

                entity.Property(e => e.cod_agen).HasMaxLength(10);

                entity.Property(e => e.cod_caus).HasMaxLength(3);

                entity.Property(e => e.cod_maga)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.cod_vett).HasMaxLength(10);

                entity.Property(e => e.doc_rife).HasMaxLength(50);

                entity.Property(e => e.doc_stat).HasMaxLength(20);

                entity.Property(e => e.num_comm).HasMaxLength(50);

                entity.Property(e => e.num_ordi).HasMaxLength(13);

                entity.Property(e => e.rag_soci).HasMaxLength(100);

                entity.Property(e => e.tot_docu).HasColumnType("numeric(20,5)");

                entity.Property(e => e.tot_impo).HasColumnType("numeric(20,5)");
            });

            OnModelCreatingPartial(modelBuilder);
        }
        void SaveChanges(oneContext sinergiaONEContext)
        {
            ClaimsIdentity identity = _httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity;

            int userID = 1;
            if (identity != null && identity.HasClaim(s => s.Type == System.Security.Claims.ClaimTypes.NameIdentifier))
            {
                userID = int.Parse(identity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
            }


            foreach (EntityEntry entry in sinergiaONEContext.ChangeTracker.Entries())
            {
                setLog(userID, entry);
            }
        }

        /// <summary>
        /// valorizzazione dei campi per il log
        /// </summary>
        /// <param name="userID">id utente operazione</param>
        /// <param name="entry">entità</param>
        void setLog(int userID, EntityEntry entry)
        {

            if (entry.Properties.Any(s => s.Metadata.Name == "creationuser"))
            {
                PropertyEntry creationUser = entry.Property("creationuser");
                if (creationUser.CurrentValue == null || (creationUser.CurrentValue is int && (int)creationUser.CurrentValue == 0))
                {
                    if (entry.State == EntityState.Added)
                    {
                        creationUser.CurrentValue = userID;
                    }
                    else
                    {
                        creationUser.IsModified = false;
                    }
                }
            }

            if (entry.Properties.Any(s => s.Metadata.Name == "creationdate"))
            {
                PropertyEntry creationDate = entry.Property("creationdate");
                if (creationDate.CurrentValue == null || (creationDate.CurrentValue is DateTime && (DateTime)creationDate.CurrentValue == DateTime.MinValue))
                {
                    if (entry.State == EntityState.Added)
                    {
                        creationDate.CurrentValue = DateTime.Now;
                    }
                    else
                    {
                        creationDate.IsModified = false;
                    }
                }
            }


            if (entry.Properties.Any(s => s.Metadata.Name == "lastmodifyuser"))
            {
                PropertyEntry lastModifyUser = entry.Property("lastmodifyuser");
                lastModifyUser.CurrentValue = userID;
            }

            if (entry.Properties.Any(s => s.Metadata.Name == "lastmodifydate"))
            {
                PropertyEntry lastModifyDate = entry.Property("lastmodifydate");
                lastModifyDate.CurrentValue = DateTime.Now;
            }

            if (entry.Properties.Any(s => s.Metadata.Name == "enabled"))
            {
                PropertyEntry enabled = entry.Property("enabled");
                if (enabled.CurrentValue == null)
                {
                    enabled.CurrentValue = true;
                }
            }
        }


        public override int SaveChanges()
        {
            this.SaveChanges(this);

            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            this.SaveChanges(this);
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            this.SaveChanges(this);
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            this.SaveChanges(this);
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
