﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace OneDatabase.Data
{


    class SchemaTenantCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
            => new SchemaTenantModelCacheKey(context);
    }


    class SchemaTenantModelCacheKey : ModelCacheKey
    {
       readonly string _schema;

        public SchemaTenantModelCacheKey(DbContext context)
            : base(context)
        {
            _schema = (context as oneContext)?.Schema;
        }

        protected override bool Equals(ModelCacheKey other)
            => base.Equals(other)
                && (other as SchemaTenantModelCacheKey)?._schema == _schema;

        public override int GetHashCode()
        {
            var hashCode = base.GetHashCode() * 397;
            if (_schema != null)
            {
                hashCode ^= _schema.GetHashCode();
            }

            return hashCode;
        }
    }

}
